<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/shopping.css" media="all" />

<article>
			
			<div id="teaser-tit">
				<h2 class="teaser-titin">お支払い方法・お届け時間帯</h2>
			</div><!-- /#teaser-tit -->
			
			<p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>お支払い方法・お届け時間帯</p>
			
			<div id="contents">
				
				<div class="visible-pc">
					<ul class="dexpress-process-list clearfix">
						<li>お届け先の確認</li>
						<li class="on">お支払い方法・お届け時間帯</li>
						<li>入力内容のご確認</li>
						<li>完了</li>
					</ul>
				</div>
<form method="post" action="<!--{$smarty.const.MOBILE_SHOPPING_PAYMENT_URLPATH}-->">
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="mode" value="confirm">
<input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->">


					<div class="section">
						<div id="form-area">
							<div class="section">
								<div class="payment-table">
									<table class="com-table01">
										<tr>
											<th>お支払い方法</th>
											<td>
												
<!--{assign var=key value="deliv_id"}-->
		<input type="hidden" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|h}-->">
	<!--{assign var=key value="payment_id"}-->
	<!--{if $arrErr[$key] != ""}-->
		<font color="#FF0000"><!--{$arrErr[$key]}--></font>
	<!--{/if}-->
	<!--{section name=cnt loop=$arrPayment}-->
	<label for="">
		<input type="radio" name="<!--{$key}-->" value="<!--{$arrPayment[cnt].payment_id}-->" <!--{$arrPayment[cnt].payment_id|sfGetChecked:$arrForm[$key].value|h}-->>

		<span><!--{$arrPayment[cnt].payment_method|h}--></span>
		</label><br>
	<!--{/section}-->
※ポイント支払いの場合は銀行振り込みを選択してください。
<br>
											</td>
										</tr>



										<tr>
											<th>お届け時間の指定</th>
											<td>

										<!--{if $cartKey != $smarty.const.PRODUCT_TYPE_DOWNLOAD}-->
			
		<!--{foreach item=shippingItem name=shippingItem from=$arrShipping}-->
		<!--{assign var=index value=$shippingItem.shipping_id}-->

		
		<!--★お届け日★-->
		<!--{assign var=key value="deliv_date`$index`"}-->
			<font color="#FF0000"><!--{$arrErr[$key]}--></font>

	
		<!--{if !$arrDelivDate}-->
		
		<!--{else}-->
		    <select name="<!--{$key}-->">
		        <option value="" selected="">指定なし</option>
		        <!--{html_options options=$arrDelivDate selected=$arrForm[$key].value|h}-->
		    </select>
		<!--{/if}-->



											
													<div class="clearfix">
														<div>
														<div>
															<!--★お届け時間★-->
		<!--{assign var=key value="deliv_time_id`$index`"}-->
		<font color="#FF0000"><!--{$arrErr[$key]}--></font>
		
		<select name="<!--{$key}-->" id="<!--{$key}-->" class="payment-select">
		    <option value="" selected="">指定なし</option>
		    <!--{html_options options=$arrDelivTime selected=$arrForm[$key].value|h}-->
		</select>
		<!--{/foreach}-->
		<!--{/if}-->

															</div>
														</div>
														<p class="payment-note">※お届け日はご指定いただけません。</p>
													</div>
											
												
											</td>
										</tr>



										<tr>
											<th>その他お問い合わせ</th>
											<td>
												<div>

	
		<!--{assign var=key value="message"}-->
		<!--{if $arrErr[$key] != ""}-->
			<font color="#FF0000"><!--{$arrErr[$key]}--></font>
		<!--{/if}-->
		<textarea cols="20" rows="2" name="<!--{$key}-->" class="payment-textarea"><!--{$arrForm[$key].value|h}--></textarea>

												</div>
											</td>
										</tr>
										<tr>
											<th class="w20per">ポイント使用の指定</th>
											<td>

		<!--{if $tpl_login == 1 && $smarty.const.USE_POINT !== false}-->
		<p>ポイントは商品購入時に1ptを<!--{$smarty.const.POINT_VALUE}-->円として使用する事ができます。<br>
※但し、送料・代引き手数料に関しましては、ポイントのご使用ができません。<br>
		<!--{$name01|h}--> <!--{$name02|h}-->様の、現在の所持ポイントは「<span class="fcred"><!--{$tpl_user_point|number_format|default:0}--> pt</span>」です。<br>

		今回ご購入合計金額：<span class="fcred"><!--{$arrPrices.subtotal|number_format}-->円(税込)</span><br>
		（送料・手数料を含みません）<br>

		今回利用可能最大ポイント<span class="fcred"><!--{$arrPrices.usable_point|number_format}-->ポイント</span></p><br>
<p class="payment-table-box"><span>
		<input type="radio" name="point_check" value="1" <!--{$arrForm.point_check.value|sfGetChecked:1}-->></span></span><span>ポイントを使用する</span></p>
		<!--{assign var=key value="use_point"}-->
		<!--{if $arrErr[$key] != ""}-->
			<font color="#FF0000"><!--{$arrErr[$key]}--></font>
		<!--{/if}-->
			<span><input type="tel" name="<!--{$key}-->" value="<!--{$arrForm[$key].value|default:0}-->" maxlength="<!--{$arrForm[$key].length}-->" size="6"></span><span>ポイント使用する</span></p>
			<p class="payment-table-box">
													<label>
														<input type="radio" name="point_check" value="2" <!--{$arrForm.point_check.value|sfGetChecked:2}-->>
														<span>ポイントを使用しない</span>
													</label>
												</p>
		<!--{/if}-->


												
												
											</td>
										</tr>
									</table>
								</div>
							</div>
							


							<div class="inner">
								<div class="combtn-top combtn-twocolumn">
									<ul class="clearfix">
										<li class="flR"><input type="submit" value="次へ" class="input-submit"></li>
</form>
										<li class="flR"><form action="<!--{$tpl_back_url|h}-->" method="get">
		<input type="submit" name="return" value="戻る" class="input-button">
	</form></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div><!-- /#contents -->
			
		</article>



