<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<!DOCTYPE html>
<html lang="ja">
<head>
<meta name="format-detection" content="telephone=no">
<meta http-equiv=”Content-Type” content=”text/html; charset=utf-8″/>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<link rel="stylesheet" href="<!--{$TPL_URLPATH}-->css/common.css" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/nav.css" media="all" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<title><!--{$arrSiteInfo.shop_name|h}--><!--{if $tpl_subtitle|strlen >= 1}-->/<!--{$tpl_subtitle|h}--><!--{elseif $tpl_title|strlen >= 1}-->/<!--{$tpl_title|h}--><!--{/if}--></title>
<meta name="author" content="<!--{$arrPageLayout.author|h}-->">
<meta name="description" content="<!--{$arrPageLayout.description|h}-->">
<meta name="keywords" content="<!--{$arrPageLayout.keyword|h}-->">
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/jquery.min.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/common.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/jquery.matchHeight-min.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/jquery.biggerlink.min.js"></script>
<script type="text/javascript" src="<!--{$TPL_URLPATH}-->js/jquery.sliderPro.min.js"></script>
<!--{* ▼Head COLUMN*}-->
<!--{if $arrPageLayout.HeadNavi|@count > 0}-->
<!--{* ▼上ナビ *}-->
<!--{foreach key=HeadNaviKey item=HeadNaviItem from=$arrPageLayout.HeadNavi}-->
<!--{* ▼<!--{$HeadNaviItem.bloc_name}--> ここから*}-->
<!--{if $HeadNaviItem.php_path != ""}-->
<!--{include_php file=$HeadNaviItem.php_path}-->
<!--{else}-->
<!--{include file=$HeadNaviItem.tpl_path}-->
<!--{/if}-->
<!--{* ▲<!--{$HeadNaviItem.bloc_name}--> ここまで*}-->
<!--{/foreach}-->
<!--{* ▲上ナビ *}-->
<!--{/if}-->
<!--{* ▲Head COLUMN*}-->
<script type="text/javascript">
	$(function(){
		if($('.biggerlink').length > 0){
			$('.biggerlink').biggerlink();
		}
	});
</script>
</head>
<!-- ▼ ＢＯＤＹ部 スタート -->
<!--{include file='./site_main.tpl'}-->
<!-- ▲ ＢＯＤＹ部 エンド -->
</html>
