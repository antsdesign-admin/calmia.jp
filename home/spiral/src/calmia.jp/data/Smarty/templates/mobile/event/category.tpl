<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<script type="text/javascript">
function fnNaviPage(pageno) {
    document.form1['pageno'].value = pageno;
    document.form1.submit();
}
</script>
<font size=1>

<!--{if count($arrNewsCategory) > 0}-->
<table width=100% bgcolor="<!--{$arrColor2[$tpl_themeid]}-->">
<tr>
<td align=center>
<font size=1>
<a href="<!--{$smarty.const.ROOT_URLPATH}-->event/category.php">ｶﾃｺﾞﾘ別一覧</a>&nbsp;|&nbsp;<a href="<!--{$smarty.const.ROOT_URLPATH}-->event/month.php">年月別一覧</a>
</font>
</td>
</tr>
</table>
<br />

<!--{foreach from=$arrNewsCategory item=newsctg name=newsctg}-->
<img src="<!--{$TPL_URLPATH}-->img/event/icon.gif" /><a href="<!--{$smarty.const.ROOT_URLPATH}-->event/index.php?mode=category&amp;category_id=<!--{$newsctg.category_id|h}-->&amp;<!--{$smarty.const.SID}-->"><!--{$newsctg.name|h}--></a>
(<!--{$newsctg.news_count|h}-->件)
<!--{if !$smarty.foreach.newsctg.last}--><br /><!--{/if}-->
<!--{/foreach}-->

<!--{else}-->
新着情報はありません。
<!--{/if}-->
</font>
