<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

           
            <!--{section name=data loop=$arrNews}-->
            <!--{assign var="date_array" value="-"|explode:$arrNews[data].news_date_disp}-->
                    <dl class="clearfix">
                      <dt><!--{$date_array[0]}-->/<!--{$date_array[1]}-->/<!--{$date_array[2]}--></dt>
                      <dd><a href="<!--{$smarty.const.ROOT_URLPATH}-->event/detail.php?news_id=<!--{$arrNews[data].news_id|h}-->"><!--{$arrNews[data].news_title|mb_substr:0:12|h|nl2br}--><!--{if $arrNews[data].news_title|mb_strlen > 14}-->...<!--{/if}--></a></dd>
                    </dl>
            <!--{/section}-->
