<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}--><link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/login.css" media="all" />
<article>
    <!--{if $errmsg}-->
        <span class="fcred"><!--{$errmsg}--></span><br>
    <!--{/if}-->

    <form action="<!--{$smarty.const.ROOT_URLPATH}-->forgot/index.php" method="post">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <input type="hidden" name="mode" value="secret_check">
        <!--{foreach key=key item=item from=$arrForm}-->
        <!--{if $key ne 'reminder_answer'}-->
        <input type="hidden" name="<!--{$key}-->" value="<!--{$item|h}-->" />
        <!--{/if}-->
        <!--{/foreach}-->

        <h2 class="login-hdl">パスワード再発行</h2>
        <div id="contents">
            <div class="inner">
                <p class="password-txt">ご登録時に入力した下記質問の答えを入力して「次へ」ボタンをクリックしてください。</p>


                <table class="com-table01 password-table">
                    <tr>
                        <th><!--{$arrReminder[$arrForm.reminder]|h}--></th>
                        <td>
                            <input type="text" name="reminder_answer" value="" class="input-text form-bgred w-max"><br />
                            <span class="fcred"><!--{$arrErr.reminder}--><!--{$arrErr.reminder_answer}--></span>
                        </td>
                    </tr>
                </table>
                <div class="combtn-top combtn-onecolumn">
                    <ul class="clearfix">
                        <li><input type="submit" value="次へ" name="next" id="next" class="input-submit"></li>
                    </ul>
                </div>
            </div>
        </div>
    </form>

</article>