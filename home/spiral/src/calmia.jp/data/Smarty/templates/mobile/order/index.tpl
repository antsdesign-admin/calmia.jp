<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/article.css" media="all" />
<div id="teaser-tit">
  <h2 class="teaser-titin">特定商取引に関する表記</h2>
</div>
<!-- /#teaser-tit -->
<p id="breadcrumb">
  <a href="/">HOME</a>
  <span>&gt;</span>特定商取引に関する表記
</p>
<div id="contents">
  <div class="inner clearfix">
    <div id="main" class="article-wrap">
      <div class="section">
        <table class="com-table02">
          <tr>
            <th>社名</th>
            <td>スパイラル株式会社</td>
          </tr>
          <tr>
            <th>代表者</th>
            <td>池田 秀之</td>
          </tr>
          <tr>
            <th>所在地</th>
            <td>〒730-0029　
              <br>広島県広島市中区三川町2-10
            </td>
          </tr>
          <tr>
            <th>連絡先</th>
            <td>TEL082-246-3333　　
              <br>FAX082-241-3355
            </td>
          </tr>
        </table>
      </div>
      <div id="anch01" class="section">
        <h3 class="hdl">販売価格</h3>
        <p>商品ごとに表示。総額で表記しております。</p>
      </div>
      <div id="anch02" class="section">
        <h3 class="hdl">商品代金以外の必要料金</h3>
        <p>送料、代引手数料、振込手数料</p>
      </div>
      <div id="anch03" class="section">
        <h3 class="hdl">送料</h3>
        <p>■1回のご注文合計
          <br>11,000円以上の場合…送料無料
          <br>11,000円未満の場合…全国一律送料550円
<br>
          <br>■代引手数料
          <br>11,000円未満の場合…330円
          <br>11,000円以上～33,000円未満の場合…440円
          <br>33,000円以上～110,000円未満の場合…660円
          <br>110,000円以上～330,000円までの場合…1,100円
<br>
         <br> ※表示価格は全て税込です。
        </p>
      </div>
      <div id="anch04" class="section">
        <h3 class="hdl">お支払い方法</h3>
        <p>■クレジット決済
          <br>VISA/MASTERS/JCB/AMEXのカードがご利用いただけます。
          <br>お支払い回数は1回のみとなります。
        </p>
        <p>■代金引換
          <br>※代引手数料はお客様負担でお願いいたします。
          <br>■銀行振込（前払い）
          <br>※振込手数料はお客様負担でお願いいたします。
          <br>
          <table>
            <tr>
              <th>振込先…</th>
              <td>広島銀行 三川町支店
                <br>普通 3025499 スパイラル株式会社
              </td>
            </tr>
          </table>
        </p>
      </div>
      <div id="anch05" class="section">
        <h3 class="hdl">お支払い時期</h3>
        <p>■クレジット決済
          <br>購入時に自動決済されます。
          <br>■代金引換
          <br>商品配達時に代金を宅配便のドライバーに直接お支払いください。
          <br>■銀行振込
          <br>ご注文日より5日以内にお振込みください。
        </p>
      </div>
      <div id="anch06" class="section">
        <h3 class="hdl">引渡し時期</h3>
        <p>■クレジット決済にてご注文の場合
          <br>決済確認でき次第、3～4日以内で発送いたします。
          <br>■代金引換にてご注文の場合
          <br>ご注文いただいてから3～4日以内で発送いたします。
          <br>■銀行振込(前払い)にてご注文の場合
          <br>ご入金確認でき次第、3～4日以内で発送いたします。
          <br>在庫がない場合は1週間程度お時間いただく場合もございます。
        </p>
      </div>
      <div id="anch07" class="section">
        <h3 class="hdl">配送先</h3>
        <p>日本国内のみ</p>
      </div>
      <div id="anch08" class="section">
        <h3 class="hdl">返品・交換について</h3>
        <p>■お客様のご都合による返品（未開封･未使用の商品に限ります）
          <br>返品は商品到着後10日以内までお受けいたします｡配送料･代金振込手数料･代引手数料はお客様のご負担となります。
          <br>また、店舗でのご対応はできかねますので、予めご了承くださいませ。
          <br>■弊社不備による返品の場合（不良品）
          <br>商品お届け日から10日以内に商品を着払いにてご返送ください｡弊社より代替品を配送させていただきます｡
          <br>交換は､不良品（弊社不備）以外は承っておりません｡
          <br>■返品をお受けできない商品
          <br>商品がお手元に届いて10日以上経過した商品 
          <br>開封された商品
          <br>汚損･破損された商品 
          <br>店舗にてお買い上げいただいた商品
        </p>
        <p class="article-box">カルミアお客様サポートデスク
          <br>E-mail：info@calmia.jp
          <br>TEL：0120-86-3730
          <br>受付時間　月～金／9:30～18:00
          <br>（土日祝・年末年始・GW・お盆・当社規定休日を除く） 
        </p>
      </div>
    </div>
    <div id="side">
      <ul class="side-list scroll">
        <li>
          <a href="#anch01">販売価格</a>
        </li>
        <li class="ls">
          <a href="#anch02">商品代金以外の必要料金</a>
        </li>
        <li>
          <a href="#anch03">送料</a>
        </li>
        <li>
          <a href="#anch04">お支払い方法</a>
        </li>
        <li>
          <a href="#anch05">お支払い期限</a>
        </li>
        <li>
          <a href="#anch06">引渡し時期</a>
        </li>
        <li>
          <a href="#anch07">配送先</a>
        </li>
        <li>
          <a href="#anch08">返品・交換について</a>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- /#contents -->
</article>