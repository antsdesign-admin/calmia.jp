<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/sitemap.css" media="all" />

<div class="header-box clearfix">
                    <div class="header-logo visible-pc">
                        <dl>
                            <dt><a href="/" class="op"><img src="<!--{$TPL_URLPATH}-->img/common/logo.svg" width="85" alt="calmia"></a></dt>
                     
                        </dl>
                    </div>
                    <div class="header-logo visible-ts">
                        <dl>
                            <dt><a href="/"><img src="<!--{$TPL_URLPATH}-->img/common/logo.svg" width="52" height="52" alt="calmia"></a></dt>
                        </dl>
                    </div>
                    <div class="header-area visible-pc">
                        <form method="post">
                            <ul class="header-list clearfix">
                                <li><a class="opa" href="<!--{$smarty.server.PHP_SELF|escape}-->" onclick="fnFormModeSubmit('login_form', 'logout', '', ''); return false;"><a class="ic-move" href="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php?<!--{$smarty.const.SID}-->&amp;mode=logout"><img class="ic-move2" src="<!--{$TPL_URLPATH}-->img/common/logout_header.jpg" alt="logout">ログアウト</a>
                            </a></li>
                                <li><a class="ic-move" href="/cart/"><img class="cart-move" src="<!--{$TPL_URLPATH}-->img/common/cart.svg" width="25" height="25" alt="cart">カゴの中を見る</a></li>
  
                            </ul>
                        </form>
        <!--▼現在のポイント-->
        <!--{if $point_disp !== false}-->
                        <p class="header-txt01"><!--{$CustomerName1|h}--> <!--{$CustomerName2|h}-->さん　いつもありがとうございます。現在のポイントは <!--{if $smarty.const.USE_POINT !== false}--><span class="fcred"><!--{$CustomerPoint|number_format|default:"0"|h}--></span> ポイントです。<!--{/if}--></p>
                    </div> <!--{/if}-->
        <!--▲現在のポイント-->
                    <div class="header-area visible-ts">
                        <ul class="header-list clearfix">
                            <li><a href="/cart/"><img src="<!--{$TPL_URLPATH}-->img/common/cart.svg" width="25" height="25" alt="cart"></a></li>
                            <li><p class="search-in" data-target=".headersp-search"><a href="javascript:void(0)"><img src="<!--{$TPL_URLPATH}-->img/common/search.svg" alt="search" width="25" height="25"></a></p></li>
                            <li><p class="navbar-toggle" data-target=".navbar-collapse"><a href="javascript:void(0)">menu</a></p></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <nav class="navbar-collapse">
            <div class="navbar-box">
                <ul>
                    <li><a href="/">HOME</a></li>
                    <li><a href="/products/">ショッピング</a></li>
                    <li><a href="/mypage/favorite.php">お気に入り</a></li>
                    <li><a href="/mypage/history_list.php">ご注文履歴</a></li>
                    <li><a href="/guide/order.php">ご注文ガイド</a></li>
                    <li><a href="/mypage/change.php">お客様情報の変更</a></li>
                    <li><a href="/order/">特定商取引に関する表記</a></li>
                    <li><a href="/guide/about.php">会社概要</a></li>
                    <li><a href="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php?<!--{$smarty.const.SID}-->&amp;mode=logout">ログアウト</a></li>
                </ul>
            </div>
        </nav>
        <div id="gnavi" class="visible-pc">
            <div class="gnavi-in">
                <p class="header-menu"><a href="javascript:void(0)"><img src="<!--{$TPL_URLPATH}-->img/common/menu_navi.png" alt="menu"></a></p>
                <ul class="gnavi-list">
                    <li><a href="/">HOME</a></li>
                    <li><a href="/products/">ショッピング</a></li>
                    <li><a href="/mypage/favorite.php">お気に入り</a></li>
                    <li><a href="/mypage/history_list.php">ご注文履歴</a></li>
                    <li><a href="/mypage/change.php">お客様情報の変更</a></li>
                </ul>
            </div>
        </div>
    </div><!-- /#header -->
</header>

<article>
            <div class="header-menu-box visible-ts">
                <div class="inner header-menusp">
                    <dl>
                        <dt><a href="javascript:void(0)"><img src="<!--{$TPL_URLPATH}-->img/common/menu_navi_ts.png" alt="menu"></a></dt>
                        <dd><!--▼現在のポイント-->
        <!--{if $point_disp !== false}-->
                        <!--{$CustomerName1|h}--> <!--{$CustomerName2|h}-->さん　いつもありがとうございます。<br>現在のポイントは <!--{if $smarty.const.USE_POINT !== false}--><span class="fcred"><!--{$CustomerPoint|number_format|default:"0"|h}--></span> ポイントです。<!--{/if}--></dd>
                    <!--{/if}-->
        <!--▲現在のポイント-->
                    </dl>
                </div>
            </div>




            <div id="teaser-tit">
                <h2 class="teaser-titin">サイトマップ</h2>
            </div><!-- /#teaser-tit -->
            
            <p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>サイトマップ</p>
            
            <div id="contents">
                
                
                <div class="section">
                    <div class="inner">
                        <div class="sitemap-box">
                            <ul class="clearfix">
                                <li><a href="/">● HOME</a></li>
                                <li><a href="/mypage/change.php">● お客様情報の変更</a></li>
                                <li><a href="/cart/">● 買い物カゴの中</a></li>
                                <li><a href="/event/index.php">● 新着情報</a></li>
                                <li><a href="/products/">● ショッピング</a></li>
                                <li><a href="/order/">● 特定商取引に関する表記</a></li>
                                <li><a href="/mypage/favorite.php">● お気に入り</a></li>
                                <li><a href="/guide/about.php">● 会社概要</a></li>
                                <li><a href="/mypage/history_list.php">● ご注文履歴</a></li>
                                <li><a href="/guide/privacy.php">● 個人情報保護方針</a></li>
                                <li><a href="/guide/order.php">● ご注文ガイド</a></li>
                                <li><a href="/user_data/sitemap.php">● サイトマップ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div><!-- /#contents -->
            
        </article>


<footer>
    <div id="footer">
        <div class="footer-link visible-ts">
            <ul>
                <li><a href="/mypage/change.php">お客様情報の変更</a></li>
                <li><a href="/order/">特定商取引に関する表記</a></li>
                <li><a href="/guide/about.php">会社概要</a></li>
                <li><a href="/guide/privacy.php">個人情報保護方針</a></li>
                <li><a href="/user_data/sitemap.php">サイトマップ</a></li>
            </ul>
        </div>
        <div class="footer-area">
            <div class="inner">
                <div class="footer-inner">
                    <dl class="footer-box">
                        <dt><a href="/" class="op"><img src="<!--{$TPL_URLPATH}-->img/common/logo.svg" width="120" alt="calmia"></a></dt>
                        <dd>
                            <div class="footer-in visible-pc">
                                <p class="footer-txt01">【お問い合わせ】カルミアお客様サポートデスク</p>
                                <p class="footer-tel01"><img src="<!--{$TPL_URLPATH}-->img/common/img_footer01.png" alt="0120863730"><span>9：30～18：00（土日祝・当社規定休日を除く）<br>E-mail：<a href="mailto:info@calmia.jp">info@calmia.jp</a></span></p>
                                <p class="footer-txt02">お電話またはE-mailでご注文いただいた場合、ポイントは付与されませんので予めご了承くださいませ。</p>
                                <p class="footer-txt03"><a href="/guide/order.php">ご注文ガイド</a> ｜ <a href="/order/">特定商取引に関する表記</a> ｜ <a href="/guide/about.php">会社概要</a> ｜ <a href="/guide/privacy.php">個人情報保護方針</a> ｜ <a href="/user_data/sitemap.php">サイトマップ</a></p>
                            </div>
                            <div class="footer-in visible-ts">
                                <p class="footersp-txt01">お問い合わせ</p>
                                <div class="footer-panel">
                                    <p>カルミアお客様サポートデスク</p>
                                    <p><a href="tel:0120-86-3730"><img src="<!--{$TPL_URLPATH}-->img/common/img_footer01.png" alt="0120863730"></a></p>
                                </div>
                                <p class="footersp-txt02">9：30～18：00（土日祝・当社規定休日を除く）<br>E-mail：<a href="mailto:info@calmia.jp">info@calmia.jp</a><br><span class="fcpink">お電話またはE-mailでご注文いただいた場合、<br>ポイントは付与されませんので予めご了承くださいませ。</span></p>
                            </div>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        <p id="copyright">copyright (c) calmia all rights reserved.</p>
    </div><!-- /#footer -->
       <p class="pagetop"><a href="#wrapper" class="op"><img class="pc" src="<!--{$TPL_URLPATH}-->img/common/page_top.png" alt="pagetop"><img class="sp" width="52" src="<!--{$TPL_URLPATH}-->img/common/page_top_sp.png" alt="pagetop"></a></p>
</footer>
</div><!-- /#wrapper -->
<div id="sidebar">

    <div class="sidebar-area">
        <p class="sidebar-logout"><a href="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php?<!--{$smarty.const.SID}-->&amp;mode=logout">ログアウト</a></p>
        <div class="sidebar-box">
            <p class="sidebar-tit">スキンケア</p>
            <ul class="sidebar-list">
                <li><a href="/products/list.php?mode=category&categorytype_id=6">&gt;&nbsp;クレンジング</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=62">&gt;&nbsp;洗顔</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=8">&gt;&nbsp;化粧水</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=9">&gt;&nbsp;美容液</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=30">&gt;&nbsp;クリーム</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=63">&gt;&nbsp;乳液</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=11">&gt;&nbsp;パック</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=43">&gt;&nbsp;角質ケア</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=66">&gt;&nbsp;UVケア</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=64">&gt;&nbsp;トライアル</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=44">&gt;&nbsp;その他スキンケア用品</a></li>
            </ul>
            <p class="sidebar-tit">メイク</p>
            <ul class="sidebar-list">
                <li><a href="/products/list.php?mode=category&categorytype_id=67">&gt;&nbsp;メイクベース</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=16">&gt;&nbsp;リキッドファンデーション</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=17">&gt;&nbsp;パウダーファンデーション</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=18">&gt;&nbsp;フェイスパウダー</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=19">&gt;&nbsp;アイブロウ</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=21">&gt;&nbsp;アイライナー</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=20">&gt;&nbsp;アイカラー</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=22">&gt;&nbsp;マスカラ</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=24">&gt;&nbsp;リップ</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=23">&gt;&nbsp;チーク</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=45">&gt;&nbsp;その他メイク用品</a></li>
            </ul>
            <p class="sidebar-tit">ボディーケア</p>
            <ul class="sidebar-list">
                <li><a href="/products/list.php?mode=category&categorytype_id=2">&gt;&nbsp;ボディーケア</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=25">&gt;&nbsp;バス用品</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=13">&gt;&nbsp;ヘアケア</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=66">&gt;&nbsp;UVケア</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=46">&gt;&nbsp;その他ボディーケア用品</a></li>
            </ul>
            <p class="sidebar-tit">ボディーウェア</p>
            <ul class="sidebar-list">
                <li><a href="/products/list.php?mode=category&categorytype_id=37">&gt;&nbsp;補整下着・ガードル</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=47">&gt;&nbsp;ボディーシェイパー</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=68">&gt;&nbsp;その他ボディーウェア用品</a></li>
            </ul>
            <p class="sidebar-tit">インナーケア</p>
            <ul class="sidebar-list">
                <li><a href="/products/list.php?mode=category&categorytype_id=65">&gt;&nbsp;サプリメント</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=49">&gt;&nbsp;ドリンク</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=71">&gt;&nbsp;食品</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=50">&gt;&nbsp;その他インナーケア用品</a></li>
            </ul>
            <p class="sidebar-tit">日用品・その他</p>
            <ul class="sidebar-list">
                <li><a href="/products/list.php?mode=category&categorytype_id=31">&gt;&nbsp;洗剤</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=35">&gt;&nbsp;オーラルケア</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=51">&gt;&nbsp;ベビー用品</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=69">&gt;&nbsp;エッセンシャルオイル</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=52">&gt;&nbsp;シェーバー</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=53">&gt;&nbsp;その他日用品</a></li>
            </ul>
        </div>
    </div>
</div><!-- /#sidebar -->
