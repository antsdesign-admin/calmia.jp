<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->



<!--▼CONTENTS-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/login.css" media="all" /><header>
<div id="wrapper" class="login-area">
      
            <div id="contents">
                <div class="inner">
                  <!--{if $tpl_form_mode != 'easy'}-->

                    <!--{/if}-->
                        <form name="member_form" id="member_form" method="post" action="<!--{$smarty.const.ROOT_URLPATH}-->frontparts/login_check.php" utn>
                        <!--{if $tpl_form_mode != 'easy'}-->
                        <input type="hidden" name="mode" value="login" >
                        <!--{else}-->
                        <input type="hidden" name="mode" value="easylogin" >
                        <!--{/if}-->
                        <input type="hidden" name="guid" value="on">
                        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                        <input type="hidden" name="url" value="<!--{$smarty.server.PHP_SELF|h}-->" />

                        <div id="form-area">
                            <div class="login-top-box">
                            <!--{if !$tpl_valid_phone_id || $tpl_form_mode != 'easy'}-->
                                <p class="login-top-in">
                                    <span class="top-id-tit">ID</span>
                                     <!--{assign var=key value="login_id"}-->
                                    <span class="top-id-input">
                                        <input type="text" name="<!--{$key}-->" value="<!--{$tpl_login_id|h}-->" maxlength="<!--{$smarty.const.SMTEXT_LEN}-->"><br>
                                        <!--{$arrErr[$key]}-->
                                    </span>
                                   
                                    <span class="top-id-check">
                                      
                                    </span>
                                </p>
                                 <!--{else}-->
                                    <input type="hidden" name="login_id" value="dummy">
                                <!--{/if}-->
                                <p class="login-top-in">
                                    <span class="top-pass-tit">パスワード</span>
                                    <!--{assign var=key value="login_pass"}-->
                                    <span class="top-pass-input">
                                    <input type="password" name="<!--{$key}-->" maxlength="<!--{$smarty.const.SMTEXT_LEN}-->" <!--{if $tpl_login_memory|h}-->value="●●●●●●●●"<!--{/if}-->>
                                        <!--{$arrErr[$key]}-->
                                    </span>
                                    <span class="top-pass-link">
                                        <a href="<!--{$smarty.const.HTTPS_URL}-->forgot/<!--{$smarty.const.DIR_INDEX_PATH}-->">パスワード再発行</a>
                                    </span>
                                </p>
                                <p class="top-pass-linksp">
                                    <a href="<!--{$smarty.const.HTTPS_URL}-->forgot/<!--{$smarty.const.DIR_INDEX_PATH}-->">パスワード再発行</a>
                                </p>
                                <p class="login-top-in save">
                                    <span class="top-pass-tit"></span>
                                  
                                    <!--{assign var=key value="login_memory"}-->
                                 <span class="top-pass-input save">
                                        <label><input type="checkbox" class="save" name="<!--{$key}-->" value="1" <!--{if $tpl_login_memory|h}-->checked="checked"<!--{/if}--> />ID・パスワードを保存</label>
                                 </span>
                                    <span class="top-pass-link">
                                      
                                    </span>
                                </p>
                                
                            </div>
                        </div>
                        <div class="login-top-btn combtn-twocolumn">
                            <ul class="clearfix">
                                <li><a href="<!--{$smarty.const.HTTPS_URL}-->entry/kiyaku.php" class="input-button">新規会員登録</a></li>
                                 <li><input type="submit" value="ログイン" class="input-submit"></li>
                            </ul>
                        </div>
                        <ul class="com-note">
                            <li>※初めての方は「新規会員登録」よりご登録ください。</li>
                            <li>※ID・パスワードをお忘れの方は <span class="login-top-telpc">0120-86-3730</span><span class="login-top-telsp"><a href="tel:0120-86-3730">0120-86-3730</a></span> まで直接お電話にてご確認ください。</li>
                        </ul>
                        <p class="login-top-read"><a href="/domain/">《必ずお読みください》ドメイン指定について</a></p>
                        <p class="login-top-detail">
                        当サイトは、エステティックサロンカルミア・カルミア美肌クリニックのご利用者様専用サイトです。過去にご来店・ご来院いただいた方もご利用可能です。詳しくはお客様サポートデスクまでお問い合わせください。
</p>
                    </form>
                </div>
            </div><!-- /#contents -->
            
        </article>
        <!--▲CONTENTS-->
  

