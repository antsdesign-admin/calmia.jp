<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/shopping.css" media="all" />

<!--{if $paygent_token_js_url}-->
  <script type="text/javascript" src="<!--{$paygent_token_js_url}-->" charset="UTF-8"></script>
<!--{/if}-->

<script type="text/javascript">//<![CDATA[
var send = false;

window.onunload=function onunloadCashClear() {
    if (send) {
        send = false;
        return false;
    } else {
        return false;
    }
}

window.onload=function onloadCashClear() {
    if (send) {
        send = false;
        return false;
    } else {
        return false;
    }
}

function fnCheckClearSubmit() {
    if(send) {
        alert("只今、処理中です。しばらくお待ち下さい。");
        return false;
    } else {
        send = true;
        
        document.form1.card_no01.removeAttribute('name');
        document.form1.card_no02.removeAttribute('name');
        document.form1.card_no03.removeAttribute('name');
        document.form1.card_no04.removeAttribute('name');
        document.form1.card_month.removeAttribute('name');
        document.form1.card_year.removeAttribute('name');
        document.form1.card_name02.removeAttribute('name');
        document.form1.card_name01.removeAttribute('name');

        if (document.form1.security_code != null) {
            document.form1.security_code.removeAttribute('name');
        }
        
        var element = document.createElement("input");
        element.name='deletecard';
        document.form1.appendChild(element);

        document.form1.submit();
    }
}

var merchant_id= <!--{$merchant_id}-->;
var token_key= "<!--{$token_key|h}-->";
var paygent_token_connect_url= "<!--{$paygent_token_connect_url}-->";

<!--{$token_js}-->

function startCreateToken() {

    var form = document.form1;
        
    //二重注文制御
    if(send) {
        alert("只今、処理中です。しばらくお待ち下さい。");
        return false;
    } else {

        send = true;
        
        //登録カード使用の場合
        if (form.stock != null && form.stock.checked == true) {
        
            //セキュリティーコード未使用時はトークン生成しない
            if (form.security_code == null) {
                form.submit();
                return true;
            }
            callCreateTokenCvc();
        } else {
            callCreateToken();
        }
    }
}

//]]>
</script>

<form method="post" action="<!--{$smarty.server.PHP_SELF|h}-->" name="form1">
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="mode" value="next">
<input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->">
<input type="hidden" name="card_token" value="">
<input type="hidden" name="card_token_stock" value="">


<div id="teaser-tit">
                <h2 class="teaser-titin">クレジットカード情報</h2>
            </div><!-- /#teaser-tit -->
            
            <p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>クレジットカード情報</p>
            
            <div id="contents">
                <div class="inner">
                
                    <div class="visible-pc">
                        <ul class="dexpress-process-list clearfix">
                            <li>お届け先の確認</li>
                            <li class="on">お支払い方法・お届け時間帯</li>
                            <li>入力内容のご確認</li>
                            <li>完了</li>
                        </ul>
                    </div>
                    <div class="section">
                        <form action="/shopping/confirm.html" method="post">
                            <div id="form-area">
                                <div class="section">
                                    <p class="credit-note">下記に必要事項を入力してください。<span class="fcred">※手続きをやり直す場合は、ブラウザや携帯の戻るボタンを使用せず、必ず下記の『戻る』ボタンを押してください。</span></p>
                                    <div class="crdit-table01">
                                        <table class="com-table01">
                                            <tr>
                                                <th class="w20per">支払い回数</th>
                                                <td>
                                                <!--{assign var=key1 value="payment_class"}-->

<select name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" style="<!--{$arrErr[$key1]|sfGetErrorColor}-->" >
<!--{html_options options=$arrPaymentClass selected=$arrForm[$key1].value}-->
</select><font color="#ff0000"><!--{$arrErr[$key1]}--></font>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

<!--{if $cnt_card >= 1}-->
<!--{assign var=key1 value="CardSeq"}-->
<!--{assign var=key2 value="stock"}-->
<font color="#ff0000"><!--{$arrErr[$key1]}--></font>
                                    <div class="crdit-box">
                                        <ul class="clearfix credit-box-list">
                                            <li>
                                                <label>
                                                <input type="checkbox" name="stock" value="1" <!--{if $arrForm[$key2].value == 1}-->checked<!--{/if}-->>
                                                    <span class="fs16">登録カードを利用する</span>
                                                </label>
                                            </li>
                                            <li>
                                                <p>登録カードをご利用の方は、カード情報の入力は不要です。<span class="fcred">※入力されても適用されませんのでご注意ください。</span></p>
                                            </li>
                                        </ul>
                                        <div class="credit-box-area clearfix">
                                            <div class="credit-box-in01">
                                                <table class="com-table01 fs16">
                                                    <tr>
                                                        <th class="w15per">選択</th>
                                                        <th class="w40per">カード番号</th>
                                                        <th class="w20per">有効期限</th>
                                                        <th class="w25per">カード名義</th>
                                                    </tr>
                                                     <!--{foreach name=cardloop from=$arrCardInfo item=card}-->
                                                    <tr>
                                                        <td class="tac">
                                                            <label>
                                                            <input type="radio" name="<!--{$key1}-->" value="<!--{$card[$key1]}-->" <!--{if $arrForm[$key1].value == $card[$key1]}-->checked<!--{/if}--> class="input-text form-bgred">
                                                            </label>
                                                        </td>
                                                        <td><!--{$card.CardNo}--></td>
                                                        <td><!--{$card.Expire|substr:0:2}-->月/<!--{$card.Expire|substr:2:4}-->年</td>
                                                        <td class="tac"><!--{$card.HolderName}--></td>
                                                    </tr>
                                                    <!--{/foreach}-->
                                                </table>
                                            </div>
                                            <div class="credit-box-in02">
                                                    <p> 
                                                    <!--{if $token_pay == 1}-->
                                                    <input type="submit" name="deletecard" onclick="fnCheckClearSubmit();return false" value="選択カードの削除">
                                                    <!--{else}-->
                                                    <input type="submit" name="deletecard" value="選択カードの削除">
                                                    <!--{/if}-->
                                                   
                                                     </p>
                                            </div>
                                        </div>
                                    </div>
                                     <!--{/if}-->
                                    <table class="com-table01 mt25 fs16">
                                        <tr>
                                            <th class="w20per">カード番号</th>
                                            <td>
                                                <p class="fcred credit-txt01 visible-ts">ご本人名義のカードをご使用ください。</p>
                                                <div class="credit-form">
  <!--{assign var=key1 value="card_no01"}-->
<!--{assign var=key2 value="card_no02"}-->
<!--{assign var=key3 value="card_no03"}-->
<!--{assign var=key4 value="card_no04"}-->
                                                    <p>
                                                    <input type="text" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" size="6" istyle="4" class="input-text form-bgred credit-form-car">-
<input type="text" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$arrForm[$key2].length}-->" size="6" istyle="4" class="input-text form-bgred credit-form-car">-
<input type="text" name="<!--{$key3}-->" value="<!--{$arrForm[$key3].value|h}-->" maxlength="<!--{$arrForm[$key3].length}-->" size="6" istyle="4" class="input-text form-bgred credit-form-car">-
<input type="text" name="<!--{$key4}-->" value="<!--{$arrForm[$key4].value|h}-->" maxlength="<!--{$arrForm[$key4].length}-->" size="6" istyle="4" class="input-text form-bgred credit-form-car">
</p>
                                                    <p class="fcred credit-txt01 visible-pc">ご本人名義のカードをご使用ください。</p>
                                                  
<font color="#ff0000"><!--{$arrErr[$key1]}--></font>
<font color="#ff0000"><!--{$arrErr[$key2]}--></font>
<font color="#ff0000"><!--{$arrErr[$key3]}--></font>
<font color="#ff0000"><!--{$arrErr[$key4]}--></font>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="w20per">有効期限</th>
                                            <td>
                                            <!--{assign var=key1 value="card_month"}-->
<!--{assign var=key2 value="card_year"}-->
                                                <div class="credit-form">
                                                   <select name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->">
<option value="">--</option>
<!--{html_options options=$arrMonth selected=$arrForm[$key1].value}-->
</select>月/
<select name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" maxlength="<!--{$arrForm[$key2].length}-->">
<option value="">--</option>
<!--{html_options options=$arrYear selected=$arrForm[$key2].value}-->
</select>年
                                                </div>
                                                <font color="#ff0000"><!--{$arrErr[$key1]}--></font>
<font color="#ff0000"><!--{$arrErr[$key2]}--></font>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="w20per">カード名義<br>（ローマ字）</th>
                                            <td>
                                            <!--{assign var=key2 value="card_name01"}-->
<!--{assign var=key1 value="card_name02"}-->
                                                <div class="credit-form">
                                                    <p class="mb05">名<input type="text" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" istyle="3" size="15" class="input-text form-bgred credit-form-name"></p>
                                                    <p>姓<input type="text" name="<!--{$key2}-->" value="<!--{$arrForm[$key2].value|h}-->" istyle="3" size="15"  class="input-text form-bgred credit-form-name"></p>
                                                </div>
                                                <font color="#ff0000"><!--{$arrErr[$key1]}--></font>
<font color="#ff0000"><!--{$arrErr[$key2]}--></font>
                                            </td>
                                        </tr>
                                        <!--{if $security_code == 1}-->
                                         <tr>
                                            <th class="w20per">セキュリティコード</th>
                                            <td>
                                                <div class="credit-form">
                                                   <input type="text" name="<!--{$key1}-->" value="<!--{$arrForm[$key1].value|h}-->" maxlength="<!--{$arrForm[$key1].length}-->" size="6" istyle="4">
                                                </div>
                                                <!--{assign var=key1 value="security_code"}-->
<font color="#ff0000"><!--{$arrErr[$key1]}--></font>
                                            </td>
                                        </tr><!--{/if}-->
                                       <!--{if $stock_flg == 1}-->
                                        <tr>
                                            <th class="w20per">カード登録</th>
                                            <td>
                                            <!--{assign var=key1 value="stock_new"}-->
                                                <div class="credit-form clearfix">
                                                    <label>
                                                       <input type="checkbox" name="stock_new" value="1" <!--{if $arrForm[$key1].value == 1}-->checked<!--{/if}-->>
                                                       <span>登録する</span>
                                                    </label>
                                                    <p class="credit-txt02">カードを登録しておくと、次回以降の購入時にカード情報入力が省略でき、大変便利です。<br>最大<!--{$smarty.const.CARD_STOCK_MAX}-->件まで登録できます。</p>
                                                </div>
                                            </td>
                                        </tr><!--{/if}-->
                                    </table>
                                    <div class="credit-in">
                                        <p>以上の内容で間違いなければ、下記「確定する」ボタンをクリックしてください。</p>
                                        <p class="fcred">※画面が切り替わるまで少々時間がかかる場合がございますが、そのままお待ちください。</p>
                                    </div>
                                </div>
                                <div class="combtn-top combtn-twocolumn">
                                    <ul class="clearfix">
                                        <li><input type="submit" value="カード決済を確定する" class="input-submit"></li>
                                        </form>
                                         <li><form action="./load_payment_module.php" method="post">
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="mode" value="return">
<input type="submit" value="戻る" class="input-button">
</form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!-- /#contents -->
            
        </article>

