<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
                  
                          
      
<div class="slider-pro largeshow">
<div class="sp-slides visible-pc">
   <ul id="carrousel" class="roundabout-holder" style="padding: 0px; position: relative; width: 1100px;">
        <li class="roundabout-moveable-item" style="opacity: 1; z-index: 249; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=185">
        <img src="<!--{$TPL_URLPATH}-->img/top/slide_top017_pc.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style="opacity: 1; z-index: 249; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=161">
        <img src="<!--{$TPL_URLPATH}-->img/top/slide_top01_pc.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style="opacity: 1; z-index: 249; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=187">
        <img src="<!--{$TPL_URLPATH}-->img/top/slide_top018_pc.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style="opacity: 1; z-index: 144; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=143"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top02_pc.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 102; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=4"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top03_pc.jpg" alt="">
        </a></li>
         <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 182; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=171"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top010_pc.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 273; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=34"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top04_pc.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style="opacity: 1; z-index: 249; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=188"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top019_pc.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style="opacity: 1; z-index: 249; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=184"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top016_pc.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style="opacity: 1; z-index: 144; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=157"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top06_pc.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style="opacity: 1; z-index: 144; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=182"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top014_pc.jpg" alt="">
        </a></li>
         <li class="roundabout-moveable-item" style="opacity: 1; z-index: 102; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=181"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top011_pc.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 182; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=10" target="_blank"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top08_pc.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 182; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=120"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top09_pc.jpg" alt="">
        </a></li>

        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 182; width: 550px;">
        <a href="/products/list.php?mode=series&series_id=183"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top015_pc.jpg" alt="">
        </a></li>
        
    </ul>
    <div class="box-ic">
    <img id="btnPrev" src="<!--{$TPL_URLPATH}-->img/top/arrow_l.png">
    <img id="btnNext" src="<!--{$TPL_URLPATH}-->img/top/arrow_r.png">
    </div>
    <ul id="pointer"></ul>
        </div>
<div class="sp-slides visible-ts">
   <ul id="carrousel-b" class="roundabout-holder" style="padding: 0px; position: relative; width: 320px;">
        <li class="roundabout-moveable-item" style="opacity: 1; z-index: 249; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=185"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top017_sp.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style="opacity: 1; z-index: 249; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=161"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top01_sp.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style="opacity: 1; z-index: 249; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=187"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top018_sp.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style="opacity: 1; z-index: 249; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=187"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top02_sp.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 102; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=4"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top03_sp.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 182; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=171"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top010_sp.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 273; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=34"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top04_sp.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 249; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=188"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top019_sp.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 249; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=184"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top016_sp.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 144; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=157"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top06_sp.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 144; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=182"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top014_sp.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 102; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=181"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top011_sp.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 182; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=10"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top08_sp.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 182; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=120"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top09_sp.jpg" alt="">
        </a></li>
        <li class="roundabout-moveable-item" style=" opacity: 1; z-index: 182; width: 300px;">
        <a href="/products/list.php?mode=series&series_id=183"><img src="<!--{$TPL_URLPATH}-->img/top/slide_top015_sp.jpg" alt="">
        </a></li>
    </ul>
    <img id="btnPrev-b" src="<!--{$TPL_URLPATH}-->img/top/arrow_l.png" width="30">
    <img id="btnNext-b" src="<!--{$TPL_URLPATH}-->img/top/arrow_r.png" width="30">
    <ul id="pointer-b"></ul>
        </div>
      </div>
      
      <div id="contents" class="sp-mb00">
        <div class="inner visible-pc">
          <div class="top-link">
            <ul class="top-link-list clearfix">
              <li class="hlg01">
                <div class="top-link-news">
                  <div class="top-link-newstop clearfix">
                    <p class="top-link-newstit">新着情報</p>
                    <p class="top-link-newsdetail"><a href="<!--{$smarty.const.ROOT_URLPATH}-->event/index.php">一覧を見る</a></p>
                  </div>
                  <div class="top-link-box">
                  <!--{include_php file=`$smarty.const.HTML_REALDIR`frontparts/bloc/news.php}-->
                  </div>
                </div>
              </li>
              <li class="hlg01">
                <div class="top-link-news">
                  <div class="top-link-newstop clearfix">
                    <p class="top-link-newstit">エステ スタッフブログ</p>
                    <p class="top-link-newsdetail"><a href="http://calmia.cc/calmia_blog/" target="_blank">一覧を見る</a></p>
                  </div>
                
                    <iframe src="//calmia.cc/calmia_blog/members_frame"　wide="347" height="148" scrolling="no" frameborder="0"></iframe>
                </div>
              </li>
              <li class="hlg01">
                <div class="top-link-news">
                  <div class="top-link-newstop clearfix">
                    <p class="top-link-newstit">院長の症例ブログ</p>
                    <p class="top-link-newsdetail"><a href="http://calmia-clinic.com/calmia_doctor/" target="_blank">一覧を見る</a></p>
                  </div>
                
                    <iframe src="//calmia-clinic.com/calmia_doctor/members_frame"　wide="347" height="148" scrolling="no" frameborder="0"></iframe>
                </div>
              </li>
              <li class="hlg01">
                <div class="top-link-news">
                  <div class="top-link-newstop clearfix">
                    <p class="top-link-newstit">クリニック スタッフブログ</p>
                    <p class="top-link-newsdetail"><a href="http://calmia-clinic.com/calmia_blog/" target="_blank">一覧を見る</a></p>
                  </div>
                
                    <iframe src="//calmia-clinic.com/calmia_blog/members_frame"　wide="347" height="148" scrolling="no" frameborder="0"></iframe>
                </div>
              </li>
            </ul>
            <ul class="side_ban">
              <li class="hlg01">
                <table class="top-link-table">
                  <tr>
                    <th><a href="/guide/order.php">ご注文ガイド</a></th>
                  </tr>
                  <tr>
                    <th><a href="/order/">特定商取引に関する表記</a></th>
                  </tr>
                  <tr>
                    <th><a href="/guide/about.php">会社概要</a></th>
                  </tr>
                  <tr>
                    <th><a href="/guide/privacy.php">個人情報保護方針</a></th>
                  </tr>
                  <tr>
                    <th><a href="/user_data/sitemap.php">サイトマップ</a></th>
                  </tr>
                </table>
              </li>
            </ul>
          </div>
        </div>
        <div class="visible-ts">
          <div class="visible-ts">
            <ul class="top-list-img clearfix">
              <li><a href="/products/"><img src="<!--{$TPL_URLPATH}-->/img/top/img_toplist01.jpg" alt="ショッピング"></a></li>
              <li><a href="/mypage/favorite.php"><img src="<!--{$TPL_URLPATH}-->/img/top/img_toplist02.jpg" alt="お気に入り"></a></li>
              <li><a href="/mypage/history_list.php"><img src="<!--{$TPL_URLPATH}-->/img/top/img_toplist03.jpg" alt="ご注文履歴"></a></li>
              <li><a href="/guide/order.php"><img src="<!--{$TPL_URLPATH}-->/img/top/img_toplist04.jpg" alt="ご注文ガイド"></a></li>
            </ul>
          </div>
          <div class="top-link">
            <ul class="top-link-list clearfix">
              <li class="w35per">
                <div class="top-link-news inner">
                  <div class="top-link-newstop clearfix">
                    <p class="top-link-newstit">新着情報</p>
                    <p class="top-link-newsdetail"><a href="<!--{$smarty.const.ROOT_URLPATH}-->event/index.php">一覧を見る</a></p>
                  </div>
                  <div class="top-link-box">
                    <!--{include_php file=`$smarty.const.HTML_REALDIR`frontparts/bloc/news.php}-->
                  </div>
                </div>
              </li>
              <li class="w35per">
                <div class="top-link-news inner">
                  <div class="top-link-newstop clearfix">
                    <p class="top-link-newstit">院長の症例ブログ</p>
                    <p class="top-link-newsdetail"><a href="http://calmia-clinic.com/calmia_doctor/" target="_blank">一覧を見る</a></p>
                  </div>
               
                   <iframe src="//calmia-clinic.com/calmia_doctor/?page_id=97"　wide="347" height="142" scrolling="no" frameborder="0"></iframe>
            
                </div>
              </li>
              <li class="w35per">
                <div class="top-link-news inner">
                  <div class="top-link-newstop clearfix">
                    <p class="top-link-newstit">エステ スタッフブログ</p>
                    <p class="top-link-newsdetail"><a href="http://calmia.cc/calmia_blog/" target="_blank">一覧を見る</a></p>
                  </div>
               
                   <iframe src="//calmia.cc/calmia_blog/members_frame_sp"　wide="347" height="142" scrolling="no" frameborder="0"></iframe>
            
                </div>
              </li>
               <li class="w35per">
                <div class="top-link-news inner">
                  <div class="top-link-newstop clearfix">
                    <p class="top-link-newstit">クリニック スタッフブログ</p>
                    <p class="top-link-newsdetail"><a href="http://calmia-clinic.com/calmia_blog/" target="_blank">一覧を見る</a></p>
                  </div>
               
                   <iframe src="//calmia-clinic.com/calmia_blog/members_frame_sp"　wide="347" height="142" scrolling="no" frameborder="0"></iframe>
            
                </div>
              </li>
            </ul>
          </div>
        </div>
        
      </div><!-- /#contents -->
      
  </article>
        