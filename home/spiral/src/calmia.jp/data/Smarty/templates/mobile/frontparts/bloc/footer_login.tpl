<footer>
	<div id="footer">
		<div class="footer-link visible-ts">
			<ul>
				<li><a href="/customer/change.html">お客様情報の変更</a></li>
				<li><a href="/article/index.html">特定商取引に関する表記</a></li>
				<li><a href="/company/index.html">会社概要</a></li>
				<li><a href="/personal/index.html">個人情報保護方針</a></li>
				<li><a href="/sitemap/index.html">サイトマップ</a></li>
			</ul>
		</div>
		<div class="footer-area">
			<div class="inner">
				<div class="footer-inner">
					<dl class="footer-box">
						<dt><a href="/index.html" class="op"><img src="<!--{$TPL_URLPATH}-->img/common/logo_footer01.png" width="100" alt="calmia"></a></dt>
						<dd>
							<div class="footer-in visible-pc">
								<p class="footer-txt01">【お問い合わせ】カルミアお客様サポートデスク</p>
								<p class="footer-tel01"><img src="<!--{$TPL_URLPATH}-->img/common/img_footer01.png" alt="0120863730"><span>9：30～18：00（土日祝・当社規定休日を除く）</span></p>
								<p class="footer-txt02">お電話でご注文いただいた場合、ポイントは付与されませんので予めご了承くださいませ。</p>
								<p class="footer-txt03"><a href="/guide/index.html">ご注文ガイド</a> ｜ <a href="/article/index.html">特定商取引に関する表記</a> ｜ <a href="/company/index.html">会社概要</a> ｜ <a href="/personal/index.html">個人情報保護方針</a> ｜ <a href="/sitemap/index.html">サイトマップ</a></p>
							</div>
							<div class="footer-in visible-ts">
								<p class="footersp-txt01">お問い合わせ</p>
								<div class="footer-panel">
									<p>カルミアお客様サポートデスク</p>
									<p><a href="tel:0120-86-3730"><img src="<!--{$TPL_URLPATH}-->img/common/img_footer01.png" alt="0120863730"></a></p>
								</div>
								<p class="footersp-txt02">9：30～18：00（土日祝・当社規定休日を除く）<br><span class="fcpink">お電話でご注文いただいた場合、<br>ポイントは付与されませんので予めご了承くださいませ。</span></p>
							</div>
						</dd>
					</dl>
				</div>
			</div>
		</div>
		<p id="copyright">copyright (c) calmia all rights reserved.</p>
	</div><!-- /#footer -->
	<p class="pagetop"><a href="#wrapper" class="op"><img src="<!--{$TPL_URLPATH}-->img/common/page_top.png" alt="pagetop"></a></p>
</footer>
</div><!-- /#wrapper -->
<div id="sidebar">
	<div class="sidebar-area">
		<p class="sidebar-logout"><a href="#">ログアウト</a></p>
		<div class="sidebar-box">
			<p class="sidebar-tit">スキンケア</p>
			<ul class="sidebar-list">
				<li><a href="#">&gt;&nbsp;クレンジング</a></li>
				<li><a href="#">&gt;&nbsp;洗顔</a></li>
				<li><a href="#">&gt;&nbsp;化粧水</a></li>
				<li><a href="#">&gt;&nbsp;美容液</a></li>
				<li><a href="#">&gt;&nbsp;クリーム</a></li>
				<li><a href="#">&gt;&nbsp;乳液</a></li>
				<li><a href="#">&gt;&nbsp;パック</a></li>
				<li><a href="#">&gt;&nbsp;UVケア</a></li>
				<li><a href="#">&gt;&nbsp;トライアル</a></li>
				<li><a href="#">&gt;&nbsp;スキンケア その他</a></li>
			</ul>
			<p class="sidebar-tit">メイク用品</p>
			<ul class="sidebar-list">
				<li><a href="#">&gt;&nbsp;メイクベース</a></li>
				<li><a href="#">&gt;&nbsp;ファンデーション</a></li>
				<li><a href="#">&gt;&nbsp;フェイスパウダー</a></li>
				<li><a href="#">&gt;&nbsp;アイブロウ</a></li>
				<li><a href="#">&gt;&nbsp;アイライナー</a></li>
				<li><a href="#">&gt;&nbsp;アイカラー</a></li>
				<li><a href="#">&gt;&nbsp;マスカラ</a></li>
				<li><a href="#">&gt;&nbsp;リップ</a></li>
				<li><a href="#">&gt;&nbsp;チーク</a></li>
				<li><a href="#">&gt;&nbsp;その他（ブラシ・パフ類）</a></li>
			</ul>
			<p class="sidebar-tit">ボディケア</p>
			<ul class="sidebar-list">
				<li><a href="#">&gt;&nbsp;ボディケア</a></li>
				<li><a href="#">&gt;&nbsp;バスグッズ</a></li>
				<li><a href="#">&gt;&nbsp;ヘアケア</a></li>
				<li><a href="#">&gt;&nbsp;その他</a></li>
			</ul>
			<p class="sidebar-tit">ウェア</p>
			<ul class="sidebar-list">
				<li><a href="#">&gt;&nbsp;補整下着・ガードル</a></li>
				<li><a href="#">&gt;&nbsp;ソックス</a></li>
				<li><a href="#">&gt;&nbsp;その他</a></li>
			</ul>
			<p class="sidebar-tit">インナーケア</p>
			<ul class="sidebar-list">
				<li><a href="#">&gt;&nbsp;サプリメント</a></li>
				<li><a href="#">&gt;&nbsp;ドリンク</a></li>
				<li><a href="#">&gt;&nbsp;その他</a></li>
			</ul>
			<p class="sidebar-tit">日用品・その他</p>
			<ul class="sidebar-list">
				<li><a href="#">&gt;&nbsp;洗剤</a></li>
				<li><a href="#">&gt;&nbsp;オーラルケア</a></li>
				<li><a href="#">&gt;&nbsp;ベビーセット</a></li>
				<li><a href="#">&gt;&nbsp;エッセンシャルオイル</a></li>
				<li><a href="#">&gt;&nbsp;シェーバー</a></li>
			</ul>
		</div>
	</div>
</div><!-- /#sidebar -->