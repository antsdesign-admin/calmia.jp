<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<!--▼CONTENTS-->
<!--{if $objSiteInfo->data.latitude && $objSiteInfo->data.longitude}-->
<script type="text/javascript">//<![CDATA[
$(function() {
    $("#maps").css({
        'margin-top': '15px',
        'margin-left': 'auto',
        'margin-right': 'auto',
        'width': '98%',
        'height': '300px'
    });
    var lat = <!--{$objSiteInfo->data.latitude}-->
    var lng = <!--{$objSiteInfo->data.longitude}-->
    if (lat && lng) {
        var latlng = new google.maps.LatLng(lat, lng);
        var mapOptions = {
            zoom: 15,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map($("#maps").get(0), mapOptions);
        var marker = new google.maps.Marker({map: map, position: latlng});
    } else {
        $("#maps").remove();
    }
});
//]]>
</script>
<!--{/if}-->


<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/company.css" media="all" />
            
            <div id="teaser-tit">
                <h2 class="teaser-titin">会社概要</h2>
            </div><!-- /#teaser-tit -->
            
            <p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>会社概要</p>
            
            <div id="contents">
                
                
                <div class="inner">
                    <div class="company-wrap clearfix">
                        <p class="company-img"><img src="<!--{$TPL_URLPATH}-->img/company/img_company.jpg" alt="会社概要"></p>
                        <div class="ftBox">
                            <table class="com-table02">
                                <tr>
                                    <th>社名</th>
                                    <td>スパイラル株式会社</td>
                                </tr>
                                <tr>
                                    <th>創業</th>
                                    <td>平成14年5月25日</td>
                                </tr>
                                <tr>
                                    <th>設立</th>
                                    <td>平成14年11月1日</td>
                                </tr>
                                <tr>
                                    <th>代表者</th>
                                    <td>池田 秀之</td>
                                </tr>
                                <tr>
                                    <th>所在地</th>
                                    <td>〒730-0029<br>広島県広島市中区三川町2-10</td>
                                </tr>
                                <tr>
                                    <th>連絡先</th>
                                    <td>TEL082-246-3333<br>FAX082-241-3355</td>
                                </tr>
                                <tr>
                                    <th class="top">URL</th>
                                    <td>
                                        <p class="mb20 wsn">エステティックサロン <br class="visible-ts">カルミア<a class="block" href="http://calmia.cc/" target="_blank">http://calmia.cc/</a></p>
                                        <p class="wsn">カルミア美肌クリニック<a class="block" href="http://calmia-clinic.com/" target="_blank">http://calmia-clinic.com/</a></p>
                                    </td>
                                </tr>
                                <tr>
                                    <th>サポート<br class="visible-ts">デスク</th>
                                    <td>0120-86-3730</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                
                
            </div><!-- /#contents -->
            
        </article>
        
