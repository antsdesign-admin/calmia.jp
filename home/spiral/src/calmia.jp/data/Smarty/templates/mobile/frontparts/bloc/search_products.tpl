<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

 <header>
    <div id="header">
        <div class="inner">
            <div class="header-in">

            <!--{if $smarty.session.customer}-->
<form name="search_form" id="search_form" method="get" action="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php" target="_parent">
  <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
  <input type="hidden" name="mode" value="search" />
<input type="image" src="<!--{$TPL_URLPATH}-->img/common/search.svg" width="18" height="18" alt="search" class="vam">
<input type="text" name="name" maxlength="50" value="<!--{$smarty.get.name|h}-->" />
  
</form>
<!--{else}-->
<!--{/if}-->
<div class="headersp-search">
            <div class="clearfix search-box">
                <form name="search_form" method="get" action="<!--{$smarty.const.ROOT_URLPATH}-->products/list.php" target="_parent">
  <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
  <input type="hidden" name="mode" value="search" />
                    <p class="search-input"><input type="text" name="name" maxlength="50" value="<!--{$smarty.get.name|h}-->" id="searchsp"></p>
                    <p class="search-sumbit"><input type="submit" value="検索"></p>
                </form>
            </div>
        </div>
       