<script type="text/javascript">
    $(function(){
        $( '.slider-pro' ).sliderPro({
            width: 950,
            height: 525,
            controls:true,
            visibleSize: '100%',
            arrows: true,
            buttons: true,
            autoSlideSize: true,
            slideDistance: 0,
            autoplayDelay: 5000,
            slideAnimationDuration: 1000,
            breakpoints: {
            959: {
                width: 386,
                height: 262,
            },
            479: {
                width: 268,
                height: 168,
            }
        }
        });
    });
</script>
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/slider-pro.min.css" media="all">
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/top.css" media="all">
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--メンバーサイトコーディング用 終了-->

<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link href="<!--{$TPL_URLPATH}-->css/slide-style.css" rel="stylesheet" media="screen">
<script src="<!--{$TPL_URLPATH}-->js/jquery.roundabout.js"></script>
    <script>
        $(function() {
            var container = $("#main-a");
            var carrousel = $("ul#carrousel");
            var pointer = $("ul#pointer");

            //カルーセル設定
            carrousel.roundabout({
               
                tilt:-10,
                minScale: -2,
                maxScale : 2,
                minOpacity: 1,
                minZ: 100,
                maxZ: 280,
                autoplay : true,
                autoplayDuration: 5000,
                autoplayPauseOnHover: false,
                btnNext : '#btnNext',
                btnPrev : '#btnPrev',
                clickToFocus: false,
                responsive:true,

                autoplayCallback: onMove,
                btnNextCallback: onMove,
                btnPrevCallback: onMove
            });

            //ポインタ生成
            var lis = new Array();
            for(var i=0; i<carrousel.children().size(); i++){
                lis[i] = $("<li></li>");
                lis[i].click(function(){
                    moveTo(pointer.children().index(this));
                });
                pointer.append(lis[i]);
            }
            onMove();

            function moveTo(index){
                carrousel.roundabout("animateToChild", index, onMove);
                onMove(index);
            }

            function onMove(index){
                var i = index || carrousel.roundabout("getChildInFocus");
                pointer.children().removeClass('active');
                pointer.children().eq(i).addClass('active');
                carrousel.roundabout("startAutoplay");
            }
        });
    </script>

    <script>
        $(function() {
            var container = $("#main-b");
            var carrousel = $("ul#carrousel-b");
            var pointer1 = $("ul#pointer-b");

            //カルーセル設定
            carrousel.roundabout({
                tilt:-3,
                minScale: -3,
                maxScale : 1,
                minOpacity: 1,
                minZ: 100,
                maxZ: 280,
                autoplay : true,
                autoplayDuration: 5000,
                autoplayPauseOnHover: false,
                btnNext : '#btnNext-b',
                btnPrev : '#btnPrev-b',
                clickToFocus: false,
                responsive:true,
                enableDrag:false,
                autoplayCallback: onMove,
                btnNextCallback: onMove,
                btnPrevCallback: onMove
            });

            //ポインタ生成
            var lis = new Array();
            for(var i=0; i<carrousel.children().size(); i++){
                lis[i] = $("<li></li>");
                lis[i].click(function(){
                    moveTo(pointer1.children().index(this));
                });
                pointer1.append(lis[i]);
            }
            onMove();

            function moveTo(index){
                carrousel.roundabout("animateToChild", index, onMove);
                onMove(index);
            }

            function onMove(index){
                var i = index || carrousel.roundabout("getChildInFocus");
                pointer1.children().removeClass('active');
                pointer1.children().eq(i).addClass('active');
                carrousel.roundabout("startAutoplay");
            }
        });
    </script>
