<!--{*
/*
* This file is part of EC-CUBE
*
* Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
*
* http://www.lockon.co.jp/
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
*}-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/shopping.css" media="all" />
<div class="cart-wrap">
    <div id="teaser-tit">
        <h2 class="teaser-titin"><span>現在のカゴの中</span></h2>
    </div><!-- /#teaser-tit -->

    <p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>現在のカゴの中</p>

    <div id="contents">
        <div class="inner">      
            <!--{* カゴの中に商品がある場合にのみ表示 *}-->
            <!--{if count($cartKeys) > 1}-->
                <!--{foreach from=$cartKeys item=key name=cartKey}--><!--{$arrProductType[$key]}--><!--{if !$smarty.foreach.cartKey.last}-->、<!--{/if}--><!--{/foreach}-->
                は同時購入できません。お手数ですが、個別に購入手続きをお願い致します。
                <!--{/if}-->

                <!--{if strlen($tpl_error) != 0}-->
                <font color="#FF0000"><!--{$tpl_error|h}--></font><br>
                <!--{/if}-->

                <!--{if strlen($tpl_message) != 0}-->
                <font color="#FF0000"><!--{$tpl_message|h|nl2br}--></font><br>
                <!--{/if}-->

            <!--{if count($cartItems) > 0}-->
            <!--{foreach from=$cartKeys item=key}-->
            <form name="form<!--{$key}-->" id="form<!--{$key}-->" method="post" action="<!--{$smarty.const.ROOT_URLPATH}-->cart/index.php"  utn>
                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                <input type="hidden" name="mode" value="confirm">
                <input type="hidden" name="cart_no" value="">
                <input type="hidden" name="cartKey" value="<!--{$key}-->">
                <!-- <div class="inner">
                <div class="visible-pc">
                <ul class="dexpress-process-list clearfix">
                <li class="on">お届け先の確認</li>
                <li>お支払い方法・お届け時間帯</li>
                <li>入力内容のご確認</li>
                <li>完了</li>
                </ul>
                </div>-->    
                <!--ご注文内容ここから-->    

                <div class="section">
                    <!--{if count($cartKeys) > 1}-->
                    <hr>
                    ■<!--{$arrProductType[$key]}-->
                    <hr>
                    <!--{/if}-->

                    <p class="cart-note">※表示価格は税抜です。</p>
                    <table class="com-table01">
                        <tr>
                            <th class="tac">ご注文詳細</th>
                            <th class="tac">個数</th>
                            <th class="tac">小計</th>
                            <th class="tac">&nbsp;</th>
                        </tr>
                        <!--{foreach from=$cartItems[$key] item=item}-->
                        <tr>
                            <td class="clearfix">
                                <p class="cart-img"><a href="<!--{$smarty.const.MOBILE_P_DETAIL_URLPATH}--><!--{$item.productsClass.product_id|u}-->"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$item.productsClass.main_image}-->" alt="<!--{* 商品名 *}--><!--{$item.productsClass.name|h}-->" height="100" /></a></p>
                                <p class="cart-txt"><a href="<!--{$smarty.const.MOBILE_P_DETAIL_URLPATH}--><!--{$item.productsClass.product_id|u}-->"><!--{* 商品名 *}--><!--{$item.productsClass.name|h}--><!--{* 規格名1 *}--><!--{if $item.productsClass.classcategory_name1 != ""}--><!--{$item.productsClass.class_name1}-->：<!--{$item.productsClass.classcategory_name1}--><br><!--{/if}--><!--{* 規格名2 *}--><!--{if $item.productsClass.classcategory_name2 != ""}--><!--{$item.productsClass.class_name2}-->：<!--{$item.productsClass.classcategory_name2}--><br><!--{/if}--></a></p>
                            </td>
                            <td> <!--{* 数量 *}--><p class="cart-num-panel"><span class="num"><!--{$item.quantity}--></span><span><a href="?mode=up&amp;cart_no=<!--{$item.cart_no}-->&amp;cartKey=<!--{$key}-->" class="add-input">＋</a>

                                </span><span><a href="?mode=down&amp;cart_no=<!--{$item.cart_no}-->&amp;cartKey=<!--{$key}-->">－</a></span></p></td>

                            <td><!--{* 販売価格 *}-->
                            <!--{if $arrSiteInfo.tax_flg eq 1}-->
                            <!--{$item.total_outtax|number_format}-->
                              <!--{else}-->
                            <!--{$item.total_inctax|number_format}-->
                             <!--{/if}-->円</td>
                            <td><a href="?mode=delete&amp;cart_no=<!--{$item.cart_no}-->&amp;cartKey=<!--{$key}-->">削除</a></td>
                        </tr>
                        <!--{/foreach}-->
                    </table>


                    <div class="cart-area clearfix">
                        <div class="cart-box-right">
                            <table class="com-table01">
                                <tr>
                                    <th>商品合計</th>
                                    <td class="tar"><!--{if $arrSiteInfo.tax_flg eq 1}--><!--{$tpl_total_outtax[$key]|number_format}--><!--{else}--><!--{$tpl_total_inctax[$key]|number_format}--><!--{/if}-->円</td>
                                </tr>
                                <!--{if $arrSiteInfo.tax_flg eq 1}-->
                                <tr>
                                    <th>消費税</th>
                                    <td class="tar"><!--{$arrData[$key].tax|number_format}-->円</td>
                                </tr>
                                <!--{/if}-->
                                <tr>
                                    <th class="bor">総計</th>
                                    <td class="tar bor"><!--{$arrData[$key].total-$arrData[$key].deliv_fee|number_format}-->円</td>
                                </tr> <!--{if $smarty.const.USE_POINT !== false}-->
                                <!--{if $arrData[$key].birth_point > 0}-->
                                <tr>
                                    <th class="bor">お誕生月ﾎﾟｲﾝﾄ</th>
                                    <td class="tar bor"><!--{$arrData[$key].birth_point|number_format}-->pt</td>
                                </tr>
                                <!--{/if}-->  
                                <tr>
                                    <th class="bor">今回加算ポイント</th>
                                    <td class="tar bor"><!--{$arrData[$key].add_point|number_format}-->pt</td>
                                </tr>
                                <!--{/if}-->
                            </table>
                        </div>
                            <!--{if $key != $smarty.const.PRODUCT_TYPE_DOWNLOAD}-->
                                <!--{if $arrInfo.free_rule > 0}-->
                                    <!--{if !$arrData[$key].is_deliv_free}-->
                                <div class="cart-txt-left">
                                            <p class="cart-spe">あと<font color="#FF0000"><!--{$tpl_deliv_free[$key]|number_format}-->円</font>で<font color="#FF0000">送料無料</font>です！！</p>
                                        <!--{else}-->
                                            <p class="cart-spe">現在<span class="fcred">送料無料</span>です</p>
                                        <!--{/if}-->

                                    <!--{/if}-->
                                <!--{/if}-->

                                 <!--{if $smarty.const.USE_POINT !== false}-->    
                                <!--{if $tpl_login}-->
                                    <!--{$tpl_name|h}--> 様の、現在の所持ポイントは「<font color="#FF0000"><!--{$tpl_user_point|number_format|default:0}--> pt</font>」です。<br>
                                <!--{else}-->
                                    ポイント制度をご利用になられる場合は、会員登録後ログインしてくださいますようお願い致します。
                                <!--{/if}-->
                                ポイントは商品購入時に1ptを<!--{$smarty.const.POINT_VALUE}-->円として使用することができます。
                            <!--{/if}-->

                               </div> 
                    </div> 
                  
 <div class="combtn-top combtn-twocolumn">
                        <ul class="clearfix">
                            
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/index.php?<!--{$smarty.const.SID}-->" class="input-button">お買い物を続ける</a></li>
                            <li>
                                <!--{if $is_purchase_limit}-->
                                この商品はすでにご購入済みであるため、購入することができません
                                <!--{else}-->
                                <input type="submit" value="レジに進む" class="input-submit">
                                <!--{/if}-->
                            </li>
                        </ul>
                    </div>
                    <br>
                </div>
            </form>  

             <!--{/foreach}-->
                            <!--{else}-->
                            現在カゴの中に商品はございません。<br>
                       
                  
                    <div class="combtn-top combtn-onecolumn">
                        <ul class="clearfix">
                            <li><a href="<!--{$smarty.const.ROOT_URLPATH}-->products/index.php?<!--{$smarty.const.SID}-->" class="input-button">お買い物を続ける</a></li>
                        </ul>
                    </div>
                  
                </div>
       <br>  <br>
                            <!--{/if}-->

                           
                  
          </div> 

    </div><!-- /#contents -->


    </article>
