<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}--><link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/shopping.css" media="all" />
  
  <div id="teaser">
                     <div class="teaserin">
<!--{if count($arrRelativeCat) > 0}-->
<!--{if $arrRelativeCat.0.0.category_id == 185}-->
<h2 class="teaser-h"><span>ショッピング</span>除菌ジェル</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser17_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 161}-->
<h2 class="teaser-h"><span>ショッピング</span>アンペリアル</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser00_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 187}-->
<h2 class="teaser-h"><span>ショッピング</span>フルリ</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser18_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 143 || $arrRelativeCat.0.0.category_id == 144 || $arrRelativeCat.0.0.category_id == 145}-->
<h2 class="teaser-h"><span>ショッピング</span>ドクターリセラ</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser02_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 4 || $arrRelativeCat.0.0.category_id == 38  || $arrRelativeCat.0.0.category_id == 39  || $arrRelativeCat.0.0.category_id == 96  || $arrRelativeCat.0.0.category_id == 139  || $arrRelativeCat.0.0.category_id == 41  || $arrRelativeCat.0.0.category_id == 132  || $arrRelativeCat.0.0.category_id == 138  || $arrRelativeCat.0.0.category_id == 164  || $arrRelativeCat.0.0.category_id == 150  || $arrRelativeCat.0.0.category_id == 163  || $arrRelativeCat.0.0.category_id == 149 }-->
<h2 class="teaser-h"><span>ショッピング</span>アクアヴィーナス</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser03_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 79|| $arrRelativeCat.0.0.category_id == 34 || $arrRelativeCat.0.0.category_id == 83  || $arrRelativeCat.0.0.category_id == 109  || $arrRelativeCat.0.0.category_id == 109}-->
<h2 class="teaser-h"><span>ショッピング</span>ナチュリスティー</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser04_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 188 || $arrRelativeCat.0.0.category_id == 190 || $arrRelativeCat.0.0.category_id == 191  || $arrRelativeCat.0.0.category_id == 192  || $arrRelativeCat.0.0.category_id == 193}-->
<h2 class="teaser-h"><span>ショッピング</span>ヴィプランツ</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser19_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 119}-->
<h2 class="teaser-h"><span>ショッピング</span>ミラクルトックス</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser13_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 184}-->
<h2 class="teaser-h"><span>ショッピング</span>スピケア</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser16_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 157  || $arrRelativeCat.0.0.category_id == 158  || $arrRelativeCat.0.0.category_id == 160  || $arrRelativeCat.0.0.category_id == 159}-->
<h2 class="teaser-h"><span>ショッピング</span>エステプロラボ</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser06_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 182}-->
<h2 class="teaser-h"><span>ショッピング</span>サプリメント</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser14_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 33}-->
<h2 class="teaser-h"><span>ショッピング</span>ラクティス</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser07_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 10  || $arrRelativeCat.0.0.category_id == 48}-->
<h2 class="teaser-h"><span>ショッピング</span>カリス</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser08_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 120}-->
<h2 class="teaser-h"><span>ショッピング</span>シェーバー</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser09_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 171}-->
<h2 class="teaser-h"><span>ショッピング</span>リセラディーヴァ</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser11_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 172}-->
<h2 class="teaser-h"><span>ショッピング</span>その他</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser10_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 181  || $arrRelativeCat.0.0.category_id == 194  || $arrRelativeCat.0.0.category_id == 195}-->
<h2 class="teaser-h"><span>ショッピング</span>ソーアディクテッド</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser12_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrRelativeCat.0.0.category_id == 183}-->
<h2 class="teaser-h"><span>ショッピング</span>訳あり商品</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser15_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{else}-->
<h2 class="teaser-h"><span>ショッピング</span><!--{$tpl_title|h}--></h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser_other_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{/if}-->
<!--{/if}-->
</p>
        </div>
      </div><!-- /#teaser -->
     
     <p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span><a href="/products/">ショッピング</a><span>&gt;</span><a href="/products/list.php?mode=series&series_id=<!--{$arrRelativeCat.0.0.category_id}-->"><!--{if $tpl_subtitle|strlen >= 1}--><!--{$tpl_subtitle|h}--><!--{elseif $tpl_title|strlen >= 1}--><!--{$tpl_title|h}--><!--{/if}--></a><span>&gt;</span><!--{$arrProduct.name|h}--></p>
      
      <div id="contents">
        <div class="section">
          <div class="inner">
            <form action="#">
              <div class="detail-wrap">
                <div class="section">
                  <table class="detail-hdl">
                    <tr>
                      <th><!--{$arrProduct.name|h}--></th>
                      <td class="pr">
                      <!--{assign var=add_favorite value="add_favorite`$product_id`"}-->
                          <!--{if !$is_favorite}-->
                               <a class="like" href="<!--{$smarty.server.PHP_SELF}-->?product_id=<!--{$arrProduct.product_id|h}-->&amp;mode=add_favorite&amp;favorite_product_id=<!--{$arrProduct.product_id|h}-->" >お気に入りに追加する<img src="<!--{$TPL_URLPATH}-->img/shopping/detail_star.png" alt="お気に入りに追加する"></a><br />
                          <!--{else}-->
                              <a class="like" href="<!--{$smarty.server.PHP_SELF}-->?product_id=<!--{$arrProduct.product_id|h}-->&amp;mode=remove_favorite&amp;favorite_product_id=<!--{$arrProduct.product_id|h}-->" >お気に入りから削除する<img src="<!--{$TPL_URLPATH}-->img/shopping/detail_star_on.png" alt="お気に入りから削除する"></a><br />
                        <!--{/if}-->
                      </td>
                    </tr>
                  </table>
                  <div class="detail-box clearfix">
                    <p class="detail-left">
                    <!--★商品画像★-->
                    <!--{if $smarty.get.image != ''}-->
                      <!--{assign var=key value="`$smarty.get.image`"}-->
                    <!--{else}-->
                      <!--{assign var=key value="main_image"}-->
                    <!--{/if}-->
                    <img src="<!--{$arrFile[$key].filepath}-->" alt="<!--{$arrProduct.name|h}-->">
                    </p>
                    <div class="detail-list">
                      <dl class="clearfix">
                        <dt>商品No.</dt>
                        <dd><!--{if $arrProduct.product_code_min == $arrProduct.product_code_max}--><!--{$arrProduct.product_code_min|h}--><!--{else}--><!--{$arrProduct.product_code_min|h}-->～<!--{$arrProduct.product_code_max|h}--><!--{/if}--></dd>
                      </dl>
                      <dl class="clearfix">
                        <dt><!--{$smarty.const.SALE_PRICE_TITLE}--></dt>
                        <dd><!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
                              <!--{if $arrSiteInfo.tax_flg eq 1}-->
                                  <!--{$arrProduct.price02_min|number_format}-->
                              <!--{else}-->
                              <!--{$arrProduct.price02_min|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->
                              <!--{/if}-->
                          <!--{else}-->
                              <!--{if $arrSiteInfo.tax_flg eq 1}-->
                                  <!--{$arrProduct.price02_min|number_format}-->～<!--{$arrProduct.price02_max|number_format}-->
                              <!--{else}-->
                              <!--{$arrProduct.price02_min|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->～<!--{$arrProduct.price02_max|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->
                              <!--{/if}-->
                          <!--{/if}-->
                          円<!--{if $arrSiteInfo.tax_flg eq 1}-->(税抜)<!--{else}-->(税込)<!--{/if}--><br>

                          <!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
                            <!--{$arrProduct.price02_min|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->
                          <!--{else}-->
                            <!--{$arrProduct.price02_min|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->～<!--{$arrProduct.price02_max|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->
                          <!--{/if}-->
                          円(税込)
                        </dd>
                      </dl>
                      <!--{if $arrProduct.volume_value > 0}-->
                      <!--{assign var=volume_unit value=$arrProduct.volume_unit}-->
                      <!--★内容量★-->
                      <dl class="clearfix">
                        <dt>内容量</dt>
                        <dd> <!--{$arrProduct.volume_value|h}--><!--{$arrVolumeUnit[$volume_unit]|h}--></dd>
                      </dl><!--{/if}-->
                      <!--{if $smarty.const.USE_POINT !== false}-->
                      <dl class="clearfix">
                          <dt>ポイント</dt>
                          <dd><!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
                            <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate:$smarty.const.POINT_RULE:$arrProduct.product_id|number_format}-->
                        <!--{else}-->
                            <!--{if $arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate:$smarty.const.POINT_RULE:$arrProduct.product_id == $arrProduct.price02_max|sfPrePoint:$arrProduct.point_rate:$smarty.const.POINT_RULE:$arrProduct.product_id}-->
                                <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate:$smarty.const.POINT_RULE:$arrProduct.product_id|number_format}-->
                            <!--{else}-->
                                <!--{$arrProduct.price02_min|sfPrePoint:$arrProduct.point_rate:$smarty.const.POINT_RULE:$arrProduct.product_id|number_format}-->～<!--{$arrProduct.price02_max|sfPrePoint:$arrProduct.point_rate:$smarty.const.POINT_RULE:$arrProduct.product_id|number_format}-->
                            <!--{/if}-->
                        <!--{/if}-->
                      pt</dd>
                     </dl><!--{/if}-->
                     <dl class="detail-quanty clearfix">
                      <form name="form1" method="post" action="<!--{$smarty.server.PHP_SELF|h}-->?product_id=<!--{$tpl_product_id}-->">
                      <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />

                      <!--{if $is_purchase_limit}-->
                       </dl>
                         <font color="#FF0000">この商品はすでにご購入済みであるため、購入することができません</font>
                        </div>
                  </div>
                </div>
                      <!--{else}-->
                        <!--{if $tpl_stock_find}-->
                            <!--{* 規格がない場合はカゴに入れるボタン表示 *}-->
                            <!--{if $tpl_classcat_find1 || $tpl_classcat_find2}-->
                              <input type="hidden" name="mode" value="select">
                              <input type="submit" name="select" id="cart" value="この商品を選ぶ">
                            <!--{else}-->
                            <!--{if $arrErr.quantity != ""}-->
                                <font color="#FF0000">※数量を入力して下さい｡</font>
                            <!--{/if}-->      
                                         
                                <dt>数量</dt>
                                <dd><input type="number" name="quantity" class="quantly" size="3" value="<!--{$arrForm.quantity.value|default:1|h}-->" maxlength=<!--{$smarty.const.INT_LEN}--> istyle="4">
                                    <input type="hidden" name="mode" value="cart">
                                    <input type="hidden" name="product_id" value="<!--{$tpl_product_id}-->">
                                    <input type="hidden" name="classcategory_id1" value="0">
                                    <input type="hidden" name="classcategory_id2" value="0">
                                    <input type="hidden" name="product_class_id" value="<!--{$tpl_product_class_id}-->">
                                    <input type="hidden" name="product_type" value="<!--{$tpl_product_type}-->">
                                </dd>
                       </dl>
                    </div>
                    <div class="detail-basket clearfix">
                      <p class="detail-btn"><img src="<!--{$TPL_URLPATH}-->img/shopping/ico_detail.png" alt=""><input type="submit" name="submit" value="カゴに入れる"></span></p>
                      
                    </div>
                  </div>
                </div>
                <!--{/if}-->

    <!--{else}-->
    <font color="#FF0000">申し訳ございませんが､只今品切れ中です｡</font>
    <!--{/if}-->
<!--{/if}-->
</form>
                <div class="section clearfix">
                  <p class="detail-txt">＜商品詳細＞<br>
                  <!--{$arrProduct.main_comment|nl2br_html}--></p>
                </div>
              
                
                
                <div class="combtn-top combtn-twocolumn">
                  <ul class="clearfix">
                    <li><a href="" onclick="javascript:window.history.back(-1);return false;" class="input-submit pass">一覧に戻る</a></li>
                    <li><a href="/products/" class="input-button">ショッピングに戻る</a></li>
                  </ul>
                </div>
              </div>
            </form>
          </div>
        </div>
        
        
      </div><!-- /#contents -->
      
    </article>
    