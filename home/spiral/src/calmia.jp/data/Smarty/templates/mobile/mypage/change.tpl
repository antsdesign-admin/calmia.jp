<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}--><link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/customer.css" media="all" />
<script src="https://zipaddr.com/js/zipaddrx.js" charset="UTF-8"></script>
<script>if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
    $('input').attr('autocomplete', 'off');
}</script>
<script>
    $(function(){
   $('.required').on('keydown keyup keypress change focus blur', function(){
       if($(this).val() == ''){
           $(this).css({backgroundColor:'#ffcccc'});
       } else {
           $(this).css({backgroundColor:'#fff'});
       }
   }).change();
});
</script>
<style>
    input:-webkit-autofill {
    -webkit-box-shadow: 0 0 0px 1000px #fff inset;
}
</style>
			<div id="teaser-tit">
				<h2 class="teaser-titin">お客様情報の変更</h2>
			</div><!-- /#teaser-tit -->
			
			<p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>お客様情報の変更</p>
			
			<div id="contents">
				
				<div class="inner">
					<div class="change-wrap">
						<p class="fs16">以下のフォームより変更情報を記入の上登録してください｡<br>※ 【<span class="fcred">※</span>】は入力必須項目です｡それ以外の項目につきましては差し支えない範囲でご入力ください｡</p>
						<form name="form1" method="post" action="<!--{$smarty.const.ROOT_URLPATH}-->mypage/change.php">
						<input type="hidden" name="mode" value="confirm" />
    					<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
							<div id="form-area">
								<table class="com-table01">
									<tr>
										<th>お客様ID<span class="fcred">※</span></th>
										<td><input type="text" name="user_id" value="<!--{$arrForm.user_id|h}-->" class="w-max required">
											<p class="fcgray fs12 mt05">ご希望のIDを英数字でご入力ください</p>
											<font color="#FF0000"><!--{$arrErr.user_id}--></font>
										</td>
									</tr>
									<tr>
										<th class="vat">パスワード<span class="fcred">※</span></th>
										<td>
										<!--{assign var="size" value="`$smarty.const.PASSWORD_MAX_LEN+2`"}-->
	<input type="password" name="password" value="<!--{$arrForm.password}-->" istyle="4" maxlength="<!--{$smarty.const.PASSWORD_MAX_LEN}-->" size="<!--{$size}-->" class="w-max required">
											<p class="fcgray mt05 fs12">ご希望のパスワードを英数字でご入力ください（半角英数字<!--{$smarty.const.PASSWORD_MIN_LEN}-->文字以上<!--{$smarty.const.PASSWORD_MAX_LEN}-->文字以内））<br>
											<font color="#FF0000"><!--{$arrErr.password}--></font></p>
										</td>
									</tr>
									<tr>
										<th>パスワード<br>確認用の質問<span class="fcred">※</span></th>
										<td>
											<select name="reminder" class="w150 required">
												<option>▼選択してください</option>
												<!--{html_options options=$arrReminder selected=$arrForm.reminder}-->
											</select>
											<font color="#FF0000"><!--{$arrErr.reminder}--></font>
										</td>
									</tr>
									<tr>
										<th>質問の答え<span class="fcred">※</span></th>
										<td><input type="text" name="reminder_answer" value="<!--{$arrForm.reminder_answer|h}-->" class="w-max required">
										<br><font color="#FF0000"><!--{$arrErr.reminder_answer}--></font></td>
									</tr>
								</table>
								
								<table class="com-table01">
									<tr>
										<th>お名前（姓）<span class="fcred">※</span></th>
										<td><input type="text" name="name01" value="<!--{$arrForm.name01|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" class="w-max required"><br>
										<font color="#FF0000"><!--{$arrErr.name01}--></font></td>
									</tr>
									<tr>
										<th>お名前（名）<span class="fcred">※</span></th>
										<td><input type="text" name="name02" value="<!--{$arrForm.name02|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" class="w-max required"><br><font color="#FF0000"><!--{$arrErr.name02}--></font></td>
									</tr>
									<tr>
										<th>フリガナ（姓）<span class="fcred">※</span></th>
										<td><input type="text" name="kana01" value="<!--{$arrForm.kana01|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" class="w-max required"><br>
										<font color="#FF0000"><!--{$arrErr.kana01}--></font></td>
									</tr>
									<tr>
										<th>フリガナ（名）<span class="fcred">※</span></th>
										<td><input type="text" name="kana02" value="<!--{$arrForm.kana02|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" class="w-max required"><br>
										<font color="#FF0000"><!--{$arrErr.kana02}--></font></td>
									</tr>
									<tr>
										<th>旧姓</th>
										<td><input type="text" name="oldname01" value="<!--{$arrForm.oldname01|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" class="w-max"><br>
										<font color="#FF0000"><!--{$arrErr.oldname01}--></font></td>
									</tr>
									<tr>
										<th>旧姓フリガナ</th>
										<td><input type="text" name="oldkana01" value="<!--{$arrForm.oldkana01|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" class="w-max"><br>
										<font color="#FF0000"><!--{$arrErr.oldkana01}--></font></td>
									</tr>
									<tr>
										<th>生年月日<span class="fcred">※</span></th>
										<td>
											<ul class="form-date">
												<li>
													<select name="year" style="height: 40px;" class="required">
														<!--{html_options options=$arrYear selected=$arrForm.year}-->
													</select>年
												</li>
												<li>
													<select name="month" style="height: 40px;" class="required">
														<!--{html_options options=$arrMonth selected=$arrForm.month}-->
													</select>月
												</li>
												<li>
													<select name="day" style="height: 40px;" class="required">
														<!--{html_options options=$arrDay selected=$arrForm.day}-->
													</select>日
												</li>
											</ul>
											<font color="#FF0000"><!--{$arrErr.year}--><!--{$arrErr.month}--><!--{$arrErr.day}--></font>
										</td>
									</tr>
									<tr>
										<th class="vat">ご住所<span class="fcred">※</span></th>
										<td><!--{assign var=key1 value="zip01"}-->
	<!--{assign var=key2 value="zip02"}-->
	<!--{assign var="size1" value="`$smarty.const.ZIP01_LEN+2`"}-->
	<!--{assign var="size2" value="`$smarty.const.ZIP02_LEN+2`"}-->
											<p class="form-address">〒
											<input size="<!--{$size1}-->" type="text" name="zip01" value="<!--{if $arrForm.zip01 == ""}--><!--{$arrOtherDeliv.zip01|h}--><!--{else}--><!--{$arrForm.zip01|h}--><!--{/if}-->" maxlength="<!--{$smarty.const.ZIP01_LEN}-->" istyle="4" class="required" id="zip" placeholder="123" style="height: 30px;">
	&nbsp;-&nbsp;
	<input size="<!--{$size2}-->" type="text" name="zip02" value="<!--{if $arrForm.zip02 == ""}--><!--{$arrOtherDeliv.zip02|h}--><!--{else}--><!--{$arrForm.zip02|h}--><!--{/if}-->" maxlength="<!--{$smarty.const.ZIP02_LEN}-->" istyle="4" class="required" id="zip1" placeholder="4567" style="height: 30px;">
	 <span class="fcblue"><a href="http://www.post.japanpost.jp/zipcode/" target="_blank">郵便番号検索</a></span><br><span class="mini">郵便番号を入力すると自動で住所が表示されます。</span>
	
	<font color="#FF0000"><!--{$arrErr[$key1]}--><!--{$arrErr[$key2]}--></font>
	</p>
											<select name="pref" id="pref" class="mt15 w150　required">
												<option>▼都道府県を選択</option>
												<!--{html_options options=$arrPref selected=$arrForm.pref}-->
											</select>
											<font color="#FF0000"><!--{$arrErr.pref}--></font>
											<p class="mt15"><span class="fs12">市町村区番地（例：○○市○○区○○町1-2）</span>
											<input type="text" id="city" name="addr01" value="<!--{$arrForm.addr01|h}-->" class="w-max required">
											<font color="#FF0000"><!--{$arrErr.addr01}--></font>
											</p>
											<p class="mt15"><span class="fs12">ビル・マンション名（例：〇〇マンション 102号室）</span>
											<input type="text" name="addr02" value="<!--{$arrForm.addr02|h}-->" class="w-max"></p>
											<p class="mt15 fcred fs12">住所は2つに分けてご記入ください。マンション名は必ず記入してください。<br>
											<font color="#FF0000"><!--{$arrErr.addr02}--></font></p>
										</td>
									</tr>
									<tr>
										<th>携帯番号<span class="fcred">※</span></th>
										<td><p class="form-tel">
										<!--{assign var="size" value="`$smarty.const.TEL_ITEM_LEN+2`"}-->
	<input type="text" size="<!--{$size}-->" name="cell01" value="<!--{$arrForm.cell01|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" istyle="4" class="required">
	&nbsp;-&nbsp;
	<input type="text" size="<!--{$size}-->" name="cell02" value="<!--{$arrForm.cell02|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" istyle="4" class="required">
	&nbsp;-&nbsp;
	<input type="text" size="<!--{$size}-->" name="cell03" value="<!--{$arrForm.cell03|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" istyle="4" class="required"><br>
<font color="#FF0000"><!--{$arrErr.cell01}--><!--{$arrErr.cell02}--><!--{$arrErr.cell03}--></font>
</p></td>
									</tr>
									<tr>
										<th>電話番号</th>
										<td><p class="form-tel"><!--{assign var="size" value="`$smarty.const.TEL_ITEM_LEN+2`"}-->
	<input type="text" size="<!--{$size}-->" name="tel01" value="<!--{$arrForm.tel01|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" istyle="4">
	&nbsp;-&nbsp;
	<input type="text" size="<!--{$size}-->" name="tel02" value="<!--{$arrForm.tel02|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" istyle="4">
	&nbsp;-&nbsp;
	<input type="text" size="<!--{$size}-->" name="tel03" value="<!--{$arrForm.tel03|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" istyle="4"><br>
	<font color="#FF0000"><!--{$arrErr.tel01}--><!--{$arrErr.tel02}--><!--{$arrErr.tel03}--></font></p></td>
									</tr>
									<tr>
										<th class="vat">メールアドレス<span class="fcred">※</span></th>
										<td>
											<p class="mt10"><input type="text" name="email" value="<!--{$arrForm.email|h}-->" class="w-max required">
											</p>
											<font color="#FF0000"><!--{$arrErr.email}--></font></p>
										</td>
									</tr>
								</table>
								
								
							</div>
							
							<div class="change-check">
									<div class="change-check-box">
									<p class="change-check-txt visible-pc">お得な情報を希望されますか？<br>カープの無料チケットプレゼントやお得なキャンペーン企画なども配信しています。<br></p>
									<input type="hidden" name="mailmaga_flg" value="3">
										<p class="visible-ts">配信許可</p>
										<p><input type="checkbox" name="mailmaga_flg" value="2" <!--{if  $arrForm.mailmaga_flg == 2}-->checked<!--{/if}--> /></p>
										<p class="visible-pc">配信許可</p>
									</div>
								<p class="change-check-txt visible-ts">お得な情報を希望されますか？カープの無料チケットプレゼントやお得なキャンペーン企画なども配信しています。</p>
							</div>
							<input type="hidden" name="directmail_flg" value="<!--{$arrForm.directmail_flg|h}-->">
							<div class="combtn-top combtn-onecolumn">
								<ul class="clearfix">
									<li><input type="submit" name="submit" class="input-submit pass" value="入力内容を確認する"></p>
								<!--{foreach from=$list_data key=key item=item}-->
		<input type="hidden" name="<!--{$key|h}-->" value="<!--{$item|h}-->">
	<!--{/foreach}--></li>
								</ul>
							</div>


							
						</form>
					</div>
				
				
				</div>
				
				
			</div><!-- /#contents -->
			
		</article>