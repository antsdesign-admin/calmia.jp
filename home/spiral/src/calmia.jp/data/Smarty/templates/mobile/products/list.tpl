<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/shopping.css" media="all" />
    <div id="teaser">
                <div class="teaserin">
<!--{if $arrSearchData.category_id == 185}--><!--除菌ジェル-->
<h2 class="teaser-h"><span>ショッピング</span>除菌ジェル</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser17_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 161}--><!--アンペリアル-->
<h2 class="teaser-h"><span>ショッピング</span>アンペリアル</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser00_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 187}--><!--フルリ-->
<h2 class="teaser-h"><span>ショッピング</span>フルリ</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser18_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 143 || $arrSearchData.category_id == 144 || $arrSearchData.category_id == 145}-->
<h2 class="teaser-h"><span>ショッピング</span>ドクターリセラ</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser02_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 4 || $arrSearchData.category_id == 38  || $arrSearchData.category_id == 39  || $arrSearchData.category_id == 96  || $arrSearchData.category_id == 139  || $arrSearchData.category_id == 41  || $arrSearchData.category_id == 132  || $arrSearchData.category_id == 138  || $arrSearchData.category_id == 164  || $arrSearchData.category_id == 150  || $arrSearchData.category_id == 163  || $arrSearchData.category_id == 149  || $arrSearchData.category_id == 40  || $arrSearchData.category_id == 164 }-->
<h2 class="teaser-h"><span>ショッピング</span>アクアヴィーナス</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser03_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 79|| $arrSearchData.category_id == 34 || $arrSearchData.category_id == 83  || $arrSearchData.category_id == 109  || $arrSearchData.category_id == 109}-->
<h2 class="teaser-h"><span>ショッピング</span>ナチュリスティー</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser04_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 188|| $arrSearchData.category_id == 190 || $arrSearchData.category_id == 191  || $arrSearchData.category_id == 192  || $arrSearchData.category_id == 193}-->
<h2 class="teaser-h"><span>ショッピング</span>ヴィプランツ</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser19_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 184}-->
<h2 class="teaser-h"><span>ショッピング</span>スピケア</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser16_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 157  || $arrSearchData.category_id == 158  || $arrSearchData.category_id == 160  || $arrSearchData.category_id == 159 || $arrSearchData.category_id == 173}-->
<h2 class="teaser-h"><span>ショッピング</span>エステプロラボ</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser06_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 182}-->
<h2 class="teaser-h"><span>ショッピング</span>サプリメント</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser14_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 181  || $arrSearchData.category_id == 194  || $arrSearchData.category_id == 195}-->
<h2 class="teaser-h"><span>ショッピング</span>ソーアディクテッド</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser12_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 33}-->
<h2 class="teaser-h"><span>ショッピング</span>ラクティス</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser07_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 10  || $arrSearchData.category_id == 48}-->
<h2 class="teaser-h"><span>ショッピング</span>カリス</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser08_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 120}-->
<h2 class="teaser-h"><span>ショッピング</span>シェーバー</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser09_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 171}-->
<h2 class="teaser-h"><span>ショッピング</span>リセラディーヴァ</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser11_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 172  || $arrSearchData.category_id == 180 || $arrSearchData.category_id == 179 || $arrSearchData.category_id == 178 || $arrSearchData.category_id == 177 || $arrSearchData.category_id == 176 || $arrSearchData.category_id == 175 || $arrSearchData.category_id == 174}-->
<h2 class="teaser-h"><span>ショッピング</span>その他</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser10_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 183}-->
<h2 class="teaser-h"><span>ショッピング</span>訳あり商品</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser15_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{else}-->
<h2 class="teaser-h"><span>ショッピング</span><!--{$tpl_title|h}--></h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser_other_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{/if}-->
</p>
                </div>
            </div><!-- /#teaser -->


            <p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span><a href="/products/">ショッピング</a><span>&gt;</span><!--{if $tpl_subtitle|strlen >= 1}--><!--{$tpl_subtitle|h}--><!--{elseif $tpl_title|strlen >= 1}--><!--{$tpl_title|h}--><!--{/if}--></p>
            
            <div id="contents">
                <div class="section">
                    <div class="inner">
                        <div class="shoplist-wrap">
                            <div class="section">
                                <p class="shoplist-txt"><!--{$category_info}--></p>
<!-- ▼ブランド別タブリンクエリア ここから -->

<!--{if $arrSearchData.category_id == 161}--><!--アンペリアル-->
<!--{elseif $arrSearchData.category_id == 143 || $arrSearchData.category_id == 144 || $arrSearchData.category_id == 145}--><!--ドクターリセラ-->
<div class="product_change clearfix"><ul>
<a href="/products/list.php?mode=series&series_id=143"><li>全てを見る</li></a>
<a href="/products/list.php?mode=series&series_id=144"><li>ADS</li></a>
<a href="/products/list.php?mode=series&series_id=145"><li>スタンダード</li></a>
</ul>
</div>
<!--{elseif $arrSearchData.category_id == 4 || $arrSearchData.category_id == 38  || $arrSearchData.category_id == 39  || $arrSearchData.category_id == 96  || $arrSearchData.category_id == 139  || $arrSearchData.category_id == 41  || $arrSearchData.category_id == 132  || $arrSearchData.category_id == 138  || $arrSearchData.category_id == 164  || $arrSearchData.category_id == 150  || $arrSearchData.category_id == 163  || $arrSearchData.category_id == 149  || $arrSearchData.category_id == 40  || $arrSearchData.category_id == 164   }--><!--アクアヴィーナス-->
<div class="product_change clearfix"><ul>
<a href="/products/list.php?mode=series&series_id=4"><li>全てを見る</li></a>
<a href="/products/list.php?mode=series&series_id=38"><li>スキンケア</li></a>
<a href="/products/list.php?mode=series&series_id=39"><li>ターゲットケア</li></a>
<a href="/products/list.php?mode=series&series_id=96"><li>UVケア</li></a>
<a href="/products/list.php?mode=series&series_id=139"><li>ヘアケア</li></a>
<a href="/products/list.php?mode=series&series_id=41"><li>ボディーケア</li></a>
<a href="/products/list.php?mode=series&series_id=132"><li>ボディーシェイパー</li></a>
<a href="/products/list.php?mode=series&series_id=138"><li>インナーケア</li></a>
<!--<a href="/products/list.php?mode=series&series_id=40"><li>透輝メイク<br class="visible-ts"> ファンデーション類</li></a>-->
<!--<a href="/products/list.php?mode=series&series_id=150"><li>透輝メイク<br class="visible-ts"> アイメイク類</li></a>-->
<!--<a href="/products/list.php?mode=series&series_id=163"><li>透輝メイク<br class="visible-ts"> チーク</li></a>-->
<!--<a href="/products/list.php?mode=series&series_id=149"><li>透輝メイク<br class="visible-ts"> リップ類</li></a>-->
</ul>
</div>
<!--{elseif $arrSearchData.category_id == 79|| $arrSearchData.category_id == 34 || $arrSearchData.category_id == 83  || $arrSearchData.category_id == 109  || $arrSearchData.category_id == 109}--><!--ナチュリスティー-->
<div class="product_change clearfix"><ul>
<a href="/products/list.php?mode=series&series_id=34"><li>全てを見る</li></a>
<a href="/products/list.php?mode=series&series_id=83"><li>アクレス</li></a>
<a href="/products/list.php?mode=series&series_id=109"><li>ナチュリスティー</li></a>
</ul>
</div>
<!--{elseif $arrSearchData.category_id == 154}--><!--リセラベビー-->
<!--{elseif $arrSearchData.category_id == 119}--><!--ミラクストックス-->
<!--{elseif $arrSearchData.category_id == 157 || $arrSearchData.category_id == 158  || $arrSearchData.category_id == 160  || $arrSearchData.category_id == 159 || $arrSearchData.category_id == 173}--><!--エステプロラボ-->
<div class="product_change clearfix"><ul>
<a href="/products/list.php?mode=series&series_id=157"><li>全てを見る</li></a>
<a href="/products/list.php?mode=series&series_id=158"><li>ハーブティー</li></a>
<a href="/products/list.php?mode=series&series_id=159"><li>サプリメント</li></a>
<a href="/products/list.php?mode=series&series_id=160"><li>ドリンク</li></a>
<a href="/products/list.php?mode=series&series_id=173"><li>食品</li></a>
</ul>
</div>
<!--{elseif $arrSearchData.category_id == 10  || $arrSearchData.category_id == 48}-->
<div class="product_change clearfix"><ul>
<a href="/products/list.php?mode=series&series_id=10"><li>全てを見る</li></a>
<a href="/products/list.php?mode=series&series_id=48"><li>エッセンシャルオイル</li></a>
</ul>
</div>
<!--{elseif $arrSearchData.category_id == 172  || $arrSearchData.category_id == 180  || $arrSearchData.category_id == 179  || $arrSearchData.category_id == 178  || $arrSearchData.category_id == 177  || $arrSearchData.category_id == 176 || $arrSearchData.category_id == 175 || $arrSearchData.category_id == 174}-->
<div class="product_change clearfix"><ul>
<a href="/products/list.php?mode=series&series_id=172"><li>全てを見る</li></a>
<a href="/products/list.php?mode=series&series_id=180"><li>ジュンパ</li></a>
<a href="/products/list.php?mode=series&series_id=179"><li>クレシェ</li></a>
<a href="/products/list.php?mode=series&series_id=178"><li>エレモア</li></a>
<a href="/products/list.php?mode=series&series_id=177"><li>カルミア</li></a>
<a href="/products/list.php?mode=series&series_id=176"><li>エンチーム</li></a>
<a href="/products/list.php?mode=series&series_id=175"><li>セルケア・GFシリーズ</li></a>
<a href="/products/list.php?mode=series&series_id=174"><li>アッシュエル</li></a>
</ul>
</div>
<!--{elseif $arrSearchData.category_id == 181  || $arrSearchData.category_id == 194  || $arrSearchData.category_id == 195   }--><!--ソーアディクテッド-->
<div class="product_change clearfix"><ul>
<a href="/products/list.php?mode=series&series_id=181"><li>全てを見る</li></a>
<a href="/products/list.php?mode=series&series_id=194"><li>リップ</li></a>
<a href="/products/list.php?mode=series&series_id=195"><li>アイラッシュ</li></a>
</ul>
</div>
<!--{elseif $arrSearchData.category_id == 188 ||  $arrSearchData.category_id == 190  || $arrSearchData.category_id == 191  || $arrSearchData.category_id == 192  || $arrSearchData.category_id == 193   }--><!--ヴィプランツ-->
<div class="product_change clearfix"><ul>
<a href="/products/list.php?mode=series&series_id=188"><li>全てを見る</li></a>
<a href="/products/list.php?mode=series&series_id=190"><li>ベース</li></a>
<a href="/products/list.php?mode=series&series_id=191"><li>アイメイク</li></a>
<a href="/products/list.php?mode=series&series_id=192"><li>チーク</li></a>
<a href="/products/list.php?mode=series&series_id=193"><li>リップ</li></a>
</ul>
</div>
<!--{else}-->
<!--{/if}-->

<!-- ▲ブランド別タブリンクエリア ここまで -->
<div class="products-list clearfix">
<div class="name-series">▼<!--{if $tpl_subtitle|strlen >= 1}--><!--{$tpl_subtitle|h}--><!--{elseif $tpl_title|strlen >= 1}--><!--{$tpl_title|h}--><!--{/if}--></div>
                                <ul class="shoplist-box clearfix">
                                <!--{foreach from=$arrProducts key=i item=arrProduct}-->
                                <!-- ▼商品 ここから -->
                                    <li>
                                        <div class="shoplist-bor hlg01 biggerlink clearfix">
                                            <p class="shoplist-img"><img height="150" src="<!--{$smarty.const.IMAGE_SAVE_URLPATH|sfTrimURL}-->/<!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->" /></p>
                                            <p class="shoplist-det"><a href="<!--{$smarty.const.MOBILE_P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->"><!-- 商品名 --><!--{$arrProduct.name|h}--><br></a><span><!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
                                                <!--{if $arrSiteInfo.tax_flg eq 1}-->
                                                    <!--{$arrProduct.price02_min|number_format}-->円
                                                <!--{else}-->
                                                    <!--{$arrProduct.price02_min|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->円
                                                <!--{/if}-->
                                            <!--{else}-->
                                                <!--{if $arrSiteInfo.tax_flg eq 1}-->
                                                    <!--{$arrProduct.price02_min|number_format}-->～<!--{$arrProduct.price02_max|number_format}-->円
                                                <!--{else}-->
                                                    <!--{$arrProduct.price02_min|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->～<!--{$arrProduct.price02_max|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->円(税込)
                                                <!--{/if}-->
                                            <!--{/if}--></span>

                                            <!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
                                            (税込 <!--{$arrProduct.price02_min|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->円)
                                            <!--{else}-->
                                            (税込 <!--{$arrProduct.price02_min|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->～<!--{$arrProduct.price02_max|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->円(税込))
                                            <!--{/if}-->
                                            </p>
                                        </div>
                                    </li>
                              <!-- ▲商品 ここまで -->
                                <!--{foreachelse}-->
                                <div class="clearfix" style="clear: both;">該当商品がありません。</div>
                                <!--{/foreach}-->     
                                </ul>
                        <!--{if $tpl_strnavi != ""}-->
        </div>               
                            <div class="news-page clearfix">
                                 <!--{$tpl_strnavi}-->
                                  <p class="shoplist-btn"><a href="/products/" class="input-button">ショッピングへ戻る</a></p>
                            </div>
                         
                           
                      
                        <!--{/if}-->
                            </div>
                          
                        </div>
                    </div>
                </div>
            </div><!-- /#contents -->
        </article>

