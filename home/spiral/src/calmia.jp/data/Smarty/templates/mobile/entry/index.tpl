<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/login.css" media="all" />
<script src="https://zipaddr.com/js/zipaddrx.js" charset="UTF-8"></script>
<script>if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
    $('input').attr('autocomplete', 'off');
}</script>
<script>
    $(function(){
   $('.required').on('keydown keyup keypress change focus blur', function(){
       if($(this).val() == ''){
           $(this).css({backgroundColor:'#ffcccc'});
       } else {
           $(this).css({backgroundColor:'#fff'});
       }
   }).change();
});
</script>
<style>
    input:-webkit-autofill {
    -webkit-box-shadow: 0 0 0px 1000px #fff inset;
}
</style>
<article>
    <h2 class="login-hdl"><!--{$tpl_title|h}--></h2>
    <div id="contents">
        <div class="inner">
            <div id="form-area">
                <form name="form1" method="post" action="<!--{$smarty.const.ROOT_URLPATH}-->entry/index.php" autocomplete="off">
                    <input type="hidden" name="mode" value="confirm" />
                    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                    <p class="password-txt">以下のフォームより会員情報を記入の上、登録してください｡<br>※ 【<span class="fcred fs13">※</span>】は入力必須項目です｡それ以外の項目につきましては差し支えない範囲でご入力ください｡</p>
                    <div class="register-table">

                        <table class="com-table01">
                            <tr>
                                <th>お客様ID<span class="fcred fs13">※</span></th>
                                <td>
                                    <input type="text" id="user_id" name="user_id" value="<!--{$arrForm.user_id|h}-->" class="input-text w-max required">
                                    <p class="fcgray fs13 mt05">ご希望のIDを英数字でご入力ください</p>
                                    <span class="fcred fs13"><!--{$arrErr.user_id}--></span>
                                </td>
                            </tr>
                        </table>

                        <table class="com-table01">
                            <tr>
                                <th>お名前（姓）<span class="fcred fs13">※</span></th>
                                <td>
                                    <input type="text" name="name01" value="<!--{$arrForm.name01|h}-->" maxlength="50" style="; ime-mode: active;" id="name01" placeholder="（例：広島）" class="input-text required w-max" />
                                    <span class="fcred fs13"><!--{$arrErr.name01}--></span>
                               </td>
                            </tr>
                            <tr>
                                <th>お名前（名）<span class="fcred fs13">※</span></th>
                                <td>
                                    <input type="text" name="name02" value="<!--{$arrForm.name02|h}-->" maxlength="50" style="; ime-mode: active;" id="name02" placeholder="（例：花子）" class="input-text required w-max" />
                                    <span class="fcred fs13"><!--{$arrErr.name02}--></span>
                                </td>
                            </tr>
                            <tr>
                                <th>フリガナ（姓）<span class="fcred fs13">※</span></th>
                                <td>
                                    <input type="text" name="kana01" value="<!--{$arrForm.kana01|h}-->" maxlength="50" style="; ime-mode: active;" id="kana01" placeholder="（例：ヒロシマ）" class="input-text required w-max" />
                                    <span class="fcred fs13"><!--{$arrErr.kana01}--></span>
                                </td>
                            </tr>
                            <tr>
                                <th>フリガナ（名）<span class="fcred fs13">※</span></th>
                                <td>
                                    <input type="text" name="kana02" value="<!--{$arrForm.kana02|h}-->" maxlength="50" style="; ime-mode: active;" id="kana02" placeholder="（例：ハナコ）" class="input-text required w-max" />
                                    <span class="fcred fs13"><!--{$arrErr.kana02}--></span>
                                  </td>
                            </tr>
                            <tr>
                                <th>旧姓</th>
                                <td>
                                    <input type="text" name="oldname01" value="<!--{$arrForm.oldname01|h}-->" maxlength="50" style="; ime-mode: active;" id="oldname01" placeholder="（例：山口）" class="input-text w-max" />
                                    <span class="fcred fs13"><!--{$arrErr.oldname01}--></span>
                                  </td>
                            </tr>
                            <tr>
                                <th>旧姓(フリガナ)</th>
                                <td>
                                    <input type="text" name="oldkana01" value="<!--{$arrForm.oldkana01|h}-->" maxlength="50" style="; ime-mode: active;" id="oldkana01" placeholder="（例：ヤマグチ）" class="input-text w-max" />
                                    <span class="fcred fs13"><!--{$arrErr.oldkana01}--></span>
                                  </td>
                            </tr>
                            <tr>
                                <th>生年月日<span class="fcred fs13">※</span></th>
                                <td>
                                    <ul class="form-date">
                                        <li><select name="year" style="height: 40px;" class="required">
                                                        <!--{html_options options=$arrYear selected=$arrForm.year}-->
                                                    </select>年</li>
                                        <li><select name="month" style="height: 40px;" class="required">
                                                        <!--{html_options options=$arrMonth selected=$arrForm.month}-->
                                                    </select>月</li>
                                        <li><select name="day" style="height: 40px;" class="required">
                                                        <!--{html_options options=$arrDay selected=$arrForm.day}-->
                                                    </select>日</li>
                                    </ul>
                                    <span class="fcred fs13"><!--{$arrErr.year}--><!--{$arrErr.month}--><!--{$arrErr.day}--></span>
                                </td>
                            </tr>
                            <tr>
                                <th class="vat">ご住所<span class="fcred fs13">※</span></th>
                                <td>
                                    <p class="form-address">〒&nbsp;
                                        <input type="text" name="zip01" value="<!--{if $arrForm.zip01 == ""}--><!--{$arrOtherDeliv.zip01|h}--><!--{else}--><!--{$arrForm.zip01|h}--><!--{/if}-->" maxlength="3" style="height: 40px; ime-mode: disabled;" class="input-text required" id="zip" placeholder="123" />&nbsp;-&nbsp;
                                        <input type="text" name="zip02" value="<!--{if $arrForm.zip02 == ""}--><!--{$arrOtherDeliv.zip02|h}--><!--{else}--><!--{$arrForm.zip02|h}--><!--{/if}-->" maxlength="4" style="height: 40px; ime-mode: disabled;" class="input-text required" id="zip1" placeholder="4567"/>&nbsp;
                                        <span class="fcblue"><a href="http://www.post.japanpost.jp/zipcode/" target="_blank">郵便番号検索</a></span>
                                    </p><span class="mini">郵便番号を入力すると自動で住所が表示されます。</span>
                                    <!--<p class="zipimg">
                                        <input type="submit" name="submit" value="住所検索">
                                        &nbsp;<span class="mini">郵便番号を入力後、クリックしてください。</span>
                                    </p>-->
                                    <select name="pref" id="pref" style="" class="mt15 w150">
                                        <option value="">都道府県を選択</option>
                                        <!--{html_options options=$arrPref selected=$arrForm.pref}-->
                                    </select>

                                    <p class="mt15">市町村区番地（例：○○市○○区○○町1-2）<br><input type="text" id="city" name="addr01" value="<!--{$arrForm.addr01}-->" class="input-text required w-max" style="; ime-mode: active;" /></p>
                                    <p class="mt15">ビル・マンション名（例：〇〇マンション 102号室）<br><input type="text" name="addr02" value="<!--{$arrForm.addr02}-->" class="input-text w-max" style="; ime-mode: active;" /></p>
                                    <p class="mt15 fcred fs13"><span class="attention">住所は2つに分けてご記入ください。マンション名は必ず記入してください。</span></p>

                                    <span class="fcred fs13"><!--{$arrErr.zip01}--><!--{$arrErr.zip02}--><!--{$arrErr.pref}--><!--{$arrErr.addr01}--><!--{$arrErr.addr02}--></span>

                                </td>
                            </tr>
                            <tr>
                                <th>携帯番号<span class="fcred fs13">※</span></th>
                                <td>
                                    <p class="form-tel">
                                        <input type="tel" name="cell01" value="<!--{$arrForm.cell01|h}-->" maxlength="6" id="phone01" class="input-text required">&nbsp;-&nbsp;
                                        <input type="tel" name="cell02" value="<!--{$arrForm.cell02|h}-->" maxlength="6" id="phone02" class="input-text required">&nbsp;-&nbsp;
                                        <input type="tel" name="cell03" value="<!--{$arrForm.cell03|h}-->" maxlength="6" id="phone03" class="input-text required">
                                    </p>
                                    <span class="fcred fs13"><!--{$arrErr.cell01}--><!--{$arrErr.cell02}--><!--{$arrErr.cell03}--></span>
                                </td>
                            </tr>
                            <tr>
                                <th>電話番号</th>
                                <td>
                                    <p class="form-tel">
                                        <input type="tel" name="tel01" value="<!--{$arrForm.tel01|h}-->" maxlength="6" style="; ime-mode: disabled;" id="tel01" class="input-text" />&nbsp;-&nbsp;
                                        <input type="tel" name="tel02" value="<!--{$arrForm.tel02|h}-->" maxlength="6" style="; ime-mode: disabled;" id="tel02" class="input-text" />&nbsp;-&nbsp;
                                        <input type="tel" name="tel03" value="<!--{$arrForm.tel03|h}-->" maxlength="6" style="; ime-mode: disabled;" id="tel03" class="input-text" />
                                    </p>
                                    <span class="fcred fs13"><!--{$arrErr.tel01}--><!--{$arrErr.tel02}--><!--{$arrErr.tel03}--></span>
                                </td>
                            </tr>
                            <tr>
                                <th class="vat">メールアドレス<span class="fcred fs13">※</span></th>
                                <td>
                                    <p><input type="text" name="email" value="<!--{$arrForm.email|h}-->" style="; ime-mode: disabled;" maxlength="200" id="mail01" class="input-text required w-max" /></p>
<!--{*
                                    <p class="mt10"> <input type="text" name="email02" value="" style="; ime-mode: disabled;" maxlength="200" id="mail02" class="input-text w-max" /></p>
                                    <p class="mt10 fcred fs13 fs13">確認のため2度入力してください。</p>
*}-->
                                    <span class="fcred fs13"><!--{$arrErr.email}--></span>
                                </td>
                            </tr>
                        </table>

                        <table class="com-table01">
                            <tr>
                                <th class="vat">パスワード<span class="fcred fs13">※</span></th>
                                <td>
                                    <input type="password" name="password" value="<!--{$arrForm.password}-->" maxlength="50" style="" class="input-text required w-max" />
                                    <p class="fcgray mt05 fs13">ご希望のパスワードを英数字でご入力ください（半角英数字<!--{$smarty.const.PASSWORD_MIN_LEN}-->文字以上<!--{$smarty.const.PASSWORD_MAX_LEN}-->文字以内）</p>
<!--{*
                                    <input type="password" name="password02" value="" maxlength="50" style="" class="box120" />
                                    <p><span class="attention mini">確認のために2度入力してください。</span></p>

                                    <input type="password" id="password" name="password" value="" class="input-text w-max">
                                    <p class="fcgray mt05 fs13">ご希望のパスワードを英数字でご入力ください（半角英数字4文字以上50文字以内）</p>
*}-->
                                    <span class="fcred fs13"><!--{$arrErr.password}--></span>
                                </td>
                            </tr>
                            <tr>
                                <th>パスワード<br>確認用の質問<span class="fcred fs13">※</span></th>
                                <td>
                                    <select name="reminder" style="" class="w200 required">
                                        <option value="">選択してください</option>
                                        <!--{html_options options=$arrReminder selected=$arrForm.reminder}-->
                                    </select>
                                    <br><span class="fcred fs13"><!--{$arrErr.reminder}--></span>
                                </td>
                            </tr>
                            <tr>
                                <th>質問の答え<span class="fcred fs13">※</span></th>
                                <td>
                                    <input type="text" id="question" name="reminder_answer" value="<!--{$arrForm.reminder_answer|h}-->" class="input-text required w-max"><br />
                                    <span class="fcred fs13"><!--{$arrErr.reminder_answer}--></span>
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" name="mailmaga_flg" value="2">
                        <input type="hidden" name="directmail_flg" value="2">
                        <p class="mt15">会員登録をされると、メールマガジンが発行されます。メールマガジン不要の場合は、<br>会員登録後変更画面から“希望しない”を選択してください。</p>
                    </div>

                    <div class="combtn-top combtn-twocolumn">
                        <ul class="clearfix">
                            <li><a href="<!--{$smarty.const.MOBILE_TOP_URLPATH}-->" class="input-button">ログイン画面に戻る</a></li>
                                <li><input type="submit" value="入力内容を確認する" name="submit" id="confirm" class="input-submit"></li>
                        </ul>
                    </div>
                    <!--{foreach from=$list_data key=key item=item}-->
                        <input type="hidden" name="<!--{$key|h}-->" value="<!--{$item|h}-->">
                    <!--{/foreach}-->
                </form>
            </div>
        </div>
    </div><!-- /#contents -->
</article>

