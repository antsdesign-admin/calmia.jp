<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<!--
当サイトのコンテンツは、下記免責事項をお読みになり、ご了承いただいた方にお使い頂けます。<br>
<br>
パケット通信には携帯キャリア指定の通信料が発生します。<br>
電波状況などにより、パケット通信をやり直す場合があります。
-->

<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/sitemap.css" media="all" />
			<div id="teaser-tit">
				<h2 class="teaser-titin">サイトマップ</h2>
			</div><!-- /#teaser-tit -->
			
			<p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>サイトマップ</p>
			
			<div id="contents">
				
				
				<div class="section">
					<div class="inner">
						<div class="sitemap-box">
							<ul class="clearfix">
								<li><a href="/">● HOME</a></li>
								<li><a href="/">● お客様情報の変更</a></li>
								<li><a href="/">● 買い物カゴの中</a></li>
								<li><a href="/">● 新着情報</a></li>
								<li><a href="/">● ショッピング</a></li>
								<li><a href="/">● 特定商取引に関する表記</a></li>
								<li><a href="/">● お気に入り</a></li>
								<li><a href="/about/">● 会社概要</a></li>
								<li><a href="/">● ご注文履歴</a></li>
								<li><a href="/guide/privacy.php">● 個人情報保護方針</a></li>
								<li><a href="/user_data/guide.php">● ご注文ガイド</a></li>
								<li><a href="/user_data/sitemap.php">● サイトマップ</a></li>
							</ul>
						</div>
					</div>
				</div>
				
			</div><!-- /#contents -->
			
		</article>