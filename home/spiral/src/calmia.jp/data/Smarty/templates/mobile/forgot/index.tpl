<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/login.css" media="all" />
<article>
   
    <form action="<!--{$smarty.const.ROOT_URLPATH}-->forgot/index.php" method="post">
        <input type="hidden" name="mode" value="mail_check">
        <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
        <h2 class="login-hdl">パスワード再発行</h2>
        <div id="contents">
            <div class="inner">
                <p class="password-txt">ご登録時のメールアドレスとお名前を入力して、「次へ」ボタンをクリックしてください。<br>新しくパスワードを発行致しますので、お忘れになったパスワードはご利用できなくなります。</p>
                <div class="password-box">
                    <p>パスワード再発行前に必ず下記ドメインをメール受信指定に追加してください｡</p>
                    <p class="password-domain"><a href="<!--{$smarty.const.ROOT_URLPATH}-->domain/index.php">ドメイン指定について</a></p>
                </div>
                <span class="red" style="color: red;"></span>
  <!--{if $errmsg}-->
        <span class="fcred"><!--{$errmsg}--></span><br>
    <!--{/if}-->
                <table class="com-table01 password-table">
               

                    <tr>
                        <th>メールアドレス</th>
                        <td>
                            <input type="text" name="email" value="<!--{$arrForm.email|default:$tpl_login_email|h}-->" class="input-text w90per" style="; ime-mode: disabled;" />
                            <br><span class="fcred"><!--{$arrErr.email}--></span>
                        </td>
                    </tr>
                    <tr>
                        <th>お名前（姓） </th>
                        <td>
                            <input type="text" name="name01" value="<!--{$arrForm.name01|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" class="input-text w90per" style="; ime-mode: active;" >
                            <br><span class="fcred"><!--{$arrErr.name01}--></span>
                        </td>
                    </tr>
                    <tr>
                        <th>お名前（名）</th>
                        <td>
                            <input type="text" name="name02" value="<!--{$arrForm.name02|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" class="input-text w90per" style="; ime-mode: active;" >
                            <br><span class="fcred"><!--{$arrErr.name02}--></span>
                        </td>
                    </tr>
                </table>

                <div class="combtn-top combtn-twocolumn">
                    <ul class="clearfix">
                       <li><a href="/mypage/login.php" class="input-button">ログイン画面に戻る</a></li>
                        <li><input type="submit" value="次へ" name="next" id="next" class="input-submit pass" />
                        </li>
                     
                    </ul>
                </div>
            </div>
        </div><!-- /#contents -->
    </form>
</article>
