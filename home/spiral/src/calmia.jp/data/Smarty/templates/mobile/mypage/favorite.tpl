<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->

<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/popular.css" media="all" />



      <div id="teaser">
        <div class="teaserin">
          <h2 class="teaser-h">お気に入り</h2>
          <p><img src="<!--{$TPL_URLPATH}-->img/popular/popular_teaser_pc.png" alt="お気に入り"></p>
        </div>
      </div><!-- /#teaser -->
      

      <p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>お気に入り</p>
      
      <div id="contents">

            <!--{if $tpl_linemax > 0}-->
    <!--{if $tpl_strnavi != ""}-->
    <!--{$tpl_strnavi}-->
    <br>
    <!--{/if}-->

    <!--{if strlen($arrErr.products_id) > 0}-->
    <!--{$arrErr.products_id|h}-->
    <!--{/if}-->

    <form method="post" action="<!--{$smarty.server.PHP_SELF}-->">
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />  
          <div class="inner">
            <div class="popular-top-wrap">
              <p class="popular-top-note">※最大で50件までお気に入り登録できます。</p>
              <div class="popular-top-box">
                <ul class="popular-top-list clearfix">



   <!--{foreach from=$arrFavorite item=product}-->
                  <li>
                    <div class="popular-top-in">
                      <p class="popular-top-img"><img height="100" src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$product.main_image|h}-->" alt="<!--{$product.name|h}-->" /></p>
                      <p class="popular-top-tit hlg01"><a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$product.product_id|u}-->"><!--{$product.name|h}--></a><span><!--{if $arrSiteInfo.tax_flg eq 1}-->
            <!--{$product.price02_min|number_format}-->円(税抜)
        <!--{else}-->
            <!--{$product.price02_min|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->円(税込)
        <!--{/if}--></span></p>
                      <div class="popular-list-btn">
                        <p class="popular-top-btn02"><input name="products_id[]" value="<!--{$product.product_id|u}-->" type="checkbox"></p>
                      </div>
                    </div>
                  </li>

 <!--{/foreach}-->
                </ul>



                <div class="visible-ts">
                
                    <table class="popular-top-table">

 <!--{foreach from=$arrFavorite item=product}-->     
                      <tr>
                        <th>
                         <input name="products_id[]" value="<!--{$product.product_id|u}-->" type="checkbox">
                        </th>
                        <td>
                          <dl class="popular-top-commodity">
                            <dt class="img-container--flex-box"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$product.main_image|h}-->" alt="<!--{$product.name|h}-->" /></dt>
                            <dd><a href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$product.product_id|u}-->"><!--{$product.name|h}--></a><!--{if $arrSiteInfo.tax_flg eq 1}-->
            <!--{$product.price02_min|number_format}-->円(税抜)
        <!--{else}-->
            <!--{$product.price02_min|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->円(税込)
        <!--{/if}--></dd>
                          </dl>
                        </td>
                      </tr>
 <!--{/foreach}-->   
                    </table>
               
                </div>

     


          
    </div>
            </div>

          </div>
          <div class="topBtn" id="topBtn">
            <div class="popular-top-area">
              <div class="popular-box-in01 biggerlink">
                <p><img src="<!--{$TPL_URLPATH}-->img/common/cart.svg" width="25" height="25" alt="cart"><input type="submit" name="cart" value="カゴに入れる"></p>
              </div>
              <div class="popular-box-in02 biggerlink">
                <p><input type="submit" name="delete" value="お気に入りから削除する"></p>
              </div>
            </div>
          </div>
</form>
         <!--{else}-->
お気に入りが登録されておりません。
<!--{/if}-->

      </div><!-- /#contents -->
      
    </article>

    <script>
$(document).ready(function(){
    $("#topBtn").hide();
    $(window).on("scroll", function() {
        if ($(this).scrollTop() > 100) {
            $("#topBtn").fadeIn("fast");
        } else {
            $("#topBtn").fadeOut("fast");
        }
        scrollHeight = $(document).height(); //ドキュメントの高さ 
        scrollPosition = $(window).height() + $(window).scrollTop(); //現在地 
        footHeight = $("footer").innerHeight(); //footerの高さ（＝止めたい位置）
        if ( scrollHeight - scrollPosition  <= footHeight ) { //ドキュメントの高さと現在地の差がfooterの高さ以下になったら
            $("#topBtn").css({
                "position":"absolute", //pisitionをabsolute（親：wrapperからの絶対値）に変更
                "bottom": footHeight + 20 //下からfooterの高さ + 20px上げた位置に配置
            });
        } else { //それ以外の場合は
            $("#topBtn").css({
                "position":"fixed", //固定表示
                "bottom": "0px" //下から20px上げた位置に
            });
        }
    });
  
});
</script>