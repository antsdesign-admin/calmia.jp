<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->

<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/personal.css" media="all" />
    <div id="teaser-tit">
                <h2 class="teaser-titin">個人情報保護方針</h2>
            </div><!-- /#teaser-tit -->
            
            <p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>個人情報保護方針</p>
            
            <div id="contents">
                
                <div class="inner clearfix">
                    <div id="main" class="personal-wrap">
                        
                        <p class="personal-txt">お客様に関する情報のお取り扱いについてスパイラル株式会社（以下「当社」といいます｡）では､お客様からの信頼を第一と考え､以下の方針に沿ってお客様からお預かりした情報を取り扱い､正確性および機密性の保持に努めてまいります｡</p>
                        
                        <div id="anch01" class="section">
                            <h3 class="hdl">情報を収集する目的</h3>
                            <p>当社ではお客様の個人情報は、ご予約登録・ポイント付与・オンラインショッピング・当社ウェブサイトにおけるサービス提供・各種お問い合わせなどに必要な場合のみ、ご提供をお願いしております。</p>
                        </div>
                        
                        <div id="anch02" class="section">
                            <h3 class="hdl">収集する情報の種類</h3>
                            <p>基本的な項目としましては、お客様の住所･氏名･電話番号･メールアドレス等を､個人情報とします。その他は、お客様の必要に即したサービスに関する情報です｡</p>
                        </div>
                        
                        <div id="anch03" class="section">
                            <h3 class="hdl">個人情報の利用と提供</h3>
                            <p>当社が保有する個人情報につきまして、以下のいずれかに該当する場合を除き、いかなる第三者にも提供または開示致しません｡<br>●利用者の方からの同意があった場合<br>●公的機関（裁判所や警察など）から、法律に基づく正式な照会要請を受けた場合<br>●利用者の方にサービスを提供する目的で、当社からの依頼を受けて業務を行う会社が情報を必要とする場合｡<br>　（ただしこれらの会社も、個人情報を上記の目的の限度を超えて、利用する事は出来ません｡）</p>
                        </div>
                        
                        <div id="anch04" class="section">
                            <h3 class="hdl">情報の管理方法</h3>
                            <p>当社はお客様からお預かりした個人情報を、不正アクセス・紛失・破壊・改ざん・漏洩から保護するために、SSL（Secure Sokets Layer）など暗号化通信や、その他適切な方法を用いて安全対策を講じます｡</p>
                        </div>
                        
                        <div id="anch05" class="section">
                            <h3 class="hdl">個人情報に関するお客様からのお問い合わせなどについて</h3>
                            <p>当社はお客様から、当社が管理するお客様自身の個人情報について、下記の要請を受けた場合には、お客様の意思を尊重し、合理的な範囲で必要な対応をします｡<br>●開示請求<br>●修正・更新・および削除<br>●当社に対して行なった個人情報の利用に関する同意の一部または全部の撤回</p>
                        </div>
                        
                        <div id="anch06" class="section">
                            <h3 class="hdl">お問い合わせ・ご質問など</h3>
                            <p>当サイトについてのご質問やご意見がございましたら、下記の方法よりお問い合わせください。</p>
                            <p class="personal-box">カルミアお客様サポートデスク<br>E-mail：info@calmia.jp<br>TEL：0120-86-3730<br>受付時間　月～金／9:30～18:00<br>（土日祝・年末年始・GW・お盆・当社規定休日を除く）
</p>
                        </div>
                        
                    </div>
                    
                    <div id="side">
                        <ul class="side-list scroll">
                            <li><a href="#anch01">情報を収集する目的</a></li>
                            <li><a href="#anch02">収集する情報の種類</a></li>
                            <li><a href="#anch03">個人情報の利用と提供</a></li>
                            <li><a href="#anch04">情報の管理方法</a></li>
                            <li><a href="#anch05">個人情報に関するお客様からのお問い合わせなどについて</a></li>
                            <li><a href="#anch06">お問い合わせ・ご質問など</a></li>
                        </ul>
                    </div>
                </div>
                
                
                
                
            </div><!-- /#contents -->
            
        </article>