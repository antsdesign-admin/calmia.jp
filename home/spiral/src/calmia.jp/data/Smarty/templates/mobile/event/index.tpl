<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}--><link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/news.css" media="all" />

			
			<div id="teaser-tit">
				<h2 class="teaser-titin">新着情報</h2>
			</div><!-- /#teaser-tit -->
			
			<p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>新着情報</p>
			
			<div id="contents">
				
				<div class="news-wrap">
				<!--{if count($arrNews) > 0}-->
					<div class="inner">
						<div class="section">


				<!--{foreach from=$arrNews item=news name=news}-->
					
							<a href="<!--{$smarty.const.ROOT_URLPATH}-->event/detail.php?news_id=<!--{$news.news_id|h}-->"><dl class="clearfix biggerlink">
								<dt><!--{$news.news_date|date_format:"%y/%m/%d"}--></dt>
								<dd><!--{$news.news_title|h}--></dd>
							</dl></a>
							<!--{if !$smarty.foreach.news.last}--><!--{/if}-->
				<!--{/foreach}-->		
						</div>
						
				
						<!--{if $tpl_dispnavi}-->
						<div class="section">
							<div class="news-page clearfix">
								<p class="news-page-pre"><!--{if strlen($tpl_beforePage) > 0}-->
						<a href="<!--{$smarty.server.PHP_SELF}-->?pageno=<!--{$tpl_beforePage}--><!--{$tpl_searchStr|h}-->">&lt; 前の10件</a>
					<!--{/if}--></p>
								<p class="news-page-next"><!--{if strlen($tpl_nextPage) > 0}-->
						<a href="<!--{$smarty.server.PHP_SELF}-->?pageno=<!--{$tpl_nextPage}--><!--{$tpl_searchStr|h}-->">次の10件 &gt;</a>
					<!--{/if}--></p>
					<p class="news-page-list"><a href="/event/">一覧に戻る</a></p>
							</div>
							<p class="news-page-listsp"><a href="/event/">一覧に戻る</a></p>
						</div>
						<!--{/if}-->
					</div>
				</div>
				<!--{else}-->
		新着情報はありません。
		<!--{/if}-->

				
				
			</div><!-- /#contents -->
			
		</article>