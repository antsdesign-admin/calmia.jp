<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<font size=1>
<!--{if count($arrNews) > 0}-->
<table width=100% bgcolor="<!--{$arrColor2[$tpl_themeid]}-->">
<tr>
<td align=center>
<font size=1>
<a href="<!--{$smarty.const.ROOT_URLPATH}-->event/category.php">ｶﾃｺﾞﾘ別一覧</a>&nbsp;|&nbsp;<a href="<!--{$smarty.const.ROOT_URLPATH}-->event/month.php">年月別一覧</a>
</font>
</td>
</tr>
</table>
<br />

<!--{foreach from=$arrNews item=news name=news}-->
<center>
<a href="<!--{$smarty.const.ROOT_URLPATH}-->event/index.php?mode=month&amp;year=<!--{$news.news_year|h}-->&amp;month=<!--{$news.news_month|h}-->&amp;<!--{$smarty.const.SID}-->"><!--{$news.news_year|h}-->年<!--{$news.news_month|h}-->月</a>
(<!--{$news.news_count|h}-->件)
</center>
<!--{if !$smarty.foreach.news.last}--><br /><!--{/if}-->
<!--{/foreach}-->

<!--{else}-->
新着情報はありません。
<!--{/if}-->
</font>
