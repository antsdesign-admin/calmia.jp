<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}--><link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/customer.css" media="all" />
			<div id="teaser-tit">
				<h2 class="teaser-titin">お客様情報の変更</h2>
			</div><!-- /#teaser-tit -->
			
			<p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span><a href="/mypage/change.php">お客様情報の変更</a><span>&gt;</span>お客様情報の変更 確認</p>
			
			<div id="contents">
				
				<div class="inner">
					<div class="confirm-wrap">
					<form name="form1" id="form1" method="post" action="<!--{$smarty.const.ROOT_URLPATH}-->mypage/change.php">
  <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
	<input type="hidden" name="mode" value="complete">
	<!--{foreach from=$arrForm key=key item=item}-->
		<input type="hidden" name="<!--{$key|h}-->" value="<!--{$item|h}-->">
	<!--{/foreach}-->
							<p class="confirm-txt">下記の内容でご登録してもよろしいですか？</p>
							<table class="com-table01">
								<tr>
									<th>ユーザー利用ID</th>
									<td><!--{$arrForm.user_id|h}--></td>
								</tr>
								<tr>
									<th>お名前（姓）</th>
									<td><!--{$arrForm.name01|h}--><br>
								</td>
								</tr>
								<tr>
									<th>お名前（名）</th>
									<td><!--{$arrForm.name02|h}--></td>
								</tr>
								<tr>
									<th>フリガナ（姓）</th>
									<td><!--{$arrForm.kana01|h}--></td>
								</tr>
								<tr>
									<th>フリガナ（名）</th>
									<td><!--{$arrForm.kana02|h}--></td>
								</tr>
								<!--{if $arrForm.oldname01 eq ""}--><!--{else}-->
								<tr>
									<th>旧姓</th>
									<td><!--{$arrForm.oldname01|h}--></td>
								</tr><!--{/if}-->
								<!--{if $arrForm.oldkana01 eq ""}--><!--{else}--><tr>
									<th>旧姓フリガナ</th>
									<td><!--{$arrForm.oldkana01|h}--></td>
								</tr><!--{/if}-->

								<tr>
									<th>生年月日</th>
									<td><!--{if strlen($arrForm.year) > 0 && strlen($arrForm.month) > 0 && strlen($arrForm.day) > 0}--><!--{$arrForm.year|h}-->年<!--{$arrForm.month|h}-->月<!--{$arrForm.day|h}-->日生まれ<!--{else}-->生年月日 未登録<!--{/if}--></td>
								</tr>
								<tr>
									<th><span class="address">ご住所</span></th>
									<td><!--{$arrForm.zip01|h}--> - <!--{$arrForm.zip02|h}--><br>
	<!--{$arrPref[$arrForm.pref]|h}--><!--{$arrForm.addr01|h}--><!--{$arrForm.addr02|h}--><br>
	</td>
								</tr>
								<tr>
									<th>携帯番号</th>
									<td><!--{$arrForm.cell01|h}-->-<!--{$arrForm.cell02|h}-->-<!--{$arrForm.cell03|h}--><br>
	</td>
								</tr><!--{if $arrForm.tel01 eq "" && $arrForm.tel02 eq "" && $arrForm.tel03 eq ""}-->
		<!-- 電話番号 非表示 -->
	<!--{else}-->
								<tr>
									<th>電話番号</th>
									<td>
		<!--{$arrForm.tel01|h}-->-<!--{$arrForm.tel02|h}-->-<!--{$arrForm.tel03|h}--><br>
</td>	<!--{/if}-->
								</tr>
								<tr>
									<th>メールアドレス</th>
									<td><!--{$arrForm.email|h}--></td>
								</tr>
								<tr>
									<th>パスワード<br>確認用の質問</th>
									<td><!--{if $arrForm.reminder eq 0}-->
		設定なし<br>
	<!--{else}-->
		<!--{$arrReminder[$arrForm.reminder]|h}--><br>
	<!--{/if}--></td>
								</tr>
								<!--{if $arrForm.reminder eq 0}-->
		<!--　非表示　-->
	<!--{else}-->
		<tr>
									<th>質問の答え</th>
									<td><!--{$arrForm.reminder_answer|h}--></td>
								</tr>	<!--{/if}-->
								<tr>
									<th>メールマガジン</th>
									<td><!--{if $arrForm.mailmaga_flg eq 2}-->希望する<!--{else}-->希望しない<!--{/if}--></td>
								</tr>
							</table>
							<div class="combtn-top combtn-twocolumn">
								<ul class="clearfix">
									<li><input type="submit" name="return" value="入力画面に戻る" class="input-button"></li>
									<li><input type="submit" name="submit" value="上記内容で登録する" class="input-submit pass"></li>
								</ul>
							</div>
						</form>
					</div>
				</div>
				
			</div><!-- /#contents -->
			
		</article>
		