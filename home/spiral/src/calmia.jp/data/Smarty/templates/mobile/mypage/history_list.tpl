<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/order.css" media="all" >


    <div id="teaser">
                <div class="teaserin">
                    <h2 class="teaser-h">ご注文履歴</h2>
                    <p><img src="<!--{$TPL_URLPATH}-->img/order/order_teaser_pc.png" alt="ご注文履歴"></p>
                </div>
            </div><!-- /#teaser -->
            
            
            <p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>ご注文履歴</p>
            
            <div id="contents">
                <div class="inner">
                    <div class="order-wrap">
                        <div class="section">
        <!--{if $objNavi->all_row > 0}-->
                            <table class="com-table01 order-table">
                                <tr>
                                    <th class="w25per tac">注文番号・注文日時</th>
                                    <th class="w55per tac">注文内容</th>
                                    <th class="w20per tac">合計金額（税込）</th>
                                </tr>
                                  <!--{section name=cnt loop=$arrOrder}-->
                                    <td><p class="order-date"><!--{$arrOrder[cnt].order_id}--><a href="./history.php?order_id=<!--{$arrOrder[cnt].order_id}-->"><!--{$arrOrder[cnt].create_date|sfDispDBDate}--></a></p></td>
                                    <td>
                                        <div class="order-in">
                                        <!--{foreach item=detail from=$arrOrder[cnt].arrOrderDetail}-->
                                            <dl>
                                                <dt class="img-container--flex-box"><img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$detail.main_image|h}-->" alt="<!--{$detail.product_name|h}-->" /></dt>
                                                <dd><a<!--{if $detail.enable}--> href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$detail.product_id|u}-->"<!--{/if}-->><!--{$detail.product_name|h}--></a></dd>
                                            </dl>
                                        <!--{/foreach}-->
                                        </div>
                                    </td>
                                    <td class="tac"><p class="order-txt"><!--{$arrOrder[cnt].payment_total|number_format}-->円</p></td>
                                </tr>
                                   <!--{/section}--> 
                            </table>
         <!--{else}-->
                            <span style="font-size: 16px;">ご購入履歴はありません。</span><br>
         <!--{/if}-->
                        </div>
                        
                    </div>
                </div>
            </div><!-- /#contents -->
            
        </article>
