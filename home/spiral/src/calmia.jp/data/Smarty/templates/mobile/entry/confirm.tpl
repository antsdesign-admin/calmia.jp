<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/login.css" media="all" />
<article>
    <h2 class="login-hdl"><!--{$tpl_title|h}--></h2>
    <div id="contents">
        <div class="inner">
            <div id="form-area">
                <form name="form1" id="form1" method="post" action="<!--{$smarty.const.ROOT_URLPATH}-->entry/index.php" utn>
                    <input type="hidden" name="mode" value="complete">
                    <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                    <!--{foreach from=$arrForm key=key item=item}-->
                        <input type="hidden" name="<!--{$key|h}-->" value="<!--{$item|h}-->">
                    <!--{/foreach}-->
                    <p class="confirm-txt">下記の内容でご登録してもよろしいですか？</p>
                    <table class="com-table01">
                        <tr>
                            <th>ユーザー利用ID</th>
                            <td>
                                <!--{$arrForm.user_id|h}-->
                            </td>
                        </tr>

                        <tr>
                            <th>お名前</th>
                            <td>
                                <!--{$arrForm.name01|h}--> <!--{$arrForm.name02|h}-->
                            </td>
                        </tr>

                        <tr>
                            <th>お名前(フリガナ)</th>
                            <td>
                                <!--{$arrForm.kana01|h}--> <!--{$arrForm.kana02|h}-->
                            </td>
                        </tr>

                        <!--{if $arrForm.oldname01 eq ""}-->
                        <!--{else}-->
                        <tr>
                            <th>旧姓</th>
                            <td>
                                <!--{$arrForm.oldname01|h}-->
                            </td>
                        </tr>
                        <!--{/if}-->

                        <!--{if $arrForm.oldkana01 eq ""}-->
                        <!--{else}-->
                        <tr>
                            <th>旧姓(フリガナ)</th>
                            <td>
                                <!--{$arrForm.oldkana01|h}-->
                            </td>
                        </tr>
                        <!--{/if}-->

                        <tr>
                            <th>生年月日</th>
                            <td>
                                <!--{if strlen($arrForm.year) > 0 && strlen($arrForm.month) > 0 && strlen($arrForm.day) > 0}-->
                                    <!--{$arrForm.year|h}-->年<!--{$arrForm.month|h}-->月<!--{$arrForm.day|h}-->日生まれ
                                <!--{else}-->
                                    未登録
                                <!--{/if}-->
                            </td>
                        </tr>

                        <tr>
                            <th>住所</th>
                            <td>〒<!--{$arrForm.zip01|h}--> - <!--{$arrForm.zip02|h}-->
                                <!--{$arrPref[$arrForm.pref]|h}-->
                                <!--{$arrForm.addr01|h}-->
                                <!--{$arrForm.addr02|h}-->
                            </td>
                        </tr>

                        <tr>
                            <th>携帯番号</th>
                            <td>
                                <!--{$arrForm.cell01|h}-->-<!--{$arrForm.cell02|h}-->-<!--{$arrForm.cell03|h}-->
                            </td>
                        </tr>

                        <!--{if $arrForm.tel01 eq "" && $arrForm.tel02 eq "" && $arrForm.tel03 eq ""}-->
                        <!--{else}-->
                        <tr>
                            <th>電話番号</th>
                            <td>
                                <!--{$arrForm.tel01|h}-->-<!--{$arrForm.tel02|h}-->-<!--{$arrForm.tel03|h}-->
                            </td>
                        </tr>
                        <!--{/if}-->

                        <tr>
                            <th>メールアドレス</th>
                            <td>
                                <!--{$arrForm.email|h}-->
                            </td>
                        </tr>

                        <tr>
                            <th>パスワード確認用質問</th>
                            <td>
                                <!--{if $arrForm.reminder eq 0}--> 設定なし <!--{else}--> <!--{$arrReminder[$arrForm.reminder]|h}-->
                                <!--{/if}-->
                            </td>
                        </tr>

                        <!--{if $arrForm.reminder eq 0}-->
                        <!--　非表示　-->
                        <!--{else}-->
                        <tr>
                            <th>質問の答え</th>
                            <td>
                                <!--{$arrForm.reminder_answer|h}-->
                            </td>
                        </tr>
                        <!--{/if}-->
                    </table>
                    <p class="mt15">会員登録をされると、メールマガジンが発行されます。メールマガジン不要の場合は、<br>会員登録後変更画面から“希望しない”を選択してください。</p>
                    <div class="combtn-top combtn-twocolumn">
                        <ul class="clearfix">
                            <li><a href="javascript:;" onclick="document.form1['mode'].value = 'return';document.form1.submit();" class="input-button">入力画面に戻る</a></li>
                              <li><input type="submit" value="上記内容で登録する" class="input-submit"></li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>
</article>

