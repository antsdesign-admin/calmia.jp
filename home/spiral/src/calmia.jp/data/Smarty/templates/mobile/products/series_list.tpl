<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/shopping.css" media="all" />
    <div id="teaser">
                <div class="teaserin">

<!--{if $arrSearchData.category_id == 185}--><!--除菌ジェル-->
<h2 class="teaser-h"><span>ショッピング</span>除菌ジェル</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser17_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 161}--><!--アンペリアル-->
<h2 class="teaser-h"><span>ショッピング</span>アンペリアル</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser00_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 143}-->
<h2 class="teaser-h"><span>ショッピング</span>ドクターリセラ</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser02_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 4}-->
<h2 class="teaser-h"><span>ショッピング</span>アクアヴィーナス</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser03_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 34}-->
<h2 class="teaser-h"><span>ショッピング</span>ナチュリスティー</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser04_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 154}-->
<h2 class="teaser-h"><span>ショッピング</span>リセラベビー</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser05_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 119}-->
<h2 class="teaser-h"><span>ショッピング</span>ミラクルトックス</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser13_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 184}-->
<h2 class="teaser-h"><span>ショッピング</span>スピケア</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser16_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 157}-->
<h2 class="teaser-h"><span>ショッピング</span>エステプロラボ</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser06_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 182}-->
<h2 class="teaser-h"><span>ショッピング</span>サプリメント</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser14_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 33}-->
<h2 class="teaser-h"><span>ショッピング</span>ラクティス</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser07_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 10}-->
<h2 class="teaser-h"><span>ショッピング</span>カリス</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser08_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 120}-->
<h2 class="teaser-h"><span>ショッピング</span>シェーバー</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser09_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 171}-->
<h2 class="teaser-h"><span>ショッピング</span>リセラディーバ</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser11_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 172}-->
<h2 class="teaser-h"><span>ショッピング</span>その他</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser10_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{elseif $arrSearchData.category_id == 183}-->
<h2 class="teaser-h"><span>ショッピング</span>訳あり商品</h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser15_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{else}-->
<h2 class="teaser-h"><span>ショッピング</span><!--{$tpl_title|h}--></h2>
<p>
<img src="<!--{$TPL_URLPATH}-->img/shopping/shopping_teaser_other_pc.png" alt="<!--{$arrSearch.category|h}-->">
<!--{/if}-->
</p>
                </div>
            </div><!-- /#teaser -->


            <p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span><a href="/products/">ショッピング</a><span>&gt;</span><!--{if $tpl_subtitle|strlen >= 1}--><!--{$tpl_subtitle|h}--><!--{elseif $tpl_title|strlen >= 1}--><!--{$tpl_title|h}--><!--{/if}--></p>
            
            <div id="contents">
                <div class="section">
                    <div class="inner">
                        <div class="shoplist-wrap">
                            <div class="section">
                                <p class="shoplist-txt"><!--{$category_info}--></p>
                                <p class="shoplist-note visible-pc">※表示価格は税抜です。</p>
<!-- ▼ブランド別タブリンクエリア ここから -->

<!--{if $arrSearchData.category_id == 161}--><!--アンペリアル-->
<!--{elseif $arrSearchData.category_id == 143 || $arrSearchData.category_id == 144 || $arrSearchData.category_id == 145}--><!--ドクターリセラ-->
<div class="product_change clearfix"><ul>
<a href="/products/list.php?mode=series&series_id=143"><li>全てを見る</li></a>
<a href="/products/list.php?mode=series&series_id=145"><li>スタンダード</li></a>
<a href="/products/list.php?mode=series&series_id=144"><li>ADS</li></a>
</ul>
</div>
<!--{elseif $arrSearchData.series_id == 4 }--><!--アクアヴィーナス-->
<div class="product_change clearfix"><ul>
<a href="/products/list.php?mode=series&series_id=4"><li>全てを見る</li></a>
<a href="/products/list.php?mode=series&series_id=38"><li>スキンケアシリーズ</li></a>
<a href="/products/list.php?mode=series&series_id=39"><li>ターゲットケア</li></a>
<a href="/products/list.php?mode=series&series_id=96"><li>UVケアシリーズ</li></a>
<a href="/products/list.php?mode=series&series_id=139"><li>ヘアケア</li></a>
<a href="/products/list.php?mode=series&series_id=41"><li>ボディシリーズ</li></a>
<a href="/products/list.php?mode=series&series_id=132"><li>ボディシェイパー</li></a>
<a href="/products/list.php?mode=series&series_id=138"><li>インナーケア</li></a>
<a href="/products/list.php?mode=series&series_id=164"><li>廃版製品</li></a>
<a href="/products/list.php?mode=series&series_id=150"><li>無添加 透輝メイクシリーズアイメイク</li></a>
<a href="/products/list.php?mode=series&series_id=163"><li>無添加 透輝メイクシリーズチーク</li></a>
<a href="/products/list.php?mode=series&series_id=149"><li>無添加 透輝メイクシリーズリップ</li></a>
</ul>
</div>
<!--{elseif $arrSearchData.category_id == 79|| $arrSearchData.category_id == 34 || $arrSearchData.category_id == 83  || $arrSearchData.category_id == 109  || $arrSearchData.category_id == 109}--><!--ナチュリスティー-->
<div class="product_change clearfix"><ul>
<a href="/products/list.php?mode=series&series_id=34"><li>全てを見る</li></a>
<a href="/products/list.php?mode=series&series_id=79"><li>オーガニック</li></a>
<a href="/products/list.php?mode=series&series_id=83"><li>アクレス</li></a>
<a href="/products/list.php?mode=series&series_id=109"><li>ナチュリスティー</li></a>
</ul>
</div>
<!--{elseif $arrSearchData.category_id == 154}--><!--リセラベビー-->
<!--{elseif $arrSearchData.category_id == 157  || $arrSearchData.category_id == 158  || $arrSearchData.category_id == 160  || $arrSearchData.category_id == 159}--><!--エステプロラボ-->
<div class="product_change clearfix"><ul>
<a href="/products/list.php?mode=series&series_id=157"><li>全てを見る</li></a>
<a href="/products/list.php?mode=series&series_id=158"><li>ハーブティー</li></a>
<a href="/products/list.php?mode=series&series_id=159"><li>サプリメント</li></a>
<a href="/products/list.php?mode=series&series_id=160"><li>ドリンク</li></a>
</ul>
</div>
<!--{elseif $arrSearchData.category_id == 7}--><!--エステプロラボ-->
<div class="product_change clearfix"><ul>
<a href="/products/list.php?mode=series&series_id=157"><li>全てを見る</li></a>
</ul>
</div>
<!--{else}-->
<!--{/if}-->

<!-- ▲ブランド別タブリンクエリア ここまで -->
<div class="products-list clearfix">
<div class="name-series">▼<!--{if $tpl_subtitle|strlen >= 1}--><!--{$tpl_subtitle|h}--><!--{elseif $tpl_title|strlen >= 1}--><!--{$tpl_title|h}--><!--{/if}--></div>
<p class="shoplist-note visible-ts">※表示価格は税抜です。</p>
                                <ul class="shoplist-box clearfix">

<!--{foreach from=$arrChildren key=i item=arrChild}-->
<!--{if $arrChild.has_children}-->
<!--{assign var=path value="`$smarty.const.ROOT_URLPATH`products/series_list.php"}-->
<!--{else}-->
<!--{assign var=path value="`$smarty.const.ROOT_URLPATH`products/list.php"}-->
<!--{/if}-->



                                <!-- ▼商品 ここから -->
                                    <li>
                                        <div class="shoplist-bor hlg01 biggerlink clearfix">
                                            <p class="shoplist-img"><img height="150" src="<!--{$smarty.const.IMAGE_SAVE_URLPATH|sfTrimURL}-->/<!--{$arrProduct.main_list_image|sfNoImageMainList|h}-->" alt="<!--{$arrProduct.name|h}-->" /></p>
                                            <p class="shoplist-det"><a href="<!--{$smarty.const.MOBILE_P_DETAIL_URLPATH}--><!--{$arrProduct.product_id|u}-->"><!-- 商品名 --><!--{$arrProduct.name|h}--><br></a><span><!--{if $arrProduct.price02_min == $arrProduct.price02_max}-->
                                                <!--{if $arrSiteInfo.tax_flg eq 1}-->
                                                    <!--{$arrProduct.price02_min|number_format}-->円(税抜)
                                                <!--{else}-->
                                                    <!--{$arrProduct.price02_min|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->円(税込)
                                                <!--{/if}-->
                                            <!--{else}-->
                                                <!--{if $arrSiteInfo.tax_flg eq 1}-->
                                                    <!--{$arrProduct.price02_min|number_format}-->～<!--{$arrProduct.price02_max|number_format}-->円(税抜)
                                                <!--{else}-->
                                                    <!--{$arrProduct.price02_min|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->～<!--{$arrProduct.price02_max|sfCalcIncTax:$arrSiteInfo.tax:$arrSiteInfo.tax_rule|number_format}-->円(税込)
                                                <!--{/if}-->
                                            <!--{/if}--></span></p>
                                        </div>
                                    </li>
                              <!-- ▲商品 ここまで -->
                                <!--{foreachelse}-->
                                該当商品がありません。<br>
                              <!--{/foreach}-->
                                </ul>
                        <!--{if $tpl_strnavi != ""}-->
        </div>               
                            <div class="news-page clearfix">
                                 <!--{$tpl_strnavi}-->
                                  <p class="shoplist-btn"><a href="/products/" class="input-button">ブランド別一覧に戻る</a></p>
                            </div>
                         
                           
                      
                        <!--{/if}-->
                            </div>
                          
                        </div>
                    </div>
                </div>
            </div><!-- /#contents -->
        </article>

