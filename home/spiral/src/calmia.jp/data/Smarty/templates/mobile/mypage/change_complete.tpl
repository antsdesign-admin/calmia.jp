<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/customer.css" media="all" />
	<div id="teaser-tit">
				<h2 class="teaser-titin visible-pc">お客様情報の変更</h2>
			</div><!-- /#teaser-tit -->
			
			<p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span><a href="/mypage/change.php">お客様情報の変更</a><span>&gt;</span><a href="/mypage/change_confirm.php">お客様情報の変更 確認</a><span>&gt;</span>お客様情報の変更 完了</p>
			
			<div id="contents">
				
				<div class="inner">
					<p class="finish-wrap">お客様情報の変更を承りました。<br>引き続きショッピングをお楽しみください。</p>
				</div>
				
			</div><!-- /#contents -->
			
		</article>