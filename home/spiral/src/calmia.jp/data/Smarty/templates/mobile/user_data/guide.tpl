<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/guide.css" media="all" />
<div id="teaser">
				<div class="teaserin">
					<h2 class="teaser-h">ご注文ガイド</h2>
					<p><img src="<!--{$TPL_URLPATH}-->img/guide/guide_teaser_pc.png" alt="ご注文ガイド"></p>
				</div>
			</div>
			
			<p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>ご注文ガイド</p>
			
			<div id="contents">
				<div class="inner">
					<div class="guide-wrap clearfix">
						<div id="main">
							<div class="section" id="anchor01">
								<div class="guide-box">
									<h3 class="hdl visible-pc">会員登録について</h3>
									<h3 class="hdl hdl-plus visible-ts">会員登録について</h3>
									<div class="guide-area">
										<p class="guide-txt01">当サイトは､当社エステティックサロンカルミア・カルミア美肌クリニックにお通いいただいているお客様・患者様専用サイトです｡カルミアにご来店いただいた事のない方のご登録はできかねますので予めご了承くださいませ｡</p>
									</div>
								</div>
							</div>
							<div class="section" id="anchor02">
								<div class="guide-box">
									<h3 class="hdl visible-pc">ポイントについて</h3>
									<h3 class="hdl hdl-plus visible-ts">ポイントについて</h3>
									<div class="guide-area">
										<p class="guide-txt01 mb20">商品お買い上げ金額に対してポイントを付与させていただきます。ただし、お電話注文・メール注文の場合はポイント対象外となりますので、予めご了承くださいませ。</p>
										<ul class="com-note">
											<li>●年会費・登録料は一切無料です。</li>
											<li>●商品ご購入金額100円（税抜）につき、1ポイント付与致します。<br>例）5,000円（税抜）のピュアモイスチャーウォーターをご購入→50ポイント付与<br>但し、ポイントをご利用された場合はそのポイント利用分に対してはポイントは付与されません。</li>
											<li>●消費税・送料・振込手数料・代引き手数料はポイント付与対象外となります。</li>
											<li>●ポイントは100円（税抜）単位で付与され、100円未満は切り捨てとなります。</li>
											<li>●「1ポイント＝1円」で、次回以降お買い物の際に利用いただけます。</li>
											<li>●一部ポイントのご利用ができない場合がございます。</li>
											<li>●最終購入日より1ヶ年ご利用がない場合、積立ポイントは無効とさせていただきます。</li>
											<li>●現金とのお引換はできません。</li>
											<li>●ポイントはご本人様のみご利用いただけます。ポイントの譲渡や他の会員様との合算もできません。</li>
											<li>●システム上ポイント付与に10日間前後お時間いただきます。</li>
											<li>●退会や何らかの理由で会員資格を失った場合は、ポイントも抹消されます。</li>
											<li>●何らかの事情により商品返品になった場合、その商品に付与したポイントはご返却いただきます。</li>
										</ul>
										<p class="guide-txt01 mt20">詳しくは、カルミアお客様サポートデスクまでお問い合わせくださいませ。<br>弊社はポイントサービスの内容を任意に変更、またはポイントサービスを廃止する事ができるものとします。</p>
									</div>
								</div>
							</div>
							<div class="section" id="anchor03">
								<div class="guide-box">
									<h3 class="hdl visible-pc">お支払い方法について</h3>
									<h3 class="hdl hdl-plus visible-ts">お支払い方法について</h3>
									<div class="guide-area">
										<p class="guide-txt01">お支払い方法は代金引換と銀行振込（前払い）、クレジットカード1回払い（VISA/MASTER/JCB/AMEX）となります。 <br>■代金引換<br>代引手数料はお客様のご負担となります。<br>（代金引換手数料）<br>10,000円未満…324円（本体価格300円、消費税等24円）<br>10,000円以上～30,000円未満　432円（本体価格400円、消費税等32円）<br>30,000円以上～100,000円未満　648円（本体価格600円、消費税等48円）<br>100,000円以上～300,000円まで　1,080円（本体価格1,000円　消費税等80円）<br>※代金引換でのお支払いの場合、代金を宅配便のドライバーに直接お支払いください。<br>■銀行振込み<br>振込手数料はお客様負担となります。ご注文日より5日以内にお振り込みください。ご入金を確認次第、「ご入金を確認いたしました」メールを送信させていただきます。同時に発送の準備を開始致します。 </p>
										<p class="guide-txt01 mt20">振込先…広島銀行三川町支店<br>普通口座 3025499 　スパイラル株式会社</p>
										<p class="guide-txt01 mt20">■クレジット決済<br>VISA/MASTERS/JCB/AMEXのカードがご利用いただけます。<br>お支払い回数は1回のみとなります。</p>
									</div>
								</div>
							</div>
							<div class="section" id="anchor04">
								<div class="guide-box">
									<h3 class="hdl visible-pc">商品送料について</h3>
									<h3 class="hdl hdl-plus visible-ts">商品送料について</h3>
									<div class="guide-area">
										<p class="guide-txt01">1回のご注文合計<br>10,000円(税込)以上の場合…送料無料<br>10,000円(税込)未満の場合…全国一律送料540円</p>
									</div>
								</div>
							</div>
							<div class="section" id="anchor05">
								<div class="guide-box">
									<h3 class="hdl visible-pc">お届けについて</h3>
									<h3 class="hdl hdl-plus visible-ts">お届けについて</h3>
									<div class="guide-area">
										<p class="guide-txt01">■代金引換・クレジット決済にてご注文の場合ご注文いただいてから、3～4日以内に発送致します。<br>■銀行振込（前払い）にてご注文の場合<br>ご入金確認でき次第、3～4日で発送致します。<br>尚、「ドクターリセラADSシリーズ」のご注文、「ドクターリセラADSシリーズ」を含むご注文の場合はご入金から商品到着まで約10日間程度のお日にちがかかる場合がございます。<br>■お届け時間帯は以下よりお選びいただけます｡<br>午前中<br>14時～16時<br>16時～18時<br>18時～20時<br>19時～21時<br>但し､交通事情･天候などにより､ご希望の時間帯に添えない場合もございますので､予めご了承ください｡配達する商品をご希望の時間帯にお届け致します｡ご注文の際にお申し付けください</p>
									</div>
								</div>
							</div>
							<div class="section" id="anchor06">
								<div class="guide-box">
									<h3 class="hdl visible-pc">返品・交換について</h3>
									<h3 class="hdl hdl-plus visible-ts">返品・交換について</h3>
									<div class="guide-area">
										<p class="guide-txt01">お客様のご都合による返品（未開封･未使用の商品に限ります）返品は商品到着後10日以内までお受け致します｡配送料･代金振込手数料･代引手数料はお客様のご負担となります。<br>また、店舗でのご対応はできかねますので、予めご了承くださいませ。<br>■弊社不備による返品の場合（不良品）<br>商品お届け日から10日以内に商品を着払いにてご返送ください｡弊社より代替品を配送させていただきます｡<br>交換は､不良品（弊社不備）以外は承っておりません｡<br>■返品をお受けできない商品<br>商品がお手元に届いて10日以上経過した商品 <br>開封された商品<br>汚損･破損された商品 <br>店舗にてお買い上げいただいた商品</p>
									</div>
								</div>
							</div>
							<div class="section" id="anchor07">
								<div class="guide-box">
									<h3 class="hdl visible-pc">お問い合わせ</h3>
									<h3 class="hdl hdl-plus visible-ts">お問い合わせ</h3>
									<div class="guide-area">
										<p class="guide-txt01">その他ご不明な点などございましたら、お気軽にメール･お電話にてご連絡ください。</p>
										<div class="guide-box-in">
											<p>カルミアお客様サポートデスク<br>e-mail  info@calmia.jp<br>TEL 0120-86-3730<br>受付時間　月～金／9:30～18:00<br>（土日・年末年始・GW・お盆を除く）<br>（日曜・当社規定休日を除く）</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="side">
							<ul class="side-list scroll">
								<li><a href="#anchor01">会員登録について</a></li>
								<li><a href="#anchor02">ポイントについて</a></li>
								<li><a href="#anchor03">お支払い方法について</a></li>
								<li><a href="#anchor04">商品送料について</a></li>
								<li><a href="#anchor05">お届けについて</a></li>
								<li><a href="#anchor06">返品･交換について</a></li>
								<li><a href="#anchor07">お問い合わせ</a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</article>