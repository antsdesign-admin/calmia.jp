<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}--><link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/login.css" media="all" />
<article>
    <h2 class="login-hdl">パスワード再発行</h2>

    <div id="contents">
        <div class="inner">
            <p class="password-txt">
                パスワードの発行が完了致しました。<br>ログインには下記のパスワードをご利用ください。<br>
                <br />
            </p>

            <div class="password-txt">
                パスワード：<span class="fcred"><!--{$arrForm.new_password}--></span>
            </div>

             <div class="combtn-top combtn-twocolumn">
                    <ul class="clearfix">
                        <li style="text-align: center; width: 60%; clear: both; margin: 0 auto; float: none;"><a href="<!--{$smarty.const.MOBILE_TOP_URLPATH}-->" class="input-button">ログイン画面に戻る</a></li>
                    </ul>
                </div>

        </div>
    </div>

</article>