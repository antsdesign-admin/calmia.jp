<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->

<!--{if $tpl_login}-->
<footer class="clearfix">
    <div id="footer" class="clearfix">
        <div class="footer-link visible-ts">
            <ul>
                <li><a href="/mypage/change.php">お客様情報の変更</a></li>
                <li><a href="/order/">特定商取引に関する表記</a></li>
                <li><a href="/guide/about.php">会社概要</a></li>
                <li><a href="/guide/privacy.php">個人情報保護方針</a></li>
                <li><a href="/user_data/sitemap.php">サイトマップ</a></li>
            </ul>
        </div>
        <div class="footer-area">
            <div class="inner">
                <div class="footer-inner">
                    <dl class="footer-box">
                        <dt><a href="/" class="op"><img src="<!--{$TPL_URLPATH}-->img/common/logo.svg" width="120" alt="calmia"></a></dt>
                        <dd>
                            <div class="footer-in visible-pc">
                                <p class="footer-txt01">【お問い合わせ】カルミアお客様サポートデスク</p>
                                <p class="footer-tel01"><img src="<!--{$TPL_URLPATH}-->img/common/img_footer01.png" alt="0120863730"><span>9：30～18：00（土日祝・当社規定休日を除く）<br>E-mail：<a href="mailto:info@calmia.jp">info@calmia.jp</a></span></p>
                                <p class="footer-txt02">お電話またはE-mailでご注文いただいた場合、ポイントは付与されませんので予めご了承くださいませ。</p>
                                <p class="footer-txt03"><a href="/guide/order.php">ご注文ガイド</a> ｜ <a href="/order/">特定商取引に関する表記</a> ｜ <a href="/guide/about.php">会社概要</a> ｜ <a href="/guide/privacy.php">個人情報保護方針</a> ｜ <a href="/user_data/sitemap.php">サイトマップ</a></p>
                            </div>
                            <div class="footer-in visible-ts">
                                <p class="footersp-txt01">お問い合わせ</p>
                                <div class="footer-panel">
                                    <p>カルミアお客様サポートデスク</p>
                                    <p><a href="tel:0120-86-3730"><img src="<!--{$TPL_URLPATH}-->img/common/img_footer01.png" alt="0120863730"></a></p>
                                </div>
                                <p class="footersp-txt02">9：30～18：00（土日祝・当社規定休日を除く）<br>E-mail：<a href="mailto:info@calmia.jp">info@calmia.jp</a><br><span class="fcpink">お電話またはE-mailでご注文いただいた場合、<br>ポイントは付与されませんので予めご了承くださいませ。</span></p>
                            </div>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        <p id="copyright">copyright (c) calmia all rights reserved.</p>
    </div><!-- /#footer -->
    <p class="pagetop"><a href="#wrapper" class="op"><img class="pc" src="<!--{$TPL_URLPATH}-->img/common/page_top.png" alt="pagetop"><img class="sp" width="52" src="<!--{$TPL_URLPATH}-->img/common/page_top_sp.png" alt="pagetop"></a></p>
</footer>
</div><!-- /#wrapper -->
<div id="sidebar">

    <div class="sidebar-area">
        <p class="sidebar-logout"><a href="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php?<!--{$smarty.const.SID}-->&amp;mode=logout">ログアウト</a></p>
        <div class="sidebar-box">
            <p class="sidebar-tit">スキンケア</p>
            <ul class="sidebar-list">
                <li><a href="/products/list.php?mode=category&categorytype_id=6">&gt;&nbsp;クレンジング</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=62">&gt;&nbsp;洗顔</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=8">&gt;&nbsp;化粧水</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=9">&gt;&nbsp;美容液</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=30">&gt;&nbsp;クリーム</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=63">&gt;&nbsp;乳液</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=11">&gt;&nbsp;パック</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=43">&gt;&nbsp;角質ケア</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=66">&gt;&nbsp;UVケア</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=64">&gt;&nbsp;トライアル</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=44">&gt;&nbsp;その他スキンケア用品</a></li>
            </ul>
            <p class="sidebar-tit">メイク</p>
            <ul class="sidebar-list">
                <li><a href="/products/list.php?mode=category&categorytype_id=67">&gt;&nbsp;メイクベース</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=16">&gt;&nbsp;リキッドファンデーション</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=17">&gt;&nbsp;パウダーファンデーション</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=18">&gt;&nbsp;フェイスパウダー</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=19">&gt;&nbsp;アイブロウ</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=21">&gt;&nbsp;アイライナー</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=20">&gt;&nbsp;アイカラー</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=22">&gt;&nbsp;マスカラ</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=24">&gt;&nbsp;リップ</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=23">&gt;&nbsp;チーク</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=45">&gt;&nbsp;その他メイク用品</a></li>
            </ul>
            <p class="sidebar-tit">ボディーケア</p>
            <ul class="sidebar-list">
                <li><a href="/products/list.php?mode=category&categorytype_id=2">&gt;&nbsp;ボディーケア</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=25">&gt;&nbsp;バス用品</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=13">&gt;&nbsp;ヘアケア</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=66">&gt;&nbsp;UVケア</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=46">&gt;&nbsp;その他ボディーケア用品</a></li>
                 <li><a href="/products/list.php?mode=category&categorytype_id=15">&gt;&nbsp;その他ヘアケア用品</a></li>
            </ul>
            <p class="sidebar-tit">ボディーウェア</p>
            <ul class="sidebar-list">
                <li><a href="/products/list.php?mode=category&categorytype_id=37">&gt;&nbsp;補整下着・ガードル</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=47">&gt;&nbsp;ボディーシェイパー</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=68">&gt;&nbsp;その他ボディーウェア用品</a></li>
            </ul>
            <p class="sidebar-tit">インナーケア</p>
            <ul class="sidebar-list">
                <li><a href="/products/list.php?mode=category&categorytype_id=65">&gt;&nbsp;サプリメント</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=49">&gt;&nbsp;ドリンク</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=71">&gt;&nbsp;食品</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=50">&gt;&nbsp;その他インナーケア用品</a></li>
            </ul>
            <p class="sidebar-tit">日用品・その他</p>
            <ul class="sidebar-list">
                <li><a href="/products/list.php?mode=category&categorytype_id=31">&gt;&nbsp;洗剤</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=35">&gt;&nbsp;オーラルケア</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=70">&gt;&nbsp;ベビー用品</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=69">&gt;&nbsp;エッセンシャルオイル</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=52">&gt;&nbsp;シェーバー</a></li>
                <li><a href="/products/list.php?mode=category&categorytype_id=53">&gt;&nbsp;その他日用品</a></li>
            </ul>
        </div>
    </div>
</div><!-- /#sidebar -->

<!--{else}-->
<footer>
    <div id="footer">
        <div class="footer-area">
            <div class="inner">
                <div class="footer-inner">
                    <dl class="footer-box">
                        <dt><a href="/" class="op"><img src="<!--{$TPL_URLPATH}-->img/common/logo.svg" width="120" alt="calmia"></a></dt>
                        <dd>
                            <div class="footer-in visible-pc">
                                <p class="footer-txt01">【お問い合わせ】カルミアお客様サポートデスク</p>
                                <p class="footer-tel02"><img src="<!--{$TPL_URLPATH}-->img/common/img_footer01.png" alt="0120863730"><span>　9：30～18：00<br>（土日祝・当社規定休日を除く）<br>　E-mail：<a href="mailto:info@calmia.jp">info@calmia.jp</a></span></p>
                            </div>
                            <div class="footer-in visible-ts">
                                <p class="footersp-txt01">お問い合わせ</p>
                                <div class="footer-panel">
                                    <p>カルミアお客様サポートデスク</p>
                                    <p><a href="tel:0120-86-3730"><img src="<!--{$TPL_URLPATH}-->img/common/img_footer01.png" alt="0120863730"></a></p>
                                </div>
                                <p class="footersp-txt02">9：30～18：00（土日祝・当社規定休日を除く）<br>E-mail：<a href="mailto:info@calmia.jp">info@calmia.jp</a></p>
                            </div>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        <p id="copyright">copyright (c) calmia all rights reserved.</p>
    </div><!-- /#footer -->
    <p class="pagetop"><a href="#wrapper" class="op"><img class="pc" src="<!--{$TPL_URLPATH}-->img/common/page_top.png" alt="pagetop"><img class="sp" width="52" src="<!--{$TPL_URLPATH}-->img/common/page_top_sp.png" alt="pagetop"></a></p>
</footer>

<!--{/if}-->