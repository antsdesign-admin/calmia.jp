<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/shopping.css" media="all" />
<script type="text/javascript">//<![CDATA[
function deleteDelivSubmit(target_form) {
    if(!window.confirm('削除しても宜しいですか？')){
        return;
    }
    $('#' + target_form).submit();
}
//]]>
</script>

		<article>


<div id="teaser-tit">
				<h2 class="teaser-titin">お届け先の選択</h2>
			</div><!-- /#teaser-tit -->
			
			<p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>お届け先の選択</p>
			
			<div id="contents">
<!--{if $arrErr.deli != ""}-->
<font color="#ff0000"><!--{$arrErr.deli}--></font>
<!--{/if}-->

				<div class="inner">
					<div class="visible-pc">
						<ul class="dexpress-process-list clearfix">
							<li class="on">お届け先の確認</li>
							<li>お支払い方法・お届け時間帯</li>
							<li>入力内容のご確認</li>
							<li>完了</li>
						</ul>
					</div>


			<div class="section">
				<div class="express-table">

				<!--{section name=cnt loop=$arrAddr}-->

					<table class="com-table01">
						<tr>
							<th class="w15per">お届け先<!--{$smarty.section.cnt.iteration}--></th>
							<td class="vam">
								<div class="express-area clearfix">
									<div class="express-box01">
										<p>〒<!--{$arrAddr[cnt].zip01}-->-<!--{$arrAddr[cnt].zip02}--></p>
										<p><!--{assign var=key value=$arrAddr[cnt].pref}--><!--{$arrPref[$key]}--><!--{$arrAddr[cnt].addr01|h}-->
							<!--{if $arrAddr[cnt].addr02 != ""}-->
							<!--{$arrAddr[cnt].addr02|h}-->
							<!--{/if}--></p>
										<p><!--{$arrAddr[cnt].name01}--> <!--{$arrAddr[cnt].name02}--></p>
									</div>
									<div class="express-box02">
										<div class="clearfix">
												<form method="post" action="<!--{$smarty.const.ROOT_URLPATH}-->shopping/deliv.php">
													<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
													<input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->">
													<input type="hidden" name="deli" value="<!--{$smarty.section.cnt.iteration}-->">
													<input type="hidden" name="mode" value="customer_addr">
													<!--{if $smarty.section.cnt.first}-->
													<input type="hidden" name="other_deliv_id" value="">
													<input type="hidden" name="deliv_check" value="-1">
													<!--{else}-->
													<input type="hidden" name="other_deliv_id" value="<!--{$arrAddr[cnt].other_deliv_id}-->">
													<input type="hidden" name="deliv_check" value="<!--{$arrAddr[cnt].other_deliv_id}-->">
													<!--{/if}-->
											<p class="express-btn01">
													<input type="submit" value="ここへ送る">
											</p></form>
												<!--{if !$smarty.section.cnt.first}-->
												<p class="express-btn02">
											<form method="post" action="<!--{$smarty.const.ROOT_URLPATH}-->shopping/deliv.php" name="deliv_delete<!--{$smarty.section.cnt.iteration}-->" id="deliv_delete<!--{$smarty.section.cnt.iteration}-->">
												<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
												<input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->">
												<input type="hidden" name="mode" value="delete">
												<input type="hidden" name="other_deliv_id" value="<!--{$arrAddr[cnt].other_deliv_id}-->">
												<input type="button" value="削除する" onclick="deleteDelivSubmit('deliv_delete<!--{$smarty.section.cnt.iteration}-->');" class="reset">
											</form></p>
											<!--{/if}-->
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>
					<!--{/section}-->


				</div>
			</div>


					<div class="visible-pc">
						<div class="combtn-top combtn-twocolumn">
							<p class="express-company">新しいお届け先を追加する</p>
							<ul class="clearfix">
								<li><a href="/cart/" class="input-button">戻る</a></li>
								<li><form method="post" action="<!--{$smarty.const.ROOT_URLPATH}-->mypage/delivery_addr.php">
									<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
									<input type="hidden" name="ParentPage" value="<!--{$smarty.const.DELIV_URLPATH}-->">
									<input type="submit" value="新規登録" class="input-submit">
								</form></li>
							
							</ul>
						</div>
					</div>



					<div class="visible-ts">
						<div class="express-insp">
							<div class="combtn-twocolumn">
								<div class="clearfix">
									<p class="express-company">新しいお届け先を追加する</p>
									<p class="express-login new"><form method="post" action="<!--{$smarty.const.ROOT_URLPATH}-->mypage/delivery_addr.php">
									<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
									<input type="hidden" name="ParentPage" value="<!--{$smarty.const.DELIV_URLPATH}-->">
									<input type="submit" value="新規登録" class="input-submit deliv">
								</form></p>
								</div>
								<p class="express-back"><a href="/cart/" class="input-button">戻る</a></p>
							</div>
						</div>
					</div>



		</div>
	</div><!-- /#contents -->
			
</article>
		






