<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}--><link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/news.css" media="all" />
	
			<div id="teaser-tit">
				<h2 class="teaser-titin">新着情報</h2>
			</div><!-- /#teaser-tit -->
			
			<p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span><a href="/event/index.php">新着情報</a><span>&gt;</span><!--{$arrNews.news_date|date_format:"%y/%m/%d"}-->　<!--{$arrNews.news_title|h}--></p>
			
			<div id="contents">
			<!--{if count($arrNews) > 0}-->	
				<div class="detail-wrap">
					
					<dl class="clearfix">
						<dt><!--{$arrNews.news_date|date_format:"%y/%m/%d"}--></dt>
						<dd><!--{$arrNews.news_title|h}--></dd>
					</dl>
					<div class="inner">
						<div class="section">
							<div class="detail-box">
								<p><!--{assign var=product_name value=$arrNews.product_name|h}-->
<!--{assign var=product_id value=$arrNews.product_id|h}-->
<!--{assign var=category_name value=$arrNews.category_name|h}-->
<!--{assign var=category_id value=$arrNews.category_id|h}-->
<!--{assign var=imagefile1 value=$arrNews.arrFile.news_image1.filepath|h}-->
<!--{assign var=imagefile2 value=$arrNews.arrFile.news_image2.filepath|h}-->
<!--{assign var=file1 value="<img src=\"`$imagefile1`\" />"}-->
<!--{assign var=file2 value="<img src=\"`$imagefile2`\" />"}-->

<!--{assign var=category_id value=$arrNews.category_id|h}-->
<!--{assign var=link value="<a href=\"`$smarty.const.ROOT_URLPATH`products/detail.php?product_id=`$product_id`\">`$product_name`</a>"}-->
<!--{assign var=link1 value="<a href=\"`$smarty.const.ROOT_URLPATH`products/list.php?mode=series&series_id=`$category_id`\">`$category_name`</a>"}-->
<!--{$arrNews.news_comment|nl2br|replace:'[link]':$link|replace:'[link1]':$link1|replace:'[file1]':$file1|replace:'[file2]':$file2}-->

<!--{else}-->
ご指定の新着情報はありません。
<!--{/if}--></p>
								
							</div>
						</div>
					<div class="section">
							<div class="news-page clearfix">
								<p class="news-page-list"><a href="/event/">一覧に戻る</a></p>
							</div>
							<p class="news-page-listsp"><a href="/event/">一覧に戻る</a></p>
						</div>
						
					</div>
				</div>
				
				


				
			</div><!-- /#contents -->
			
		</article>
		