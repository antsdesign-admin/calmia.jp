<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<!--{$to_name01}--> <!--{$to_name02}--> 様
<!--{$CONF.shop_name}-->お客様サポートデスクでございます。

この度は会員登録依頼をいただきまして、有り難うございます。
現在は仮登録の状態です。
　　　~~~~~~
<!--{$to_name01}-->様の会員照会をさせていただいたのち、メールを送付させていただきます。

※1週間経ってもご連絡メールが届かない場合は、お手数ですがお電話にてご連絡ください。

<!--{$CONF.shop_name}-->お客様サポートデスク
TEL：<!--{$arrSiteInfo.tel01}-->-<!--{$arrSiteInfo.tel02}-->-<!--{$arrSiteInfo.tel03}--> <!--{if $arrSiteInfo.business_hour != ""}-->【<!--{$arrSiteInfo.business_hour}-->】<!--{/if}-->
