<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->


<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/shopping.css" media="all" />
<script src="https://zipaddr.com/js/zipaddrx.js" charset="UTF-8"></script>
<script>if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
    $('input').attr('autocomplete', 'off');
}</script>
<script>
    $(function(){
   $('.required').on('keydown keyup keypress change focus blur', function(){
       if($(this).val() == ''){
           $(this).css({backgroundColor:'#ffcccc'});
       } else {
           $(this).css({backgroundColor:'#fff'});
       }
   }).change();
});
</script>
<style>
    input:-webkit-autofill {
    -webkit-box-shadow: 0 0 0px 1000px #fff inset;
}
</style>
<article>

			
			<div id="teaser-tit">
				<h2 class="teaser-titin">お届け先の追加・変更</h2>
			</div><!-- /#teaser-tit -->
			
			<p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>お届け先の追加・変更</p>
			
			<div id="contents">
				<div class="inner">
				
					<div class="visible-pc">
						<ul class="dexpress-process-list clearfix">
							<li class="on">お届け先の確認</li>
							<li>お支払い方法・お届け時間帯</li>
							<li>入力内容のご確認</li>
							<li>完了</li>
						</ul>
					</div>

					<form name="form1" method="post" action="<!--{$smarty.const.ROOT_URLPATH}-->mypage/delivery_addr.php">
  <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
	<input type="hidden" name="mode" value="edit">
    <input type="hidden" name="ParentPage" value="<!--{$ParentPage}-->">

	
					<div class="section">
						<div id="form-area">
						
							<form method="post">
								<div class="add-box">
								<font color="#FF0000">*は必須項目です。</font><br>
									<table class="com-table01">
										<tr>
											<th>お名前（姓）<span class="fcred">※</span></th>
											<td><input type="text" id="name01" name="name01" value="<!--{$arrForm.name01|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" placeholder="（例：広島）"class="input-text required w-max">
											<br>
											<font color="#FF0000"><!--{$arrErr.name01}--></font></td>
										</tr>
										<tr>
											<th>お名前（名）<span class="fcred">※</span></th>
											<td><input type="text" id="name02" name="name02" placeholder="（例：花子）" class="input-text required w-max" value="<!--{$arrForm.name02|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" istyle="1"><br><font color="#FF0000"><!--{$arrErr.name02}--></font>
											</td>
										</tr>
										<tr>
											<th>フリガナ（姓）<span class="fcred">※</span></th>
											<td><input type="text" id="name03" name="kana01" value="<!--{$arrForm.kana01|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" placeholder="（例：ヒロシマ）" class="input-text required w-max">
												<br><font color="#FF0000"><!--{$arrErr.kana01}--></font>
											</td>
										</tr>
										<tr>
											<th>フリガナ（名）<span class="fcred">※</span></th>
											<td><input type="text" id="name04" name="kana02" value="<!--{$arrForm.kana02|h}-->" maxlength="<!--{$smarty.const.STEXT_LEN}-->" placeholder="（例：ハナコ）" value="" class="input-text required w-max">
												<br><font color="#FF0000"><!--{$arrErr.kana02}--></font>
											</td>
										</tr>
										<tr>
										<!--{assign var=key1 value="zip01"}-->
	<!--{assign var=key2 value="zip02"}-->
											<th class="vat">ご住所<span class="fcred">※</span></th>
											<td>
											
	<!--{assign var="size1" value="`$smarty.const.ZIP01_LEN+2`"}-->
	<!--{assign var="size2" value="`$smarty.const.ZIP02_LEN+2`"}-->
												<p class="form-address">〒<input id="zip" size="<!--{$size1}-->" type="text" name="zip01" value="<!--{if $arrForm.zip01 != ""}--><!--{$arrForm.zip01|h}--><!--{else}--><!--{$zip01|h}--><!--{/if}-->" maxlength="<!--{$smarty.const.ZIP01_LEN}-->" istyle="4" class="input-text required" style="height: 40px;" placeholder="123">
	&nbsp;-&nbsp;
	<input id="zip1" size="<!--{$size2}-->" type="text" name="zip02" value="<!--{if $arrForm.zip02 != ""}--><!--{$arrForm.zip02|h}--><!--{else}--><!--{$zip02|h}--><!--{/if}-->" maxlength="<!--{$smarty.const.ZIP02_LEN}-->" istyle="4"class="input-text required" style="height: 40px;" placeholder="4567">
<font color="#FF0000"><!--{$arrErr[$key1]}--><!--{$arrErr[$key2]}--></font>
       <span class="fcblue"><a href="http://www.post.japanpost.jp/zipcode/" target="_blank">郵便番号検索</a></span></p>
											
									
												<select name="pref" id="pref" class="mt15 w150">
		<option value="">都道府県を選択</option>
		<!--{html_options options=$arrPref selected=$arrForm.pref}-->
	</select><br><font color="#FF0000"><!--{$arrErr.pref}--></font>
											
												<p class="mt15">
												市町村区番地（例：○○市○○区○○町1-2）<br>
	<input type="text" id="city" name="addr01" value="<!--{$arrForm.addr01|h}-->" istyle="1" class="input-text required w-max"></p><font color="#FF0000"><!--{$arrErr.addr01}--></font>
												<p class="mt15">
												ビル・マンション名（例：〇〇マンション 102号室）<br>
	<input type="text" id="address04" name="addr02" value="<!--{$arrForm.addr02|h}-->" istyle="1" class="input-text w-max"><font color="#FF0000"><!--{$arrErr.addr02}--></font>
</p>
												<p class="mt15 fcred fs12">住所は2つに分けてご記入ください。マンション名は必ず記入してください。</p>
											</td>
										</tr>
										<tr>
											<th>携帯番号<span class="fcred">※</span></th>
											<td><p class="form-tel">
	<!--{assign var="size" value="`$smarty.const.TEL_ITEM_LEN+2`"}-->
	<input type="tel" id="phone01" size="<!--{$size}-->" name="cell01" value="<!--{$arrForm.cell01|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" style="height: 40px;" istyle="4" class="input-text required">
	<span>－</span>
	<input type="tel" id="phone02" size="<!--{$size}-->" name="cell02" value="<!--{$arrForm.cell02|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" style="height: 40px;" istyle="4" class="input-text required">
	<span>－</span>
	<input type="tel" id="phone03" size="<!--{$size}-->" name="cell03" value="<!--{$arrForm.cell03|h}-->" maxlength="<!--{$smarty.const.TEL_ITEM_LEN}-->" style="height: 40px;" istyle="4" class="input-text required"><br>
<font color="#FF0000"><!--{$arrErr.cell01}--><!--{$arrErr.cell02}--><!--{$arrErr.cell03}--></font>
</p></td>
										</tr>
									</table>
								</div>
								<div class="combtn-top combtn-twocolumn">
									<ul class="clearfix">
									<li><a href="/shopping/deliv.php" class="visible-pc input-button">戻る</a></li>
										<li><input type="submit" name="submit" value="上記内容で登録する" class="input-submit">

	<!--{foreach from=$list_data key=key item=item}-->
		<input type="hidden" name="<!--{$key|h}-->" value="<!--{$item|h}-->">
	<!--{/foreach}--></li>
<li><a href="/shopping/deliv.php" class="visible-ts input-button">戻る</a></li>
									</ul>
								</div>
							</form>
						</div>
					</div>
				</div>
				</form>
			</div><!-- /#contents -->
			
		</article>





