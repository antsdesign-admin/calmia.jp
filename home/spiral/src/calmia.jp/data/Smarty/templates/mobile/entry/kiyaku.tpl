<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/login.css" media="all" />

<article>
    <h2 class="login-hdl">新規会員登録</h2>
    <div id="contents" class="statement-box">
        <div class="inner">

            <div class="section">
                <p class="statement-txt">ご注文に際して必要な内容（ご自宅住所など）をご登録していただきます。<br>ご利用の規約をよくお読みの上、ご登録ください。</p>
            </div>

            <div class="section">
                <h3 class="hdl">第1条 （会員）</h3>
                <p><span class="fwb">1.</span> 「会員」とは、当社が定める手続に従い本規約に同意の上、入会の申込みを行う個人をいいます。<br><span class="fwb">2.</span> 「会員情報」とは、会員が当社に開示した会員の属性に関する情報および会員の取引に関する履歴等の情報をいいます。<br><span class="fwb">3.</span> 本規約は、すべての会員に適用され、登録手続時および登録後にお守りいただく規約です。</p>
            </div>

            <div class="section">
                <h3 class="hdl">第2条 （登録）</h3>
                <p><span class="fwb">1. 会員資格</span><br>本規約に同意の上、所定の入会申込みをされたお客様は、所定の登録手続き完了後に会員としての資格を有します。会員登録手続きは、会員となるご本人が行ってください。代理による登録は一切認められません。なお、過去に会員資格が取り消された方やその他当社が相応しくないと判断した方からの会員申込みはお断りする場合があります。<br><span class="fwb">2. 会員情報の入力</span><br>会員登録手続きの際には、入力上の注意をよく読み、所定の入力フォームに必要事項を正確に入力してください。会員情報の登録において、特殊記号・旧漢字・ローマ数字などはご使用になれません。これらの文字が登録された場合は当社にて変更致します。<br><span class="fwb">3. パスワードの管理</span><br>(1)パスワードは会員本人のみが利用できるものとし、第三者に譲渡・貸与できないものとします。<br>(2)パスワードは、他人に知られることがないよう定期的に変更するなど、会員本人が責任をもって管理してください。<br>(3)パスワードを用いて当社に対して行われた意思表示は、会員本人の意思表示とみなし、そのために生じる支払いなどはすべて会員の責任となります。</p>
            </div>

            <div class="section">
                <h3 class="hdl">第3条 （変更）</h3>
                <p><span class="fwb">1.</span> 会員は、氏名、住所など当社に届け出た事項に変更があった場合には、速やかに当社に連絡するものとします。<br><span class="fwb">2.</span> 変更登録がなされなかった事により生じた損害について、当社は一切責任を負いません。また、変更登録がなされた場合でも、変更登録前にすでに手続きがなされた取引きは、変更登録前の情報に基づいて行われますのでご注意ください。</p>
            </div>

            <div class="section">
                <h3 class="hdl">第4条 （退会）</h3>
                <p>会員が退会を希望する場合には、会員本人が退会手続きを行ってください。所定の退会手続きの終了後に、退会となります。</p>
            </div>

            <div class="section">
                <h3 class="hdl">第5条（会員資格の喪失及び賠償義務）</h3>
                <p><span class="fwb">1.</span> 会員が、会員資格取得申し込みの際に虚偽の申告をしたとき、通信販売による代金支払債務を怠ったとき、その他当社が会員として不適当と認める事由があるときは、当社は、会員資格を取り消すことができる事とします。<br><span class="fwb">2.</span> 会員が、以下の各号に定める行為をしたときは、これにより当社が被った損害を賠償する責任を負います。<br>(1)会員番号、パスワードを不正に使用する事<br>(2)当ホームページにアクセスして情報を改ざんしたり、当ホームページに有害なコンピュータプログラムを送信するなどして、当社の営業を妨害する事<br>(3)当社が扱う商品の知的所有権を侵害する行為をする事<br>(4)その他、この利用規約に反する行為をする事</p>
            </div>

            <div class="section">
                <h3 class="hdl">第6条 （会員情報の取扱い）</h3>
                <p><span class="fwb">1.</span> 当社は、原則として会員情報を会員の事前の同意なく第三者に対して開示する事はありません。ただし、次の各号の場合には、会員の事前の同意なく、当社は会員情報その他のお客様情報を開示できるものとします。<br>(1)法令に基づき開示を求められた場合<br>(2)当社の権利、利益、名誉等を保護するために必要であると当社が判断した場合<br><span class="fwb">2.</span> 会員情報につきましては、当社の「個人情報保護への取組み」に従い、当社が管理します。当社は、会員情報を、会員へのサービス提供、サービス内容の向上、サービスの利用促進、およびサービスの健全かつ円滑な運営の確保を図る目的のために、当社おいて利用する事ができるものとします。<br><span class="fwb">3.</span> 当社は、会員に対して、メールマガジンその他の方法による情報提供(広告を含みます)を行う事ができるものとします。会員が情報提供を希望しない場合は、当社所定の方法に従い、その旨を通知していただければ、情報提供を停止します。ただし、本サービス運営に必要な情報提供につきましては、会員の希望により停止をする事はできません。</p>
            </div>

            <div class="section">
                <h3 class="hdl">第7条 （禁止事項）</h3>
                <p>本サービスの利用に際して、会員に対し次の各号の行為を行う事を禁止します。<br><span class="fwb">1.</span> 法令または本規約、本サービスご利用上のご注意、本サービスでのお買い物上のご注意その他の本規約等に違反する事<br><span class="fwb">2.</span> 当社、およびその他の第三者の権利、利益、名誉などを損ねる事<br><span class="fwb">3.</span> 青少年の心身に悪影響を及ぼす恐れがある行為、その他公序良俗に反する行為を行う事<br><span class="fwb">4.</span> 他の利用者その他の第三者に迷惑となる行為や不快感を抱かせる行為を行う事<br><span class="fwb">5.</span> 虚偽の情報を入力する事<br><span class="fwb">6.</span> 有害なコンピュータプログラム、メールなどを送信または書き込む事<br><span class="fwb">7.</span> 当社のサーバその他のコンピュータに不正にアクセスする事<br><span class="fwb">8.</span> パスワードを第三者に貸与・譲渡する事、または第三者と共用する事<br><span class="fwb">9.</span> その他当社が不適切と判断する事</p>
            </div>

            <div class="section">
                <h3 class="hdl">第8条（サービスの中断・停止等）</h3>
                <p><span class="fwb">1.</span> 当社は、本サービスの稼動状態を良好に保つために、次の各号の一に該当する場合、予告なしに、本サービスの提供全てあるいは一部を停止する事があります。<br>(1)システムの定期保守および緊急保守のために必要な場合<br>(2)システムに負荷が集中した場合<br>(3)火災、停電、第三者による妨害行為などによりシステムの運用が困難になった場合<br>(4)その他、止むを得ずシステムの停止が必要と当社が判断した場合</p>
            </div>

            <div class="section">
                <h3 class="hdl">第9条 （サービスの変更・廃止）</h3>
                <p>当社は、その判断によりサービスの全部または一部を事前の通知なく、適宜変更・廃止できるものとします。</p>
            </div>

            <div class="section">
                <h3 class="hdl">第10条（免責）</h3>
                <p><span class="fwb">1.</span> 通信回線やコンピュータなどの障害によるシステムの中断・遅滞・中止・データの消失、データへの不正アクセスにより生じた損害、その他当社のサービスに関して会員に生じた損害について、当社は一切責任を負わないものとします。<br><span class="fwb">2.</span> 当社は、当社のウェブページ・サーバ・ドメインなどから送られるメール・コンテンツに、コンピュータ・ウィルスなどの有害なものが含まれていないことを保証致しません。<br><span class="fwb">3.</span> 会員が本規約等に違反した事によって生じた損害については、当社は一切責任を負いません。</p>
            </div>

            <div class="section">
                <h3 class="hdl">第11条（本規約の改定）</h3>
                <p>当社は、本規約を任意に改定できるものとし、また、当社において本規約を補充する規約(以下「補充規約」といいます)を定める事ができます。本規約の改定または補充は、改定後の本規約または補充規約を当社所定のサイトに掲示したときにその効力を生じるものとします。この場合、会員は、改定後の規約および補充規約に従うものと致します。</p>
            </div>

            <div class="section">
                <h3 class="hdl">第12条 （準拠法、管轄裁判所）</h3>
                <p>本規約に関して紛争が生じた場合、当社本店所在地を管轄する地方裁判所を第一審の専属的合意管轄裁判所とします。</p>
            </div>

            <div class="combtn-top combtn-twocolumn">
                <ul class="clearfix">
                    <li><a href="<!--{$smarty.const.MOBILE_TOP_URLPATH}-->" class="input-button">ログイン画面に戻る</a></li>
                     <li><a class="input-submit" href="<!--{$smarty.const.HTTPS_URL}-->entry/<!--{$smarty.const.DIR_INDEX_PATH}-->">同意して登録へ</a><span class="agree-no"><a class="agree-no" href="<!--{$smarty.const.MOBILE_TOP_URLPATH}-->">同意しない</a></span></li>
                </ul>
            </div>
        </div>
    </div><!-- /#contents -->
</article>

