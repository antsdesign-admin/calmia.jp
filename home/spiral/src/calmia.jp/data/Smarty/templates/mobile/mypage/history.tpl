<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/order.css" media="all" >




  <div id="teaser">
        <div class="teaserin">
          <h2 class="teaser-h">ご注文履歴</h2>
          <p><img src="<!--{$TPL_URLPATH}-->img/order/order_teaser_pc.png" alt="ご注文履歴"></p>
        </div>
      </div><!-- /#teaser -->
      
      
      <p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span><a href="/mypage/history_list.php">ご注文履歴</a><span>&gt;</span><!--{$tpl_arrOrderData.create_date|sfDispDBDate}--></p>
      
      <div id="contents">
        <div class="inner">
          <div class="order-wrap">
            <div class="section">
              <table class="com-table01 order-table">
                <tr>
                  <th class="w75per tac">ご注文詳細</th>
                  <th class="w25per tac">小計</th>
                </tr>

  <!--{foreach from=$tpl_arrOrderDetail item=orderDetail}-->
                <tr>
                  <td>
                    <div class="order-detail-box">
                      <dl>
                        <dt><!--{assign var=product_id value=$orderDetail.product_id}-->
      <img src="<!--{$smarty.const.IMAGE_SAVE_URLPATH}--><!--{$arrOrderProductImage[$product_id]|h}-->" alt="<!--{$orderDetail.product_name|h}-->" /></dt>
                        <dd>
                          <div class="order-detail-in clearfix">
                            <p class="order-detail-link"><a<!--{if $orderDetail.enable}--> href="<!--{$smarty.const.P_DETAIL_URLPATH}--><!--{$orderDetail.product_id|u}-->"<!--{/if}-->><!--{$orderDetail.product_name|h}--></a></p>
                            <div class="order-detail-area">
                            <!--{assign var=quantity value=`$orderDetail.quantity`}-->
                              <p>【個数】<!--{$quantity|h}--></p>
                              <!--{assign var=price value=`$orderDetail.price`}-->
                              <p>【単価】<!--{if $arrSiteInfo.tax_flg eq 1}-->
          <!--{$price|number_format|h}-->円
      <!--{else}-->
          <!--{$price|sfCalcIncTax:$orderDetail.tax_rate:$orderDetail.tax_rule|number_format|h}-->円
      <!--{/if}--></p>
                            </div>
                          </div>
                        </dd>
                      </dl>
                    </div>
                  </td>
                  <td class="tac"><!--{if $arrSiteInfo.tax_flg eq 1}-->
          <!--{$price|sfMultiply:$quantity|number_format}-->円
      <!--{else}-->
          <!--{$price|sfCalcIncTax:$orderDetail.tax_rate:$orderDetail.tax_rule|sfMultiply:$quantity|number_format}-->円
      <!--{/if}--></td>
                </tr>
              <!--{/foreach}-->
              </table>
            </div>
            <div class="order-detail-main clearfix">
              <div class="order-detail-panel01">
                <table class="com-table01 order-table">
                  <tr>
                    <th class="w40per">商品合計</th>
                    <td class="tar"><!--{if $arrSiteInfo.tax_flg eq 1}-->
          <!--{$tpl_arrOrderData.subtotal_outtax|number_format}-->円
      <!--{else}-->
          <!--{$tpl_arrOrderData.subtotal|number_format}-->円
      <!--{/if}--></td>
                  </tr><!--{assign var=key value="deliv_fee"}-->
                  <tr>
                    <th>送料</th>
                    <td class="tar">
      <!--{if $arrSiteInfo.tax_flg eq 1}-->
          <!--{$tpl_arrOrderData[$key]|number_format|h}-->円
      <!--{else}-->
          <!--{$tpl_arrOrderData[$key]|sfCalcIncTax:$tpl_arrOrderData.order_tax_rate:$tpl_arrOrderData.order_tax_rule|number_format|h}-->円
      <!--{/if}--></td>
                  </tr>
                  <!--{assign var=point_discount value="`$tpl_arrOrderData.use_point*$smarty.const.POINT_VALUE`"}-->
                  <!--{if $point_discount > 0}-->
                  <tr>
                    <th>ポイント値引き</th>
                    <td class="tar"><!--{$point_discount|number_format}-->円</td>
                  </tr><!--{/if}-->

  <!--{assign var=key value="discount"}-->
  <!--{if $tpl_arrOrderData[$key] != "" && $tpl_arrOrderData[$key] > 0}-->
 <tr>
                    <th>値引き</th>
                    <td class="tar"><!--{$tpl_arrOrderData[$key]|number_format}-->円</td>
                  </tr>
  <!--{/if}-->
  <!--{assign var=key value="charge"}-->
  <!--{if $tpl_arrOrderData[$key] != "" && $tpl_arrOrderData[$key] > 0}-->
  <tr>
                    <th>手数料</th>
                    <td class="tar"><!--{assign var=key value="charge"}-->
      <!--{if $arrSiteInfo.tax_flg eq 1}-->
          <!--{$tpl_arrOrderData[$key]|number_format|h}-->円
      <!--{else}-->
          <!--{$tpl_arrOrderData[$key]|sfCalcIncTax:$tpl_arrOrderData.order_tax_rate:$tpl_arrOrderData.order_tax_rule|number_format|h}-->円
      <!--{/if}--></td>
                  </tr>
<!--{/if}-->
  <!--{if $arrSiteInfo.tax_flg eq 1 && $tpl_arrOrderData.tax > 0}-->

                  <tr>
                    <th>消費税</th>
                    <td class="tar"><!--{$tpl_arrOrderData.tax|number_format}-->円</td>
                  </tr>
                   <!--{/if}-->
                  <tr class="order-detail-bor">
                    <th>総計</th>
                    <td class="tar"><!--{$tpl_arrOrderData.payment_total|number_format}-->円</td>
                  </tr>
                </table>
              </div>
              <div class="order-detail-panel02">
               
                <table class="com-table02">
                <!--{if $tpl_arrOrderData.deliv_time_id != ""}-->
                 <tr>
                    <th class="w20per">お届け時間帯</th>
                    <td><!--{$arrDelivTime[$tpl_arrOrderData.deliv_time_id]|h}--></td>
                  </tr>
                 <!--{/if}-->
                  <tr>
                    <th class="w20per">支払い方法</th>
                    <td><!--{$arrPayment[$tpl_arrOrderData.payment_id]|h}--></td>
                  </tr>
                  <tr>
                    <th>注文日時</th>
                    <td><!--{$tpl_arrOrderData.create_date|sfDispDBDate}--></td>
                  </tr>
                   <!--{foreach item=shippingItem name=shippingItem from=$arrShipping}-->
                  <tr>
                    <th>氏名</th>
                    <td><!--{$shippingItem.shipping_name01|h}-->&nbsp;<!--{$shippingItem.shipping_name02|h}--></td>
                  </tr>
                  <tr>
                    <th>フリガナ</th>
                    <td><!--{$shippingItem.shipping_kana01|h}-->&nbsp;<!--{$shippingItem.shipping_kana02|h}--></td>
                  </tr>
                  <tr>
                    <th>連絡先</th>
                    <td><!--{$shippingItem.shipping_tel01}-->-<!--{$shippingItem.shipping_tel02}-->-<!--{$shippingItem.shipping_tel03}--></td>
                  </tr>
                  <tr>
                    <th>郵便番号</th>
                    <td>〒<!--{$shippingItem.shipping_zip01}-->-<!--{$shippingItem.shipping_zip02}--></td>
                  </tr>
                  <tr>
                    <th>住所</th>
                    <td><!--{$arrPref[$shippingItem.shipping_pref]}--><!--{$shippingItem.shipping_addr01|h}--><!--{$shippingItem.shipping_addr02|h}--></td>
                  </tr>
                   <!--{/foreach}-->
                </table>
              </div>
            </div>

            <div class="order-page-navi">
              <div class="clearfix">
                <!--{if $tpl_prev_id}--><p class="order-page-prev"><a href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/history.php?order_id=<!--{$tpl_prev_id|h}-->">&lt; 前へ</a></p><!--{/if}-->
                <!--{if $tpl_next_id}--><p class="order-page-next"><a href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/history.php?order_id=<!--{$tpl_next_id|h}-->">次へ &gt;</a></p><!--{/if}-->
              </div>
              <p class="order-page-back"><a href="<!--{$smarty.const.ROOT_URLPATH}-->mypage/history_list.php">一覧に戻る</a></p>
            </div>
          </div>
        </div>
      </div><!-- /#contents -->
      
    </article>