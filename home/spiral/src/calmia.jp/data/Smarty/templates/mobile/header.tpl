<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *}-->

<!--{if $tpl_login}-->

<div class="header-box clearfix">
                    <div class="header-logo visible-pc">
                        <dl>
                            <dt><a href="/" class="op"><img src="<!--{$TPL_URLPATH}-->img/common/logo.svg" width="85" alt="calmia"></a></dt>
                     
                        </dl>
                    </div>
                    <div class="header-logo visible-ts">
                        <dl>
                            <dt><a href="/"><img src="<!--{$TPL_URLPATH}-->img/common/logo.svg" width="52" height="52" alt="calmia"></a></dt>
                        </dl>
                    </div>
                    <div class="header-area visible-pc">
                        <form method="post">
                            <ul class="header-list clearfix">
                                <li><a class="opa" href="<!--{$smarty.server.PHP_SELF|escape}-->" onclick="fnFormModeSubmit('login_form', 'logout', '', ''); return false;"><a class="ic-move" href="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php?<!--{$smarty.const.SID}-->&amp;mode=logout"><img class="ic-move2" src="<!--{$TPL_URLPATH}-->img/common/logout_header.jpg" alt="logout">ログアウト</a>
                            </a></li>
                                <li><a class="ic-move" href="/cart/"><img class="cart-move" src="<!--{$TPL_URLPATH}-->img/common/cart.svg" width="25" height="25" alt="cart">カゴの中を見る</a></li>
  
                            </ul>
                        </form>
        <!--▼現在のポイント-->
        <!--{if $point_disp !== false}-->
                        <p class="header-txt01"><!--{$CustomerName1|h}--> <!--{$CustomerName2|h}-->さん　いつもありがとうございます。現在のポイントは <!--{if $smarty.const.USE_POINT !== false}--><span class="fcred"><!--{$CustomerPoint|number_format|default:"0"|h}--></span> ポイントです。<!--{/if}--></p>
                    </div> <!--{/if}-->
        <!--▲現在のポイント-->
                    <div class="header-area visible-ts">
                        <ul class="header-list clearfix">
                            <li><a href="/cart/"><img src="<!--{$TPL_URLPATH}-->img/common/cart.svg" width="25" height="25" alt="cart"></a></li>
                            <li><p class="search-in" data-target=".headersp-search"><a href="javascript:void(0)"><img src="<!--{$TPL_URLPATH}-->img/common/search.svg" alt="search" width="25" height="25"></a></p></li>
                            <li><p class="navbar-toggle" data-target=".navbar-collapse"><a href="javascript:void(0)">menu</a></p></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <nav class="navbar-collapse">
            <div class="navbar-box">
                <ul>
                    <li><a href="/">HOME</a></li>
                    <li><a href="/products/">ショッピング</a></li>
                    <li><a href="/mypage/favorite.php">お気に入り</a></li>
                    <li><a href="/mypage/history_list.php">ご注文履歴</a></li>
                    <li><a href="/guide/order.php">ご注文ガイド</a></li>
                    <li><a href="/mypage/change.php">お客様情報の変更</a></li>
                    <li><a href="/order/">特定商取引に関する表記</a></li>
                    <li><a href="/guide/about.php">会社概要</a></li>
                    <li><a href="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php?<!--{$smarty.const.SID}-->&amp;mode=logout">ログアウト</a></li>
                </ul>
            </div>
        </nav>
        <div id="gnavi" class="visible-pc">
            <div class="gnavi-in">
                <p class="header-menu"><a href="javascript:void(0)"><img src="<!--{$TPL_URLPATH}-->img/common/menu_navi.png" alt="menu"></a></p>
                <ul class="gnavi-list">
                    <li><a href="/">HOME</a></li>
                    <li><a href="/products/">ショッピング</a></li>
                    <li><a href="/mypage/favorite.php">お気に入り</a></li>
                    <li><a href="/mypage/history_list.php">ご注文履歴</a></li>
                    <li><a href="/mypage/change.php">お客様情報の変更</a></li>
                </ul>
            </div>
        </div>
    </div><!-- /#header -->
</header>

<article>
            <div class="header-menu-box visible-ts">
                <div class="inner header-menusp">
                    <dl>
                        <dt><a href="javascript:void(0)"><img src="<!--{$TPL_URLPATH}-->img/common/menu_navi_ts.png" alt="menu"></a></dt>
                        <dd><!--▼現在のポイント-->
        <!--{if $point_disp !== false}-->
                        <!--{$CustomerName1|h}--> <!--{$CustomerName2|h}-->さん　いつもありがとうございます。<br>現在のポイントは <!--{if $smarty.const.USE_POINT !== false}--><span class="fcred"><!--{$CustomerPoint|number_format|default:"0"|h}--></span> ポイントです。<!--{/if}--></dd>
                    <!--{/if}-->
        <!--▲現在のポイント-->
                    </dl>
                </div>
            </div>


<!--{else}-->


    <div id="header-login">
        <h1><a href="<!--{$smarty.const.MOBILE_TOP_URLPATH}-->"><img src="<!--{$TPL_URLPATH}-->img/common/logo.svg" width="120" alt="カルミアメンバーズサイト"></a></h1>
    </div><!-- /#header-login -->
    </div><!-- /#header -->
</header>

<article>

<!--{/if}-->


<!--{* タイトル
<!--{if $tpl_title != "" || $tpl_subtitle != ""}-->
	<!--{if $tpl_title != ""}-->
		<!--{if $tpl_title_marquee == 1}-->
			<!--{$tpl_title|h}-->
		<!--{else}-->
			<!--{$tpl_title|h}-->
		<!--{/if}-->
	<!--{/if}-->
	<!--{if $tpl_subtitle != ""}-->
		<!--{$tpl_subtitle|h}-->
	<!--{/if}-->
	<!--{if $tpl_sub2title != ""}-->
		<!--{$tpl_sub2title|h}-->
	<!--{/if}-->
<!--{/if}-->
*}-->



<!--▼HEADER-->
<!--{* ▼HeaderInternal COLUMN*}-->
<!--{if $arrPageLayout.HeaderInternalNavi|@count > 0}-->
    <!--{* ▼上ナビ *}-->
    <!--{foreach key=HeaderInternalNaviKey item=HeaderInternalNaviItem from=$arrPageLayout.HeaderInternalNavi}-->
        <!-- ▼<!--{$HeaderInternalNaviItem.bloc_name}--> -->
        <!--{if $HeaderInternalNaviItem.php_path != ""}-->
            <!--{include_php file=$HeaderInternalNaviItem.php_path}-->
        <!--{else}-->
            <!--{include file=$HeaderInternalNaviItem.tpl_path}-->
        <!--{/if}-->
        <!-- ▲<!--{$HeaderInternalNaviItem.bloc_name}--> -->
    <!--{/foreach}-->
    <!--{* ▲上ナビ *}-->
<!--{/if}-->
<!--{* ▲HeaderInternal COLUMN*}-->
<!--▲HEADER-->
