<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->
<link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/login.css" media="all" />
<style>
  #search_form {
    display: none;
  }
</style>
 <div id="header-login">
        <h1><img src="<!--{$TPL_URLPATH}-->img/common/logo.svg" width="120" alt="カルミアメンバーズサイト"></h1>
    </div><!-- /#header-login -->
    </div><!-- /#header -->
</header>


<article>
    <div id="contents">
        <div class="inner tac">
            <p class="finish-wrap">
                <!--{$tpl_name01|h}--> <!--{$tpl_name02|h}--> 様<br>
               会員登録の受付を完了致しました。<br>
現在は仮登録の状態です。<br>
<!--{$tpl_name01|h}-->様の会員照会をさせていただいたのち、本登録完了のご案内メールを送付させていただきます。<br>

※1週間経ってもご連絡メールが届かない場合は、お手数ですがお電話にてご連絡ください。<br>
<br>
カルミアお客様サポートデスク<br>
TEL：0120-86-3730 【月～金/9:30～18:00　(土日祝・年末年始・GW・お盆を除く）】<br>
<!--<br>
<a href="/mypage/login.php">>ログイン画面に戻る</a>-->

            </p>
        </div>
    </div>
</article>
<footer>
    <div id="footer">
        <div class="footer-area">
            <div class="inner">
                <div class="footer-inner">
                    <dl class="footer-box">
                        <dt><img src="<!--{$TPL_URLPATH}-->img/common/logo.svg" width="120" alt="calmia"></dt>
                        <dd>
                            <div class="footer-in visible-pc">
                                <p class="footer-txt01">【お問い合わせ】カルミアお客様サポートデスク</p>
                                <p class="footer-tel02"><img src="<!--{$TPL_URLPATH}-->img/common/img_footer01.png" alt="0120863730"><span>　9：30～18：00<br>（土日祝・当社規定休日を除く）<br>E-mail：<a href="mailto:info@calmia.jp">info@calmia.jp</a></span></p>
                            </div>
                            <div class="footer-in visible-ts">
                                <p class="footersp-txt01">お問い合わせ</p>
                                <div class="footer-panel">
                                    <p>カルミアお客様サポートデスク</p>
                                    <p><a href="tel:0120-86-3730"><img src="<!--{$TPL_URLPATH}-->img/common/img_footer01.png" alt="0120863730"></a></p>
                                </div>
                                <p class="footersp-txt02">9：30～18：00（土日祝・当社規定休日を除く）<br>E-mail：<a href="mailto:info@calmia.jp">info@calmia.jp</a></p>
                            </div>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        <p id="copyright">copyright (c) calmia all rights reserved.</p>
    </div><!-- /#footer -->
    <p class="pagetop"><a href="#wrapper" class="op"><img src="<!--{$TPL_URLPATH}-->img/common/page_top.png" alt="pagetop"></a></p>
</footer>