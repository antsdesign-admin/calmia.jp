<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}--><link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/login.css" media="all" />
		<article>
			<h2 class="login-hdl">必ずお読みください</h2>
			<div id="contents" class="statement-box">
				<div class="inner">
					<div class="read-main">
						<p class="read-tit">ドメイン指定について</p>
						<p>カルミアメンバーズサイト登録前に必ず下記ドメインをメール受信指定に追加してください｡</p>
						<p class="read-txt01">コピー用ドメイン<span>calmia.jp</span></p>
						<p>ドメインを追加せずに会員登録手続きをしてしまうと､ご登録確認メールが届かない為､会員登録が完了致しませんのでご注意ください｡ </p>
						<div class="read-box01">
							<p><strong>【通信キャリア別設定方法】</strong></p>
							<p>機種および通信キャリアの仕様変更などにより､下記の設定と異なる場合がございます。<br>設定がうまくいかない場合は､携帯電話､スマートフォン､iPhoneの各携帯会社(NTTドコモ､au､ソフトバンク)サイトをご確認ください。</p>
						</div>
					</div>
				</div>
			</div>
			<div class="read-model">
				<div class="read-area">
					<h2 class="hdl-plus read-hdl">docomo (スマートフォン／iPhone)</h2>
					<div class="read-plus">
						<h3 class="read-hdl-s">1.設定画面</h3>
						<div class="statement-box">
							<div class="inner">
								<div class="read-main">
									<p>下記のSPモード設定サイトより「メール設定」画面にアクセスしてください。</p>
									<p class="read-btn"><a href="https://spmode.ne.jp/mail_setting/" target="_blank">SPモード設定サイト</a></p>
								</div>
							</div>
						</div>
						<h3 class="read-hdl-s">2.ドメイン指定受信設定</h3>
						<div class="statement-box">
							<div class="inner">
								<div class="read-main">
									<p>「メール設定」画面が表示されましたら、下記の手順で操作を実施してください。</p>
									<div class="read-table01">
										<table class="com-table01">
											<tr>
												<th class="w10per">1</th>
												<td>「メール設定」画面を下へスクロールし、<br>「指定受信／拒否設定」を選択してください。</td>
											</tr>
											<tr>
												<th>2</th>
												<td>「設定を利用する」を選択し、「次へ」を押してください。</td>
											</tr>
											<tr>
												<th>3</th>
												<td>「受信するメールの設定」を選択してください。</td>
											</tr>
											<tr>
												<th>4</th>
												<td>「受信するメールの登録」の「+さらに追加する」を選択してください。</td>
											</tr>
											<tr>
												<th>5</th>
												<td>入力欄の中に「calmia.jp」と入力し、「確認する」を押してください。</td>
											</tr>
											<tr>
												<th>6</th>
												<td>「設定を確定する」を押してください。</td>
											</tr>
										</table>
									</div>
									
								</div>
							</div>
						</div>
						<h3 class="read-hdl-s">3.完了</h3>
						<div class="statement-box">
							<div class="inner">
								<div class="read-main">
									<p class="sankou">参考：<a href="https://www.nttdocomo.co.jp/info/spam_mail/spmode/domain/index.html" target="_blank">docomo 指定受信／拒否設定</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="read-model">
				<div class="read-area">
					<h2 class="hdl-plus read-hdl">au（スマートフォン／auメール）</h2>
					<div class="read-plus">
						<h3 class="read-hdl-s">1.ドメイン指定受信設定</h3>
						<div class="statement-box">
							<div class="inner">
								<div class="read-main">
									<p>auのスマートフォン（auメール）の方は､下記の手順で｢ドメイン指定受信設定｣を実施してください。</p>
									<div class="read-table01">
										<table class="com-table01">
											<tr>
												<th class="w10per">1</th>
												<td>「auメールアプリ」を押して起動します。</td>
											</tr>
											<tr>
												<th>2</th>
												<td>画面左上にある「メニューキー」を押してください。</td>
											</tr>
											<tr>
												<th>3</th>
												<td>「アドレス変更／フィルター設定」を選択してください。</td>
											</tr>
											<tr>
												<th>4</th>
												<td>「迷惑メールフィルターの設定/確認へ」を選択してください。</td>
											</tr>
											<tr>
												<th>5</th>
												<td>ご契約時に設定した、4桁の暗証番号を入力して「送信」を押してください。</td>
											</tr>
											<tr>
												<th>6</th>
												<td>「個別設定」の「受信リストに登録／アドレス帳受信設定をする」を押してください。</td>
											</tr>
											<tr>
												<th>7</th>
												<td>以下の順に設定の操作をしてください。 <br>
													【1】入力欄に「calmia.jp」を入力します。 <br>
													【2】「必ず受信」にチェックを入れます。 <br>
													【3】「ドメイン一致」を選択してください。 <br>
													【4】「有効」を選択してください。 <br>
													【5】「変更する」を押してください。</td>
											</tr>
											<tr>
												<th>8</th>
												<td>内容をご確認の上、間違いがなければ「OK」を押してください。</td>
											</tr>
										</table>
									</div>
								
								</div>
							</div>
						</div>
						<h3 class="read-hdl-s">2.完了</h3>
						<div class="statement-box">
							<div class="inner">
								<div class="read-main">
									<p class="sankou">参考：<a href="https://www.au.com/support/faq/view.k13121723783/" target="_blank">au サポートサイト 受信リスト設定</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="read-model">
				<div class="read-area">
					<h2 class="hdl-plus read-hdl">au（スマートフォン／Eメール）</h2>
					<div class="read-plus">
						<h3 class="read-hdl-s">1.ドメイン指定受信設定</h3>
						<div class="statement-box">
							<div class="inner">
								<div class="read-main">
									<p>auのスマートフォン（Eメール）の方は､下記の手順で｢ドメイン指定受信設定｣を実施してください。</p>
									<div class="read-table01">
										<table class="com-table01">
											<tr>
												<th class="w10per">1</th>
												<td>「Eメールアプリ」を押して起動します。</td>
											</tr>
											<tr>
												<th>2</th>
												<td>画面左下の「MENU」を押してください。</td>
											</tr>
											<tr>
												<th>3</th>
												<td>「アドレス変更 / 迷惑メール設定」を選択してください。</td>
											</tr>
											<tr>
												<th>4</th>
												<td>「迷惑メールフィルターの設定/確認へ」を選択してください。</td>
											</tr>
											<tr>
												<th>5</th>
												<td>ご契約時に設定した、4桁の暗証番号を入力して「送信」を押してください。</td>
											</tr>
											<tr>
												<th>6</th>
												<td>「個別設定」の「受信リストに登録／アドレス帳受信設定をする」を押してください。
</td>
											</tr>
											<tr>
												<th>7</th>
												<td>以下の順に設定の操作をしてください。<br>
												【1】入力欄に「calmia.jp」を入力します。<br>
												【2】「必ず受信」にチェックを入れます。<br> 
												【3】「ドメイン一致」を選択してください。<br> 
												【4】「有効」を選択してください。<br> 
												【5】「変更する」を押してください。</td>
											</tr>
											<tr>
												<th>8</th>
												<td>内容をご確認の上、間違いがなければ「OK」を押してください。</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<h3 class="read-hdl-s">2.完了</h3>
						<div class="statement-box">
							<div class="inner">
								<div class="read-main">
									<p class="sankou"><a href="https://www.au.com/support/faq/view.k13121723783/" target="_blank">参考:au サポートサイト 受信リスト設定</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
						<div class="read-model">
				<div class="read-area">
					<h2 class="hdl-plus read-hdl">au（iPhone）</h2>
					<div class="read-plus">
						<h3 class="read-hdl-s">1.ドメイン指定受信設定</h3>
						<div class="statement-box">
							<div class="inner">
								<div class="read-main">
									<p>auのiPhoneの方は､下記の手順で｢ドメイン指定受信設定｣を実施してください。
</p>
									<div class="read-table01">
										<table class="com-table01">
											<tr>
												<th class="w10per">1</th>
												<td>「迷惑メールフィルター設定のログインページ」へアクセスします。<br>
												<a href="https://mfilter.ezweb.ne.jp/jsp/md/index.jsp" target="_blank">https://mfilter.ezweb.ne.jp/jsp/md/index.jsp</a></td>
											</tr>
											<tr>
												<th>2</th>
												<td>au IDとパスワードを入力してログインしてください。</td>
											</tr>
											<tr>
												<th>3</th>
												<td>「EZ番号通知確認」が表示されるので、内容をご確認のうえ「同意する」を押してください。</td>
											</tr>
											<tr>
												<th>4</th>
												<td>「個別設定」の「受信リストに登録／アドレス帳受信設定をする」を押してください。</td>
											</tr>
											<tr>
												<th>5</th>
												<td>以下の順に設定の操作をしてください。<br>
												【1】入力欄に「calmia.jp」を入力します。<br>
												【2】「必ず受信」にチェックを入れます。<br> 
												【3】「ドメイン一致」を選択してください。<br> 
												【4】「有効」を選択してください。<br> 
												【5】「変更する」を押してください。</td>
											</tr>
											<tr>
												<th>6</th>
												<td>内容をご確認の上、間違いがなければ「OK」を押してください。
</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<h3 class="read-hdl-s">2.完了</h3>
						<div class="statement-box">
							<div class="inner">
								<div class="read-main">
									<p class="sankou"><a href="https://www.au.com/support/faq/view.k13121323725/" target="_blank">参考:au サポートサイト 受信リスト設定</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="read-model">
				<div class="read-area">
					<h2 class="hdl-plus read-hdl">SoftBank（スマートフォン／iPhone）</h2>
					<div class="read-plus">
						<h3 class="read-hdl-s">1.ドメイン指定受信設定</h3>
						<div class="statement-box">
							<div class="inner">
								<div class="read-main">
									<p>SoftBankの方は､下記の手順で｢ドメイン指定受信設定｣を実施してください。</p>
									<div class="read-table01">
										<table class="com-table01">
											<tr>
												<th class="w10per">1</th>
												<td>My SoftBank へアクセスし、「メール設定」を選択してください。（注1）</td>
											</tr>
											<tr>
												<th>2</th>
												<td>「迷惑メール対策」を選択してください。</td>
											</tr>
											<tr>
												<th>3</th>
												<td>「許可するメールの登録」の「登録する」を押してください。</td>
											</tr>
											<tr>
												<th>4</th>
												<td>入力欄に 「calmia.jp」 を入力し､｢後方一致｣にチェックを入れ｢次へ｣を選択してください。</td>
											</tr>
											<tr>
												<th>5</th>
												<td>「登録する」を押してください。</td>
											</tr>
										</table>
									</div>
									<p>注1：｢My SoftBank｣がみつからない場合はお手数ですが､SoftBankサイトをご覧いただくか､最寄りのSoftBankショップへお尋ねください。</p>
								</div>
							</div>
						</div>
						<h3 class="read-hdl-s">3.完了</h3>
						<div class="statement-box">
							<div class="inner">
								<div class="read-main">
									<p class="sankou"><a href="https://www.softbank.jp/mobile/support/iphone/antispam/email_i/white/" target="_blank">参考:SoftBank迷惑メール設定をする（受信許可リストを設定）</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="read-panel">
				<div class="inner">
					<p class="read-txt02">▼各携帯会社のサイトはこちら</p>
					<ul class="read-logo-list clearfix">
						<li><a href="https://www.nttdocomo.co.jp/"><img src="<!--{$TPL_URLPATH}-->img/login/logo_login01.png" alt="ntt docomo"></a></li>
						<li><a href="https://www.au.com/"><img src="<!--{$TPL_URLPATH}-->img/login/logo_login02.png" alt="au"></a></li>
						<li><a href="https://www.softbank.jp/"><img src="<!--{$TPL_URLPATH}-->img/login/logo_login03.png" alt="softbank"></a></li>
					</ul>
					<p class="read-backlogin-btn"><a href="/mypage/">ログイン画面に戻る</a></p>
				</div>
			</div>
		</article>