<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}--><link rel="stylesheet" type="text/css" href="<!--{$TPL_URLPATH}-->css/shopping.css" media="all" />
<article>
          
            
            <div id="teaser-tit">
                <h2 class="teaser-titin">入力内容のご確認</h2>
            </div><!-- /#teaser-tit -->
            
            <p id="breadcrumb"><a href="/">HOME</a><span>&gt;</span>入力内容のご確認</p>
            
            <div id="contents">
                
                <div class="visible-pc">
                    <ul class="dexpress-process-list clearfix">
                        <li>お届け先の確認</li>
                        <li>お支払い方法・お届け時間帯</li>
                        <li class="on">入力内容のご確認</li>
                        <li>完了</li>
                    </ul>
                </div>
                <div class="section">
                    <div class="inner">
                        <p class="visible-ts mb10">下記のご注文内容に間違いはございませんか？</p>
                    </div>
                   <form method="post" action="<!--{$smarty.const.MOBILE_SHOPPING_CONFIRM_URLPATH}-->">
<input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
<input type="hidden" name="mode" value="confirm">
<input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->">
                        <div class="section">
                            <div class="confirm-main clearfix">
                                <div class="confirm-left">
                                    <div class="section">
                                        <div class="confirm-table01">
                                            <p class="confirm-tit mb10 visible-ts">ご注文内容</p>
                                            <div class="inner">
                                                <div class="visible-pc">
                                                    <table class="com-table01">
                                                    <tr>
                                                        <th class="w30per">ご注文内容</th>
                                                        <td>

                                                        <!--{foreach from=$arrCartItems item=item}-->
                                                            <div class="confirm-table-txt">
                                                                <dl class="clearfix">
                                                                    <dt><!--{$item.productsClass.name|h}--><br>
                                                                    <!--{if $item.productsClass.classcategory_name1 != ""}--><!--{$item.productsClass.class_name1}-->：<!--{$item.productsClass.classcategory_name1}--><br><!--{/if}-->
                                                                    <!--{if $item.productsClass.classcategory_name2 != ""}--><!--{$item.productsClass.class_name2}-->：<!--{$item.productsClass.classcategory_name2}--><br><!--{/if}--></dt>
                                                                    <dd><span>単価：<!--{if $arrSiteInfo.tax_flg eq 1}-->
                                                                        <!--{$item.productsClass.price02|number_format}-->
                                                                    <!--{else}-->
                                                                        <!--{$item.productsClass.price02|sfCalcIncTax:$arrInfo.tax:$arrInfo.tax_rule|number_format}-->
                                                                    <!--{/if}-->円</span><span>数量：<!--{$item.quantity|number_format}--></span><span>小計：<!--{if $arrSiteInfo.tax_flg eq 1}-->
                                                                        <!--{$item.total_outtax|number_format}-->
                                                                    <!--{else}-->
                                                                        <!--{$item.total_inctax|number_format}-->
                                                                    <!--{/if}-->円</span></dd>
                                                                </dl>
                                                            </div>
                                                       <!--{/foreach}-->    
                                                        </td>
                                                    </tr>
                                                </table>
                                                </div>
                                                <div class="visible-ts">
                                                    <table class="com-table01">
                                                    <tr>
                                                        <td>
                                                          <!--{foreach from=$arrCartItems item=item}-->
                                                            <div class="confirm-table-txt">
                                                                <dl class="clearfix">
                                                                    <dt><!--{$item.productsClass.name|h}--><br>
                                                                    <!--{if $item.productsClass.classcategory_name1 != ""}--><!--{$item.productsClass.class_name1}-->：<!--{$item.productsClass.classcategory_name1}--><br><!--{/if}-->
                                                                    <!--{if $item.productsClass.classcategory_name2 != ""}--><!--{$item.productsClass.class_name2}-->：<!--{$item.productsClass.classcategory_name2}--><br><!--{/if}--></dt>
                                                                    <dd><span>単価：<!--{if $arrSiteInfo.tax_flg eq 1}-->
                                                                        <!--{$item.productsClass.price02|number_format}-->
                                                                    <!--{else}-->
                                                                        <!--{$item.productsClass.price02|sfCalcIncTax:$arrInfo.tax:$arrInfo.tax_rule|number_format}-->
                                                                    <!--{/if}-->円</span><span>数量：<!--{$item.quantity|number_format}--></span><span>小計：<!--{if $arrSiteInfo.tax_flg eq 1}-->
                                                                        <!--{$item.total_outtax|number_format}-->
                                                                    <!--{else}-->
                                                                        <!--{$item.total_inctax|number_format}-->
                                                                    <!--{/if}-->円</span></dd>
                                                                </dl>
                                                            </div>
                                                       <!--{/foreach}-->    
                                                        </td>
                                                    </tr>
                                                </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="confirm-table-press visible-ts">

                                            <div class="inner">
                                                <table class="com-table01">
                                                    <tr>
                                                        <th class="w60per">商品合計</th>
                                                        <td><!--{if $arrSiteInfo.tax_flg eq 1}--><!--{$tpl_total_outtax[$cartKey]|number_format}--><!--{else}--><!--{$tpl_total_inctax[$cartKey]|number_format}--><!--{/if}-->円</td>
                                                    </tr>
                                                    
                                                   
                                                    <tr>
                                                        <th>送料</th>
                                                        <td><!--{if $arrSiteInfo.tax_flg eq 1}--><!--{$arrForm.deliv_fee|number_format}--><!--{else}--><!--{$arrForm.deliv_fee|sfCalcIncTax:$arrInfo.tax:$arrInfo.tax_rule|number_format}--><!--{/if}-->円</td>
                                                    </tr>

                                                     <!--{if $arrForm.charge > 0}-->
                                                    <tr>
                                                        <th>手数料</th>
                                                        <td><!--{if $arrSiteInfo.tax_flg eq 1}--><!--{$arrForm.charge|number_format}--><!--{else}--><!--{$arrForm.charge|sfCalcIncTax:$arrInfo.tax:$arrInfo.tax_rule|number_format}--><!--{/if}-->円</td>
                                                    </tr><!--{/if}-->


                                                    <!--{if $arrSiteInfo.tax_flg eq 1}--><tr>
                                                        <th>消費税</th>
                                                        <td><!--{$arrForm.tax|number_format}-->円</td>
                                                    </tr><!--{/if}-->
                                                     <!--{if $smarty.const.USE_POINT !== false}-->
                                                    <!--{assign var=discount value=`$arrForm.use_point*$smarty.const.POINT_VALUE`}-->
                                                    <tr>
                                                        <th>ポイント値引き</th>
                                                        <td><!--{if $discount > 0}-->-<!--{/if}--><!--{$discount|number_format|default:0}-->円</td>
                                                    </tr><!--{/if}-->
                                                   
                                                    <tr>
                                                        <th class="confirm-border-no">合計</th>
                                                        <td><!--{$arrForm.payment_total|number_format}-->円</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="visible-ts confirm-table02">
                                        <p class="confirm-tit visible-ts">ポイントの確認</p>
                                            <div class="inner">
                                                <table class="com-table01">

                                                 <tr>
                                                <td class="w65per">ご注文前のポイント</td>
                                                <td class="tar"><!--{$tpl_user_point|number_format|default:0}-->Pt</td>
                                            </tr>
                                            <tr>
                                                <td>ご使用ポイント</td>
                                                <td class="tar"><!--{if $discount > 0}-->-<!--{/if}--><!--{$arrForm.use_point|number_format|default:0}-->Pt</td>
                                            </tr>
                                            <!--{if $arrForm.birth_point > 0}--><tr>
                                                <td>ご使用ポイント</td>
                                                <td class="tar">+<!--{$arrForm.birth_point|number_format|default:0}-->Pt</td>
                                            </tr><!--{/if}-->
                                            <tr>
                                                <td>今回加算予定のポイント</td>
                                                <td class="tar">+<!--{$arrForm.add_point|number_format|default:0}-->Pt</td>
                                            </tr><!--{assign var=total_point value=`$tpl_user_point-$arrForm.use_point+$arrForm.add_point`}-->
                                            <tr>
                                                <td>加算後のポイント</td>
                                                <td class="tar"><!--{$total_point|number_format}-->Pt</td>
                                            </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    
                           
                                    
                                    <div class="section">
                                        <div class="confirm-table03">
                                            <table class="com-table01">
                                                <tr>
                                                    <th class="w30per">ご依頼主</th>
                                                    <td>〒<!--{$arrForm.order_zip01|h}-->-<!--{$arrForm.order_zip02|h}--><br><!--{$arrPref[$arrForm.order_pref]}--><!--{$arrForm.order_addr01|h}--><!--{$arrForm.order_addr02|h}--><br><!--{$arrForm.order_name01|h}--> <!--{$arrForm.order_name02|h}--><br><!--{$arrForm.order_email|h}--><br><!--{$arrForm.order_tel01|h}-->-<!--{$arrForm.order_tel02|h}-->-<!--{$arrForm.order_tel03|h}--></td>
                                                </tr>
                                                <!--{if $cartKey != $smarty.const.PRODUCT_TYPE_DOWNLOAD}-->
                                                <!--{foreach item=shippingItem from=$arrShipping name=shippingItem}-->

                                                <tr>
                                                    <th class="w30per">お届け先</th>
                                                    <td>
                                                    <!--{if $is_multiple}-->
                                                        <!--{$smarty.foreach.shippingItem.iteration}--><br>
                                                        <!--{* 複数お届け先の場合、お届け先毎の商品を表示 *}-->
                                                        <!--{foreach item=item from=$shippingItem.shipment_item}-->
                                                        <!--{$item.productsClass.name|h}--><br>
                                                        <!--{if $item.productsClass.classcategory_name1 != ""}--><!--{$item.productsClass.class_name1}-->：<!--{$item.productsClass.classcategory_name1}--><br><!--{/if}-->
                                                        <!--{if $item.productsClass.classcategory_name2 != ""}--><!--{$item.productsClass.class_name2}-->：<!--{$item.productsClass.classcategory_name2}--><br><!--{/if}-->
                                                        &nbsp;数量：<!--{$item.quantity}--><br>
                                                        <br>
                                                        <!--{/foreach}-->
                                                    <!--{/if}-->
                                                    〒<!--{$shippingItem.shipping_zip01|h}-->-<!--{$shippingItem.shipping_zip02|h}--><br><!--{$arrPref[$shippingItem.shipping_pref]}--><!--{$shippingItem.shipping_addr01|h}--><!--{$shippingItem.shipping_addr02|h}--><br><!--{$shippingItem.shipping_name01|h}--> <!--{$shippingItem.shipping_name02|h}--><br><!--{$shippingItem.shipping_tel01|h}-->-<!--{$shippingItem.shipping_tel02|h}-->-<!--{$shippingItem.shipping_tel03|h}-->    </td>
                                                </tr><!--{/foreach}-->
                                                <!--{/if}-->
                                                <tr>
                                                    <th class="w30per">お届け時間</th>
                                                    <td><!--{$shippingItem.shipping_time|default:"指定なし"|h}--></td>
                                                </tr>
                                                <tr>
                                                    <th class="w30per">配送方法</th>
                                                    <td><!--{$arrDeliv[$arrForm.deliv_id]|h}--></td>
                                                </tr>
                                                <tr>
                                                    <th class="w30per">お支払い方法</th>
                                                    <td><!--{$arrForm.payment_method|h}--></td>
                                                </tr>
                                                <!--{if $arrForm.message != ""}--><tr>
                                                    <th class="w30per">その他のお問い合わせ</th>
                                                    <td><!--{$arrForm.message|h|nl2br}--></td>
                                                </tr><!--{/if}-->
                                            </table>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="confirm-right visible-pc">
                                    
                                    
                                    <div class="section">
                                        <div class="confirm-table-press">
                                            <table class="com-table01">
                                            <tr>
                                                        <th class="w60per">商品合計</th>
                                                        <td><!--{if $arrSiteInfo.tax_flg eq 1}--><!--{$tpl_total_outtax[$cartKey]|number_format}--><!--{else}--><!--{$tpl_total_inctax[$cartKey]|number_format}--><!--{/if}-->円</td>
                                                    </tr>
                                                   
                                                     <tr>
                                                        <th>送料</th>
                                                        <td><!--{if $arrSiteInfo.tax_flg eq 1}--><!--{$arrForm.deliv_fee|number_format}--><!--{else}--><!--{$arrForm.deliv_fee|sfCalcIncTax:$arrInfo.tax:$arrInfo.tax_rule|number_format}--><!--{/if}-->円</td>
                                                    </tr>
                                                     <!--{if $arrForm.charge > 0}-->
                                                    <tr>
                                                        <th>手数料</th>
                                                        <td><!--{if $arrSiteInfo.tax_flg eq 1}--><!--{$arrForm.charge|number_format}--><!--{else}--><!--{$arrForm.charge|sfCalcIncTax:$arrInfo.tax:$arrInfo.tax_rule|number_format}--><!--{/if}-->円</td>
                                                    </tr><!--{/if}-->

                                                    <!--{if $arrSiteInfo.tax_flg eq 1}--><tr>
                                                        <th>消費税</th>
                                                        <td><!--{$arrForm.tax|number_format}-->円</td>
                                                    </tr><!--{/if}-->
                                                    <!--{if $smarty.const.USE_POINT !== false}-->
                                                    <!--{assign var=discount value=`$arrForm.use_point*$smarty.const.POINT_VALUE`}-->
                                                    <tr>
                                                        <th>ポイント値引き</th>
                                                        <td><!--{if $discount > 0}-->-<!--{/if}--><!--{$discount|number_format|default:0}-->円</td>
                                                    </tr><!--{/if}-->
                                                    <tr>
                                                        <th class="confirm-border-no">合計</th>
                                                        <td><!--{$arrForm.payment_total|number_format}-->円</td>
                                                    </tr>
                                            </table>
                                        </div>
                                    </div>
                                   
                                    <div class="section">
                                        <table class="com-table01">

                                            <tr>
                                                <th colspan="2">ポイントの確認</th>
                                            </tr>
                                            <tr>
                                                <td class="w65per">ご注文前のポイント</td>
                                                <td class="tar"><!--{$tpl_user_point|number_format|default:0}-->pt</td>
                                            </tr>
                                            <tr>
                                                <td class="w65per">ご使用ポイント</td>
                                                <td class="tar"><!--{if $discount > 0}-->-<!--{/if}--><!--{$arrForm.use_point|number_format|default:0}-->pt</td>
                                            </tr>
                                            <!--{if $arrForm.birth_point > 0}--><tr>
                                                <td class="w65per">ご使用ポイント</td>
                                                <td class="tar">+<!--{$arrForm.birth_point|number_format|default:0}-->pt</td>
                                            </tr><!--{/if}-->
                                            <tr>
                                                <td class="w65per">今回加算予定のポイント</td>
                                                <td class="tar">+<!--{$arrForm.add_point|number_format|default:0}-->pt</td>
                                            </tr><!--{assign var=total_point value=`$tpl_user_point-$arrForm.use_point+$arrForm.add_point`}-->
                                            <tr>
                                                <td class="w65per">加算後のポイント</td>
                                                <td class="tar"><!--{$total_point|number_format}-->pt</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="inner">
                                <p class="confirm-note01 fcred">※クレジットカード払いを選択された場合は注文確定後にカード情報をご入力いただきます。</p>
                            </div>
                        </div>
                        <div class="combtn-top combtn-twocolumn">


                            <ul class="clearfix">
                                <li class="flR"><input type="submit" value="注文を確定する" class="input-submit"></li></form>
                                <li class="confirm-note02 fcred">※クレジットカード払いを選択された場合は注文確定後にカード情報をご入力いただきます。</li>
                                <li class="flR"><form action="<!--{$smarty.const.MOBILE_SHOPPING_PAYMENT_URLPATH}-->" method="post">
                                <input type="hidden" name="<!--{$smarty.const.TRANSACTION_ID_NAME}-->" value="<!--{$transactionid}-->" />
                                <input type="hidden" name="mode" value="select_deliv">
                                <input type="hidden" name="deliv_id" value="<!--{$arrForm.deliv_id|h}-->">
                                <input type="hidden" name="uniqid" value="<!--{$tpl_uniqid}-->">
                                <input type="submit" value="戻る" class="input-button back">
                                </form></li>
                            </ul>
                        </div>
                </div>
            </div><!-- /#contents -->
            
        </article>