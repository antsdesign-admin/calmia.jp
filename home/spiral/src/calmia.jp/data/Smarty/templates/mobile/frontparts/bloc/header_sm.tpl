<div class="header-box clearfix">
                    <div class="header-logo visible-pc">
                        <dl>
                            <dt><a href="/" class="op"><img src="<!--{$TPL_URLPATH}-->img/common/logo.svg" width="85" alt="calmia"></a></dt>
                     
                        </dl>
                    </div>
                    <div class="header-logo visible-ts">
                        <dl>
                            <dt><a href="/"><img src="<!--{$TPL_URLPATH}-->img/common/logo.svg" width="52" height="52" alt="calmia"></a></dt>
                        </dl>
                    </div>
                    <div class="header-area visible-pc">
                        <form method="post">
                            <ul class="header-list clearfix">
                                <li><img class="ic-move2" src="<!--{$TPL_URLPATH}-->img/common/logout_header.jpg" alt="logout"><a class="opa" href="<!--{$smarty.server.PHP_SELF|escape}-->" onclick="fnFormModeSubmit('login_form', 'logout', '', ''); return false;"><a class="ic-move" href="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php?<!--{$smarty.const.SID}-->&mode=logout">ログアウト</a>
                            </a></li>
                                <li><img src="<!--{$TPL_URLPATH}-->img/common/cart.svg" width="25" height="25" alt="cart">　<a class="ic-move" href="/cart/">カゴの中を見る</a></li>
  
                            </ul>
                        </form>
        <!--▼現在のポイント-->
        <!--{if $point_disp !== false}-->
                        <p class="header-txt01"><!--{$CustomerName1|h}--> <!--{$CustomerName2|h}-->さん　いつもありがとうございます。現在のポイントは<!--{if $smarty.const.USE_POINT !== false}--><span class="fcred"><!--{$CustomerPoint|number_format|default:"0"|h}--></span> ポイントです。<!--{/if}--></p>
                    </div> <!--{/if}-->
        <!--▲現在のポイント-->
                    <div class="header-area visible-ts">
                        <ul class="header-list clearfix">
                            <li><a href="/cart/"><img src="<!--{$TPL_URLPATH}-->img/common/cart.svg" width="25" height="25" alt="cart"></a></li>
                            <li><p class="search-in" data-target=".headersp-search"><a href="javascript:void(0)"><img src="<!--{$TPL_URLPATH}-->img/common/search.svg" alt="search" width="25" height="25"></a></p></li>
                            <li><p class="navbar-toggle" data-target=".navbar-collapse"><a href="javascript:void(0)">menu</a></p></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <nav class="navbar-collapse">
            <div class="navbar-box">
                <ul>
                    <li><a href="/">HOME</a></li>
                    <li><a href="/products/">ショッピング</a></li>
                    <li><a href="/mypage/favorite.php">お気に入り</a></li>
                    <li><a href="/mypage/history_list.php">ご注文履歴</a></li>
                    <li><a href="/guide/order.php">ご注文ガイド</a></li>
                    <li><a href="/mypage/change.php">お客様情報の変更</a></li>
                    <li><a href="/order/">特定商取引に関する表記</a></li>
                    <li><a href="/guide/about.php">会社概要</a></li>
                    <li><a href="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php?<!--{$smarty.const.SID}-->&mode=logout">ログアウト</a></li>
                </ul>
            </div>
        </nav>
        <div id="gnavi" class="visible-pc">
            <div class="gnavi-in">
                <p class="header-menu"><a href="javascript:void(0)"><img src="<!--{$TPL_URLPATH}-->img/common/menu_navi.png" alt="menu"></a></p>
                <ul class="gnavi-list">
                    <li><a href="/">HOME</a></li>
                    <li><a href="/products/">ショッピング</a></li>
                    <li><a href="/mypage/favorite.php">お気に入り</a></li>
                    <li><a href="/mypage/history_list.php">ご注文履歴</a></li>
                    <li><a href="/mypage/change.php">お客様情報の変更</a></li>
                </ul>
            </div>
        </div>
    </div><!-- /#header -->
</header>

<article>
            <div class="header-menu-box visible-ts">
                <div class="inner header-menusp">
                    <dl>
                        <dt><a href="javascript:void(0)"><img src="<!--{$TPL_URLPATH}-->img/common/menu_navi_ts.png" alt="menu"></a></dt>
                        <dd><!--▼現在のポイント-->
        <!--{if $point_disp !== false}-->
                        <!--{$CustomerName1|h}--> <!--{$CustomerName2|h}-->さん　いつもありがとうございます。<br>現在のポイントは<!--{if $smarty.const.USE_POINT !== false}--><span class="fcred"><!--{$CustomerPoint|number_format|default:"0"|h}--></span> ポイントです。<!--{/if}--></dd>
                    <!--{/if}-->
        <!--▲現在のポイント-->
                    </dl>
                </div>
            </div>

