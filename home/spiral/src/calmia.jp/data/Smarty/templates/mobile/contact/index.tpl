<!--{*
/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
*}-->

<font size=1>
&#xE688;お電話でのご注文･お問合せ<br>
&#xE687;<a href="tel:<!--{$objSiteInfo->data.tel01|h}--><!--{$objSiteInfo->data.tel02|h}--><!--{$objSiteInfo->data.tel03|h}-->" telbook="xxxxxxx" email="<!--{$objSiteInfo->data.email02|escape:'hexentity'}-->"><!--{$objSiteInfo->data.tel01|h}-->-<!--{$objSiteInfo->data.tel02|h}-->-<!--{$objSiteInfo->data.tel03|h}--></a><br>
<!--{if strlen($objSiteInfo->business_hour) > 0}-->
&#xE6BA;受付時間:<!--{$objSiteInfo->business_hour|h}--><br />
<!--{/if}-->
<!--{if strlen($tpl_holiday) > 0}-->
<!--{$tpl_holiday|h}--><br />
<!--{/if}-->

<br>
<br>
尚、お電話でご注文いただいた場合、ポイントは付与されませんので予めご了承くださいませ。
</font>

<hr size=1 noshade color="#eeeaec">
<font size=1>
&#xE6D3;ﾒｰﾙでのお問合せ<br>
&#xE687;<a href=mailto:<!--{$objSiteInfo->data.email02|escape:'hexentity'}-->?subject=問い合わせ><!--{$objSiteInfo->data.email02|escape:'hexentity'}--></a>
</font>
<br>

