<!--{*
 * This file is part of EC-CUBE
 *
 * Copyright(c) 2000-2011 LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA    02111-1307, USA.
 *}-->
<!--▼HEADER-->
<body>
    <div id="wrapper">
<header>
    <div id="header">
        <div class="inner">
            <div class="header-in">
                <div class="header-box clearfix">
                    <div class="header-logo visible-pc">
                        <dl>
                            <dt><a href="/index.html" class="op"><img src="<!--{$TPL_URLPATH}-->img/common/logo_header.png" alt="calmia"></a></dt>
                            <dd><a href="/index.html">カルミアメンバーズサイト</a></dd>
                        </dl>
                    </div>
                    <div class="header-logo visible-ts">
                        <dl>
                            <dt><a href="/index.html"><img src="<!--{$TPL_URLPATH}-->img/common/logo_header.png" alt="calmia"></a></dt>
                            <dd><a href="/index.html">カルミア<span>メンバーズサイト</span></a></dd>
                        </dl>
                    </div>
                    <div class="header-area visible-pc">
                        <form method="post">
                            <ul class="header-list clearfix">
                                <li><img src="<!--{$TPL_URLPATH}-->img/common/logout_header.jpg" alt="logout"><a class="opa" href="<!--{$smarty.server.PHP_SELF|escape}-->" onclick="fnFormModeSubmit('login_form', 'logout', '', ''); return false;">
                           <a href="<!--{$smarty.const.HTTPS_URL}-->frontparts/login_check.php?<!--{$smarty.const.SID}-->&amp;mode=logout">ログアウト</a>
                            </a></li>
                                <li><img src="<!--{$TPL_URLPATH}-->img/common/cart_header.jpg" alt="cart"><a href="/shopping/cart.html">カゴの中を見る</a></li>
                                <li><img src="<!--{$TPL_URLPATH}-->img/common/search_header.png" alt="search" class="vam"><input type="text" id="search" name="search" class="header-search"></li>
                            </ul>
                        </form>
        <!--▼現在のポイント-->
        <!--{if $point_disp !== false}-->
                        <p class="header-txt01"><!--{$CustomerName1|h}--> <!--{$CustomerName2|h}-->さん　いつもありがとうございます。現在のポイントは<!--{if $smarty.const.USE_POINT !== false}--><span class="fcred"><!--{$CustomerPoint|number_format|default:"0"|h}--></span> ポイントです。<!--{/if}--></p>
                    </div> <!--{/if}-->
        <!--▲現在のポイント-->
                    <div class="header-area visible-ts">
                        <ul class="header-list clearfix">
                            <li><a href="/shopping/cart.html"><img src="<!--{$TPL_URLPATH}-->img/common/cart_header.jpg" alt="cart"></a></li>
                            <li><p class="search-in" data-target=".headersp-search"><a href="javascript:void(0)"><img src="<!--{$TPL_URLPATH}-->img/common/search_header.png" alt="search"></a></p></li>
                            <li><p class="navbar-toggle" data-target=".navbar-collapse"><a href="javascript:void(0)">menu</a></p></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="headersp-search">
            <div class="clearfix search-box">
                <form method="post">
                    <p class="search-input"><input type="text" id="searchsp"></p>
                    <p class="search-sumbit"><input type="submit" value="検索"></p>
                </form>
            </div>
        </div>
        <nav class="navbar-collapse">
            <div class="navbar-box">
                <ul>
                    <li><a href="/index.html">HOME</a></li>
                    <li><a href="/shopping/index.html">ショッピング</a></li>
                    <li><a href="/popular/index.html">お気に入り</a></li>
                    <li><a href="/order-list/list.html">ご注文履歴</a></li>
                    <li><a href="/guide/index.html">ご注文ガイド</a></li>
                    <li><a href="/customer/change.html">お客様情報の変更</a></li>
                    <li><a href="/article/index.html">特定商取引に関する表記</a></li>
                    <li><a href="/company/index.html">会社概要</a></li>
                    <li><a class="opa" href="<!--{$smarty.server.PHP_SELF|escape}-->" onclick="fnFormModeSubmit('login_form', 'logout', '', ''); return false;">
                            <input type="submit" src="<!--{$TPL_URLPATH}-->img/common/h_logout.png" width="124" height="27" alt="ログアウト" value="ログアウト">
                            </a></li>
                </ul>
            </div>
        </nav>
        <div id="gnavi" class="visible-pc">
            <div class="gnavi-in">
                <p class="header-menu"><a href="javascript:void(0)"><img src="<!--{$TPL_URLPATH}-->img/common/menu_navi.png" alt="menu"></a></p>
                <ul class="gnavi-list">
                    <li><a href="/index.html">HOME</a></li>
                    <li><a href="/shopping/index.html">ショッピング</a></li>
                    <li><a href="/popular/index.html">お気に入り</a></li>
                    <li><a href="/order-list/list.html">ご注文履歴</a></li>
                    <li><a href="/customer/change.html">お客様情報の変更</a></li>
                </ul>
            </div>
        </div>
    </div><!-- /#header -->
</header>

<article>
            <div class="header-menu-box visible-ts">
                <div class="inner header-menusp">
                    <dl>
                        <dt><a href="javascript:void(0)"><img src="<!--{$TPL_URLPATH}-->img/common/menu_navi_ts.png" alt="menu"></a></dt>
                        <dd><!--▼現在のポイント-->
        <!--{if $point_disp !== false}-->
                        <!--{$CustomerName1|h}--> <!--{$CustomerName2|h}-->さん　いつもありがとうございます。<br>現在のポイントは<!--{if $smarty.const.USE_POINT !== false}--><span class="fcred"><!--{$CustomerPoint|number_format|default:"0"|h}--></span> ポイントです。<!--{/if}--></dd>
                    <!--{/if}-->
        <!--▲現在のポイント-->
                    </dl>
                </div>
            </div>
            